function initializesegMessagesTmplKA() {
    flxSegContainerKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxSegContainerKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxSegContainerKA.setDefaultUnit(kony.flex.DP);
    var flxSegMsgSwipe = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxSegMsgSwipe",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "130%",
        "zIndex": 1
    }, {}, {});
    flxSegMsgSwipe.setDefaultUnit(kony.flex.DP);
    var flxMainKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxMainKA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "77%",
        "zIndex": 1
    }, {}, {});
    flxMainKA.setDefaultUnit(kony.flex.DP);
    var lblTitleKA = new kony.ui.Label({
        "id": "lblTitleKA",
        "isVisible": true,
        "left": "5%",
        "maxNumberOfLines": 1,
        "skin": "sknMessageTitleKA",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "2%",
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxDescChevronContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxDescChevronContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "25dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDescChevronContainer.setDefaultUnit(kony.flex.DP);
    var lblDescKA = new kony.ui.Label({
        "id": "lblDescKA",
        "isVisible": true,
        "left": "5.00%",
        "maxNumberOfLines": 1,
        "skin": "CopyslLabel031d27909a26c4a",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var imgChevronKA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "20dp",
        "id": "imgChevronKA",
        "isVisible": true,
        "left": 0,
        "right": "4%",
        "skin": "sknslImage",
        "src": "right_chevron_icon.png",
        "width": "10%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxDescChevronContainer.add(lblDescKA, imgChevronKA);
    var lblTimestampKA = new kony.ui.Label({
        "id": "lblTimestampKA",
        "isVisible": true,
        "left": "5%",
        "skin": "sknasOfTimeLabelBlack",
        "top": "60dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMessageIdKA = new kony.ui.Label({
        "id": "lblMessageIdKA",
        "isVisible": false,
        "left": "204dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "67dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxMainKA.add(lblTitleKA, flxDescChevronContainer, lblTimestampKA, lblMessageIdKA);
    var btnDeleteKA = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknbtnDeleteKA",
        "height": "100dp",
        "id": "btnDeleteKA",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_b616e3930d014803a060a8d7412fffab,
        "skin": "sknbtnDeleteKA",
        "text": "Delete",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxSegMsgSwipe.add(flxMainKA, btnDeleteKA);
    flxSegContainerKA.add(flxSegMsgSwipe);
}