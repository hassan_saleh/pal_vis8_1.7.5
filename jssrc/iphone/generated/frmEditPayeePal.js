function addWidgetsfrmEditPayeePal() {
    frmEditPayeePal.setDefaultUnit(kony.flex.DP);
    var mainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": 180,
        "centerY": 307,
        "clipBounds": true,
        "height": "100%",
        "id": "mainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContainer.setDefaultUnit(kony.flex.DP);
    var headerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "headerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    headerContainer.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var CopylblHeaderBackIcon0e57343f2e2274c = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblHeaderBackIcon0e57343f2e2274c",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblHeaderBack0j7f57eb046594b = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblHeaderBack0j7f57eb046594b",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBack.add(CopylblHeaderBackIcon0e57343f2e2274c, CopylblHeaderBack0j7f57eb046594b);
    var CopylblHeaderTitle0de254f31c2b44e = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "CopylblHeaderTitle0de254f31c2b44e",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.billspay.editnickname"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblClose = new kony.ui.Label({
        "centerX": "94%",
        "height": "100%",
        "id": "lblClose",
        "isVisible": true,
        "onTouchEnd": AS_Label_a626b32721584f8794db8e993b6d642f,
        "skin": "sknCloseConfirm",
        "text": "O",
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    headerContainer.add(flxBack, CopylblHeaderTitle0de254f31c2b44e, lblClose);
    var bodyContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "bodyContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    bodyContainer.setDefaultUnit(kony.flex.DP);
    var flxImpDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "25%",
        "id": "flxImpDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxImpDetails.setDefaultUnit(kony.flex.DP);
    var flxIcon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "top": "20%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon.setDefaultUnit(kony.flex.DP);
    var initialsCategory = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "initialsCategory",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "text": "AI",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxIcon.add(initialsCategory);
    var lblBillingName = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBillingName",
        "isVisible": true,
        "skin": "sknBeneTitle",
        "text": "Airtel",
        "top": "3%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblBeneAccNum0d0906faa372149 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopylblBeneAccNum0d0906faa372149",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": "9845654568",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxImpDetails.add(flxIcon, lblBillingName, CopylblBeneAccNum0d0906faa372149);
    var flxNickname = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxNickname",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNickname.setDefaultUnit(kony.flex.DP);
    var lblNickname = new kony.ui.Label({
        "height": "50%",
        "id": "lblNickname",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var txtNickname = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "50%",
        "id": "txtNickname",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "8%",
        "maxTextLength": 35,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblUnderline = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderline",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "8%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "-5%",
        "width": "84%",
        "zIndex": 1
    }, {}, {});
    lblUnderline.setDefaultUnit(kony.flex.DP);
    lblUnderline.add();
    flxNickname.add(lblNickname, txtNickname, lblUnderline);
    var flxServiceType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxServiceType",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxServiceType.setDefaultUnit(kony.flex.DP);
    var lblServiceType1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblServiceType1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblServiceType2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblServiceType2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBillerCode = new kony.ui.Label({
        "id": "lblBillerCode",
        "isVisible": false,
        "left": "269dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxServiceType.add(lblServiceType1, lblServiceType2, lblBillerCode);
    var flxDenomination = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxDenomination",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDenomination.setDefaultUnit(kony.flex.DP);
    var lblDenominationTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblDenominationTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.prepaid.denomination"),
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDenominationDESC = new kony.ui.Label({
        "height": "50%",
        "id": "lblDenominationDESC",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDenomination = new kony.ui.Label({
        "id": "lblDenomination",
        "isVisible": false,
        "left": "269dp",
        "skin": "slLabel",
        "text": "Label",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDenomination.add(lblDenominationTitle, lblDenominationDESC, lblDenomination);
    var flxBillerNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxBillerNumber",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillerNumber.setDefaultUnit(kony.flex.DP);
    var lblBillerNumber1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblBillerNumber1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBillerNumber2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblBillerNumber2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBillerNumber.add(lblBillerNumber1, lblBillerNumber2);
    var btnEditBillerConfirm = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "25%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "10%",
        "id": "btnEditBillerConfirm",
        "isVisible": true,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    bodyContainer.add(flxImpDetails, flxNickname, flxServiceType, flxDenomination, flxBillerNumber, btnEditBillerConfirm);
    mainContainer.add(headerContainer, bodyContainer);
    frmEditPayeePal.add(mainContainer);
};

function frmEditPayeePalGlobals() {
    frmEditPayeePal = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmEditPayeePal,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmEditPayeePal",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};