function initializetempSectionHeaderP2P() {
    CopyFlexContainer0i47e9b94f6b74f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "CopyFlexContainer0i47e9b94f6b74f",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer0i47e9b94f6b74f.setDefaultUnit(kony.flex.DP);
    var lblSepKA = new kony.ui.Label({
        "height": "1dp",
        "id": "lblSepKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLineEDEDEDKA",
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxP2PphonePayeeNameKA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "41.97%",
        "id": "flxP2PphonePayeeNameKA",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxP2PphonePayeeNameKA.setDefaultUnit(kony.flex.DP);
    var payeeFirstName = new kony.ui.Label({
        "centerY": "50%",
        "height": "20dp",
        "id": "payeeFirstName",
        "isVisible": true,
        "left": "5%",
        "skin": "skn383838LatoRegular107KA",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var payeeLastName = new kony.ui.Label({
        "centerY": "50%",
        "height": "20dp",
        "id": "payeeLastName",
        "isVisible": true,
        "left": "2%",
        "skin": "skn383838LatoRegular107KA",
        "top": "10dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxP2PphonePayeeNameKA.add(payeeFirstName, payeeLastName);
    CopyFlexContainer0i47e9b94f6b74f.add(lblSepKA, flxP2PphonePayeeNameKA);
}