function initializetmpServiceTypeDataPrePaid() {
    tmpflxServiceType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90dp",
        "id": "tmpflxServiceType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopyslFbox0i474450467a946"
    }, {}, {});
    tmpflxServiceType.setDefaultUnit(kony.flex.DP);
    var lblServiceTypePrepaid = new kony.ui.Label({
        "id": "lblServiceTypePrepaid",
        "isVisible": true,
        "left": "20%",
        "skin": "sknlblBlack130CarioService",
        "top": "30dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    tmpflxServiceType.add(lblServiceTypePrepaid);
}