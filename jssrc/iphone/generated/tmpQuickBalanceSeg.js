function initializetmpQuickBalanceSeg() {
    flxSegQuickBalance = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxSegQuickBalance",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxSegQuickBalance.setDefaultUnit(kony.flex.DP);
    var lblAccount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccount",
        "isVisible": true,
        "left": "5%",
        "skin": "sknlblTouchIdsmall",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccNumber = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccNumber",
        "isVisible": false,
        "left": "0%",
        "skin": "sknlblTouchIdsmall",
        "text": "Label",
        "top": "101dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIncommingTick = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingTick",
        "isVisible": true,
        "right": "3.80%",
        "skin": "sknBOJttfwhiteeSmall",
        "text": "r",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIncommingRing = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingRing",
        "isVisible": true,
        "right": "2%",
        "skin": "sknBOJttfwhitee",
        "text": "s",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccountName = new kony.ui.Label({
        "id": "lblAccountName",
        "isVisible": false,
        "left": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSegQuickBalance.add(lblAccount, lblAccNumber, lblIncommingTick, lblIncommingRing, lblAccountName);
}