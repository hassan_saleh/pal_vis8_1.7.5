function addWidgetsfrmApplyNewCardsConfirmKA() {
    frmApplyNewCardsConfirmKA.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_e1bff9ec879642459df7f5d7f4db94eb,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblApplyCardTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "90%",
        "id": "lblApplyCardTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeader.add(flxBack, lblApplyCardTitle);
    var flxApplyNewCardsConfirmBody = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "70%",
        "horizontalScrollIndicator": true,
        "id": "flxApplyNewCardsConfirmBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "10%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxApplyNewCardsConfirmBody.setDefaultUnit(kony.flex.DP);
    var flxCreditCardNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxCreditCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxCreditCardNumber.setDefaultUnit(kony.flex.DP);
    var lblCreditCardNumberTitle = new kony.ui.Label({
        "id": "lblCreditCardNumberTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.primarycreditcardnumber"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCreditCardNumber = new kony.ui.Label({
        "id": "lblCreditCardNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCreditCardNumber.add(lblCreditCardNumberTitle, lblCreditCardNumber);
    var flxUpgradeCard = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxUpgradeCard",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxUpgradeCard.setDefaultUnit(kony.flex.DP);
    var lblUpgradeCardTitle = new kony.ui.Label({
        "id": "lblUpgradeCardTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.cards.upgradetype"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblUpgradeCard = new kony.ui.Label({
        "id": "lblUpgradeCard",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxUpgradeCard.add(lblUpgradeCardTitle, lblUpgradeCard);
    var flxSuplementryCardType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxSuplementryCardType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSuplementryCardType.setDefaultUnit(kony.flex.DP);
    var lblSuplementryCardTypeTitle = new kony.ui.Label({
        "id": "lblSuplementryCardTypeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.supplementrycardtype"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSuplementryCardType = new kony.ui.Label({
        "id": "lblSuplementryCardType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSuplementryCardType.add(lblSuplementryCardTypeTitle, lblSuplementryCardType);
    var flxAccountNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxAccountNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAccountNumber.setDefaultUnit(kony.flex.DP);
    var lblAccountNumberTitle = new kony.ui.Label({
        "id": "lblAccountNumberTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxAccountNumber.add(lblAccountNumberTitle, lblAccountNumber);
    var flxCardHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxCardHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxCardHolder.setDefaultUnit(kony.flex.DP);
    var lblCardHolderTitle = new kony.ui.Label({
        "id": "lblCardHolderTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.cardholdername"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCardHolder = new kony.ui.Label({
        "id": "lblCardHolder",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxCardHolder.add(lblCardHolderTitle, lblCardHolder);
    var flxRelationShip = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxRelationShip",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRelationShip.setDefaultUnit(kony.flex.DP);
    var lblRelationShipTitle = new kony.ui.Label({
        "id": "lblRelationShipTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.applycards.realationship"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRelationShip = new kony.ui.Label({
        "id": "lblRelationShip",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRelationShip.add(lblRelationShipTitle, lblRelationShip);
    var flxAge = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxAge",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAge.setDefaultUnit(kony.flex.DP);
    var lblAgeTitle = new kony.ui.Label({
        "id": "lblAgeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.age"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAge = new kony.ui.Label({
        "id": "lblAge",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxAge.add(lblAgeTitle, lblAge);
    var flxReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxReason.setDefaultUnit(kony.flex.DP);
    var lblReasonTitle = new kony.ui.Label({
        "id": "lblReasonTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.reason"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblReason = new kony.ui.Label({
        "id": "lblReason",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxReason.add(lblReasonTitle, lblReason);
    var flxColor = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxColor",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxColor.setDefaultUnit(kony.flex.DP);
    var lblColorTitle = new kony.ui.Label({
        "id": "lblColorTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.common.color"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblColor = new kony.ui.Label({
        "id": "lblColor",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxColor.add(lblColorTitle, lblColor);
    var flxLimitType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxLimitType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxLimitType.setDefaultUnit(kony.flex.DP);
    var lblLimitTypeTitle = new kony.ui.Label({
        "id": "lblLimitTypeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.limittype"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblLimitType = new kony.ui.Label({
        "id": "lblLimitType",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxLimitType.add(lblLimitTypeTitle, lblLimitType);
    var flxSuplementryCardLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxSuplementryCardLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSuplementryCardLimit.setDefaultUnit(kony.flex.DP);
    var lblSuplementryCardLimitTitle = new kony.ui.Label({
        "id": "lblSuplementryCardLimitTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.supplementrycardlimit"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSuplementryCardLimit = new kony.ui.Label({
        "id": "lblSuplementryCardLimit",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxSuplementryCardLimit.add(lblSuplementryCardLimitTitle, lblSuplementryCardLimit);
    var flxEmploymentStatus = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxEmploymentStatus",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxEmploymentStatus.setDefaultUnit(kony.flex.DP);
    var lblEmploymentStatusTitle = new kony.ui.Label({
        "id": "lblEmploymentStatusTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.employmentstatus"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblEmploymentStatus = new kony.ui.Label({
        "id": "lblEmploymentStatus",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxEmploymentStatus.add(lblEmploymentStatusTitle, lblEmploymentStatus);
    var flxProfession = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxProfession",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxProfession.setDefaultUnit(kony.flex.DP);
    var lblProfessionTitle = new kony.ui.Label({
        "id": "lblProfessionTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newcards.profession"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblProfession = new kony.ui.Label({
        "id": "lblProfession",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxProfession.add(lblProfessionTitle, lblProfession);
    var flxTransferedSalary = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxTransferedSalary",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTransferedSalary.setDefaultUnit(kony.flex.DP);
    var lblTransferedSalaryTitle = new kony.ui.Label({
        "id": "lblTransferedSalaryTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.transferedsalary"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblTransferedSalary = new kony.ui.Label({
        "id": "lblTransferedSalary",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTransferedSalary.add(lblTransferedSalaryTitle, lblTransferedSalary);
    var flxMonthlyIncome = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxMonthlyIncome",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMonthlyIncome.setDefaultUnit(kony.flex.DP);
    var lblMonthlyIncomeTitle = new kony.ui.Label({
        "id": "lblMonthlyIncomeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.monthlyincome"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMonthlyIncome = new kony.ui.Label({
        "id": "lblMonthlyIncome",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxMonthlyIncome.add(lblMonthlyIncomeTitle, lblMonthlyIncome);
    var flxRequestLimit = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxRequestLimit",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRequestLimit.setDefaultUnit(kony.flex.DP);
    var lblRequestLimitTitle = new kony.ui.Label({
        "id": "lblRequestLimitTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.requestlimit"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRequestLimit = new kony.ui.Label({
        "id": "lblRequestLimit",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRequestLimit.add(lblRequestLimitTitle, lblRequestLimit);
    var flxDeliveryMode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxDeliveryMode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDeliveryMode.setDefaultUnit(kony.flex.DP);
    var lblDeliveryModeTitle = new kony.ui.Label({
        "id": "lblDeliveryModeTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.accounts.deliveryMode"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblDeliveryMode = new kony.ui.Label({
        "id": "lblDeliveryMode",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxDeliveryMode.add(lblDeliveryModeTitle, lblDeliveryMode);
    var flxNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "18%",
        "id": "flxNotes",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "4%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNotes.setDefaultUnit(kony.flex.DP);
    var lblNotesTitle = new kony.ui.Label({
        "id": "lblNotesTitle",
        "isVisible": true,
        "left": "0%",
        "skin": "sknlblBodytxt",
        "text": kony.i18n.getLocalizedString("i18n.newCard.notes"),
        "textStyle": {},
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblNotes = new kony.ui.Label({
        "id": "lblNotes",
        "isVisible": true,
        "left": "0%",
        "skin": "sknNumber",
        "textStyle": {},
        "top": "26%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxNotes.add(lblNotesTitle, lblNotes);
    flxApplyNewCardsConfirmBody.add(flxCreditCardNumber, flxUpgradeCard, flxSuplementryCardType, flxAccountNumber, flxCardHolder, flxRelationShip, flxAge, flxReason, flxColor, flxLimitType, flxSuplementryCardLimit, flxEmploymentStatus, flxProfession, flxTransferedSalary, flxMonthlyIncome, flxRequestLimit, flxDeliveryMode, flxNotes);
    var flxTermsandConditionCheck = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTermsandConditionCheck",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "3%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "82%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxTermsandConditionCheck.setDefaultUnit(kony.flex.DP);
    var lblTermsandConditionsCheckBox = new kony.ui.Label({
        "id": "lblTermsandConditionsCheckBox",
        "isVisible": true,
        "left": "1%",
        "onTouchEnd": AS_Label_fa984e91e5bb4b6d9c19e156b03e30a3,
        "skin": "sknBOJttfwhitee150",
        "text": "q",
        "textStyle": {},
        "top": "2dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxTncBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxTncBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "10%",
        "isModalContainer": false,
        "onClick": AS_Label_idf25a9034c64002b77400650c0e0f3b,
        "skin": "slFbox",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTncBody.setDefaultUnit(kony.flex.DP);
    var richtxtTermsandConditions = new kony.ui.RichText({
        "id": "richtxtTermsandConditions",
        "isVisible": true,
        "left": "0%",
        "skin": "sknrichTxtWhite100",
        "text": "RichText",
        "top": "0dp",
        "width": "95%",
        "zIndex": 3
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTncBody.add(richtxtTermsandConditions);
    flxTermsandConditionCheck.add(lblTermsandConditionsCheckBox, flxTncBody);
    var btnconfirm = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "94%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "8%",
        "id": "btnconfirm",
        "isVisible": true,
        "onClick": AS_Button_iee0fe973fe9429d9d58a8dd99374807,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    frmApplyNewCardsConfirmKA.add(flxHeader, flxApplyNewCardsConfirmBody, flxTermsandConditionCheck, btnconfirm);
};

function frmApplyNewCardsConfirmKAGlobals() {
    frmApplyNewCardsConfirmKA = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmApplyNewCardsConfirmKA,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmApplyNewCardsConfirmKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "sknmainGradient",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": false,
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_DEFAULT,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};