function prepare_LOOPSERVICEDATA_BULK_PAYMENT_PAY() {
    try {
        selDataPostpaid = [];
        selDataPrepaid = [];
        var paymentMethod;
        var accountNumber;
        var currencyCode;
        var branchCode;
        var acc = kony.store.getItem("BillPayfromAcc");
        var bulkPaymentData = frmBulkPaymentConfirmKA.bulkpaymentsegmant.data; //frmBulkPaymentKA.bulkpaymentsegmant.data;
        kony.print("bulkPaymentData payment -->" + JSON.stringify(bulkPaymentData));
        var temp = {
            "custid": "",
            "lang": "",
            "p_biller_code": "",
            "p_serv_type_code": "",
            "p_billing_no": "",
            "P_deno": "",
            "amount": "",
            "p_validation_code": "",
            "p_due_amt": "",
            "fees": "",
            "p_paymt_Type": "",
            "p_acc_or_cr": "",
            "p_account_br": "",
            "p_AccessChannel": "",
            "p_PaymentMethod": "",
            "p_SendSMSFlag": "",
            "TransferFlag": "",
            "loop_seperator": ":",
            "loop_count": ""
        };
        var tempPostpaid = {
            "custid": "",
            "lang": "",
            "fromAccountNumber": "",
            "SourceBranchCode": "",
            "p_BillerCode": "",
            "p_ServiceType": "",
            "p_BillingNo": "",
            "p_BillNo": "",
            "p_DueAmt": "",
            "amount": "",
            "p_AccessChannel": "",
            "p_PaymentMethod": "",
            "p_SendSMSFlag": "",
            "p_CustInfoFlag": "",
            "p_IdType": "",
            "p_ID": "",
            "p_Nation": "",
            "p_payment_Status": "",
            "p_MobileNo": "",
            "fees": "",
            "p_Fees_On_Biller": "",
            "TransferFlag": "",
            "p_channel": "",
            "p_user_id": "",
            "loop_seperator": ":",
            "loop_count": ""
        };
        var langObj = kony.store.getItem("langPrefObj");
        if (langObj.toUpperCase() === "EN") {
            langObj = "eng";
        } else {
            langObj = "ara";
        }
        var selFields = 0;
        var selFieldsPrepaid = 0;
        if (frmNewBillDetails.btnBillsPayAccounts.text === "t") {
            accountNumber = acc.accountID;
            branchCode = acc.branchNumber;
            currencyCode = acc.ccydesc;
            paymentMethod = "ACTDEB";
        } else {
            paymentMethod = "CCARD";
            accountNumber = acc.card_num;
            branchCode = "";
            currencyCode = acc.balance.split(" ")[1];
        }
        kony.print("bulkPaymentData-->" + JSON.stringify(bulkPaymentData));
        for (var i in bulkPaymentData) {
            if (bulkPaymentData[i].category === "prepaid") {
                //                if (bulkPaymentData[i].btnSetting.text === "p"){
                if (selFieldsPrepaid === "0" || selFieldsPrepaid == 0) {
                    temp.custid = temp.custid + "" + custid;
                    temp.lang = temp.lang + "" + langObj;
                    temp.p_biller_code = temp.p_biller_code + "" + bulkPaymentData[i].p_billercode;
                    temp.p_serv_type_code = temp.p_serv_type_code + "" + bulkPaymentData[i].p_servicetype;
                    temp.p_billing_no = temp.p_billing_no + "" + bulkPaymentData[i].BillingNumber.text;
                    if (bulkPaymentData[i].p_denodesc !== "") temp.P_deno = temp.P_deno + "" + bulkPaymentData[i].p_denodesc;
                    else temp.P_deno = temp.P_deno + "nill";
                    temp.amount = temp.amount + "" + bulkPaymentData[i].hiddueamount;
                    temp.p_validation_code = temp.p_validation_code + "" + bulkPaymentData[i].validationCode;
                    temp.p_due_amt = temp.p_due_amt + "" + bulkPaymentData[i].hiddueamount;
                    temp.fees = temp.fees + "" + bulkPaymentData[i].fee;
                    temp.p_paymt_Type = temp.p_paymt_Type + "" + "A";
                    temp.p_acc_or_cr = temp.p_acc_or_cr + "" + accountNumber;
                    temp.p_account_br = temp.p_account_br + "" + frmBulkPaymentKA.lblBranchCode.text;
                    temp.p_AccessChannel = temp.p_AccessChannel + "" + "BTELLER";
                    temp.p_PaymentMethod = temp.p_PaymentMethod + "" + paymentMethod;
                    temp.p_SendSMSFlag = temp.p_SendSMSFlag + "" + "N";
                    temp.TransferFlag = temp.TransferFlag + "" + "P";
                } else {
                    temp.custid = temp.custid + ":" + custid;
                    temp.lang = temp.lang + ":" + langObj;
                    temp.p_biller_code = temp.p_biller_code + ":" + bulkPaymentData[i].p_billercode;
                    temp.p_serv_type_code = temp.p_serv_type_code + ":" + bulkPaymentData[i].p_servicetype;
                    temp.p_billing_no = temp.p_billing_no + ":" + bulkPaymentData[i].BillingNumber.text;
                    if (bulkPaymentData[i].p_denodesc !== "") temp.P_deno = temp.P_deno + ":" + bulkPaymentData[i].p_denodesc;
                    else temp.P_deno = temp.P_deno + ":nill";
                    temp.amount = temp.amount + ":" + bulkPaymentData[i].hiddueamount;
                    temp.p_validation_code = temp.p_validation_code + ":" + bulkPaymentData[i].validationCode;
                    temp.p_due_amt = temp.p_due_amt + ":" + bulkPaymentData[i].hiddueamount;
                    temp.fees = temp.fees + ":" + bulkPaymentData[i].fee;
                    temp.p_paymt_Type = temp.p_paymt_Type + ":" + "A";
                    temp.p_acc_or_cr = temp.p_acc_or_cr + ":" + accountNumber;
                    temp.p_account_br = temp.p_account_br + ":" + frmBulkPaymentKA.lblBranchCode.text;
                    temp.p_AccessChannel = temp.p_AccessChannel + ":" + "BTELLER";
                    temp.p_PaymentMethod = temp.p_PaymentMethod + ":" + paymentMethod;
                    temp.p_SendSMSFlag = temp.p_SendSMSFlag + ":" + "N";
                    temp.TransferFlag = temp.TransferFlag + ":" + "P";
                }
                selFieldsPrepaid = selFieldsPrepaid + 1;
                //selData.push(bulkPaymentData[i]);
                selDataPrepaid.push(bulkPaymentData[i]);
                //}
            } else {
                //if (bulkPaymentData[i].btnSetting.text === "p"){
                if (selFields === "0" || selFields == 0) {
                    tempPostpaid.custid = tempPostpaid.custid + "" + custid;
                    tempPostpaid.lang = tempPostpaid.lang + "" + langObj;
                    tempPostpaid.fromAccountNumber = tempPostpaid.fromAccountNumber + "" + accountNumber;
                    tempPostpaid.SourceBranchCode = tempPostpaid.SourceBranchCode + "" + branchCode;
                    tempPostpaid.p_BillerCode = tempPostpaid.p_BillerCode + "" + bulkPaymentData[i].BillerCode;
                    tempPostpaid.p_ServiceType = tempPostpaid.p_ServiceType + "" + bulkPaymentData[i].ServiceType;
                    tempPostpaid.p_BillingNo = tempPostpaid.p_BillingNo + "" + bulkPaymentData[i].BillingNumber.text;
                    tempPostpaid.p_BillNo = tempPostpaid.p_BillNo + "" + bulkPaymentData[i].BillingNumber.text;
                    tempPostpaid.p_DueAmt = tempPostpaid.p_DueAmt + "" + bulkPaymentData[i].hiddueamount;
                    tempPostpaid.amount = tempPostpaid.amount + "" + bulkPaymentData[i].hiddueamount;
                    tempPostpaid.p_AccessChannel = tempPostpaid.p_AccessChannel + "" + "MOBILE";
                    tempPostpaid.p_PaymentMethod = tempPostpaid.p_PaymentMethod + "" + paymentMethod;
                    tempPostpaid.p_SendSMSFlag = tempPostpaid.p_SendSMSFlag + "" + "N";
                    tempPostpaid.p_CustInfoFlag = tempPostpaid.p_CustInfoFlag + "" + "Y";
                    tempPostpaid.p_IdType = tempPostpaid.p_IdType + "" + "NAT";
                    tempPostpaid.p_ID = tempPostpaid.p_ID + "" + "9861033442";
                    tempPostpaid.p_Nation = tempPostpaid.p_Nation + "" + "JO";
                    tempPostpaid.p_payment_Status = tempPostpaid.p_payment_Status + "" + "BillNew";
                    tempPostpaid.p_MobileNo = tempPostpaid.p_MobileNo + "" + "962797545415";
                    tempPostpaid.fees = tempPostpaid.fees + "" + bulkPaymentData[i].feesamt;
                    tempPostpaid.p_Fees_On_Biller = tempPostpaid.p_Fees_On_Biller + "" + bulkPaymentData[i].feesonbiller;
                    tempPostpaid.TransferFlag = tempPostpaid.TransferFlag + "" + "E";
                    tempPostpaid.p_channel = tempPostpaid.p_channel + "" + "MOBILE";
                    tempPostpaid.p_user_id = tempPostpaid.p_user_id + "" + "BOJMOB";
                } else {
                    tempPostpaid.custid = tempPostpaid.custid + ":" + custid;
                    tempPostpaid.lang = tempPostpaid.lang + ":" + langObj;
                    tempPostpaid.fromAccountNumber = tempPostpaid.fromAccountNumber + ":" + accountNumber;
                    tempPostpaid.SourceBranchCode = tempPostpaid.SourceBranchCode + ":" + branchCode;
                    tempPostpaid.p_BillerCode = tempPostpaid.p_BillerCode + ":" + bulkPaymentData[i].BillerCode;
                    tempPostpaid.p_ServiceType = tempPostpaid.p_ServiceType + ":" + bulkPaymentData[i].ServiceType;
                    tempPostpaid.p_BillingNo = tempPostpaid.p_BillingNo + ":" + bulkPaymentData[i].BillingNumber.text;
                    tempPostpaid.p_BillNo = tempPostpaid.p_BillNo + ":" + bulkPaymentData[i].BillingNumber.text;
                    tempPostpaid.p_DueAmt = tempPostpaid.p_DueAmt + ":" + bulkPaymentData[i].hiddueamount;
                    tempPostpaid.amount = tempPostpaid.amount + ":" + bulkPaymentData[i].hiddueamount;
                    tempPostpaid.p_AccessChannel = tempPostpaid.p_AccessChannel + ":" + "MOBILE";
                    tempPostpaid.p_PaymentMethod = tempPostpaid.p_PaymentMethod + ":" + paymentMethod;
                    tempPostpaid.p_SendSMSFlag = tempPostpaid.p_SendSMSFlag + ":" + "N";
                    tempPostpaid.p_CustInfoFlag = tempPostpaid.p_CustInfoFlag + ":" + "Y";
                    tempPostpaid.p_IdType = tempPostpaid.p_IdType + ":" + "NAT";
                    tempPostpaid.p_ID = tempPostpaid.p_ID + ":" + "9861033442";
                    tempPostpaid.p_Nation = tempPostpaid.p_Nation + ":" + "JO";
                    tempPostpaid.p_payment_Status = tempPostpaid.p_payment_Status + ":" + "BillNew";
                    tempPostpaid.p_MobileNo = tempPostpaid.p_MobileNo + ":" + "962797545415";
                    tempPostpaid.fees = tempPostpaid.fees + ":" + bulkPaymentData[i].feesamt;
                    tempPostpaid.p_Fees_On_Biller = tempPostpaid.p_Fees_On_Biller + ":" + bulkPaymentData[i].feesonbiller;
                    tempPostpaid.TransferFlag = tempPostpaid.TransferFlag + ":" + "E";
                    tempPostpaid.p_channel = tempPostpaid.p_channel + ":" + "MOBILE";
                    tempPostpaid.p_user_id = tempPostpaid.p_user_id + ":" + "BOJMOB";
                }
                selFields = selFields + 1;
                //selData.push(bulkPaymentData[i]);
                selDataPostpaid.push(bulkPaymentData[i]);
                //}
            }
        }
        kony.print("tempPostpaid-->" + JSON.stringify(tempPostpaid));
        kony.print("tempPrepaid-->" + JSON.stringify(temp));
        temp.loop_count = selFieldsPrepaid;
        tempPostpaid.loop_count = selFields;
        return [temp, tempPostpaid];
    } catch (e) {
        kony.print("Exception_prepare_LOOPSERVICEDATA_BULK_PAYMENT_PAY ::" + e);
    }
}
var isPrepaid = false,
    isPostpaid = false;

function serv_BULK_PAYMENT() {
    try {
        selData = [];
        var servData = prepare_LOOPSERVICEDATA_BULK_PAYMENT_PAY();
        kony.print("servData ::" + JSON.stringify(servData));
        if (servData[0].custid !== "") {
            kony.print("inside prepaid");
            var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopBulkPrepaidPaymentServ");
            if (kony.sdk.isNetworkAvailable()) {
                isPrepaid = true;
                kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                appMFConfiguration.invokeOperation("LoopBulkPrepaidPaymentServ", {}, servData[0], PayPrepaidBulkPaymentSuccess, function(err) {
                    kony.print("Error Data ::" + JSON.stringify(err));
                });
            }
        }
        if (servData[1].custid !== "") {
            kony.print("inside postpaid");
            var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("LoopBulkPaymentPostpaid");
            if (kony.sdk.isNetworkAvailable()) {
                isPostpaid = true;
                kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
                appMFConfiguration.invokeOperation("LoopBulkPaymentPostpaid", {}, servData[1], PayPostPaidBulkPaymentSuccess, function(err) {
                    kony.print("Error Data ::" + JSON.stringify(err));
                });
            }
        }
    } catch (e) {
        kony.print("Exception_serv_BULK_PAYMENT ::" + e);
    }
}

function PayPrepaidBulkPaymentSuccess(res) {
    checkPostpaid = 0;
    var status = "",
        details = {};
    kony.print("Success PayPrepaidBulkPaymentSuccess111111-->" + JSON.stringify(res));
    kony.print("selDataPrepaid ::" + JSON.stringify(selDataPrepaid));
    kony.application.dismissLoadingScreen();
    if (kony.store.getItem("langPrefObj") === "en") {
        details = {
            left: "1%",
            width: "98%"
        };
    } else {
        details = {
            right: "1%",
            width: "98%"
        };
    }
    //frmBulkPaymentConfirmKA.bulkpaymentsegmant.setData([]);
    frmBulkPaymentConfirmKA.bulkpaymentsegmant.widgetDataMap = {
        payeename: "NickName",
        //flxIcon1: "initial",
        accountnumber: "billingTitle",
        lblBillerType: "BillerCode",
        dueAmount: "dueAmount",
        //btnBillsPayAccounts:"btnSetting",
        //flxDetails:"hiddueamount",
        paidAmount: "dueamount"
    };
    isPrepaid = false;
    if (res.LoopDataset !== undefined && res.LoopDataset.length > 0) {
        for (var i in res.LoopDataset) {
            kony.print("hassan Response " + res.LoopDataset);
            if (res.LoopDataset[i].ErrorCode === "00000" || res.LoopDataset[i].ErrorCode === "0" || res.LoopDataset[i].ErrorCode == 0 && res.LoopDataset[i].referenceId !== "" && res.LoopDataset[i].referenceId !== null) {
                status = {
                    "text": geti18Value("i18n.requeststatus.referencenumber") + ":\n" + res.LoopDataset[i].referenceId,
                    "skin": "lblsknNumberGreen"
                };
            } else {
                status = {
                    "text": getErrorMessage(res.LoopDataset[i].ErrorCode),
                    "skin": "lblsknNumberRed"
                };
            }
            selDataPrepaid[i].NickName.centerY = "";
            selDataPrepaid[i].billingTitle.centerY = "";
            selDataPrepaid[i].dueAmount.centerY = "";
            /*if(kony.store.getItem("langPrefObj") === "en")
	       selDataPrepaid[i].dueAmount.left = "60%";
       else
       	   selDataPrepaid[i].dueAmount.right = "60%";*/
            selDataPrepaid[i].dueamount = status;
            //selDataPrepaid[i].dueamount.width = "100%";
            selDataPrepaid[i].dueamount.centerY = "";
            //selDataPrepaid[i].btnSetting = {"isVisible":false};
            //selDataPrepaid[i].initial = {"isVisible":false};
            //selDataPrepaid[i].hiddueamount = details;
            selDataPrepaid[i].template = flxBulkPaymentConfirm;
            selData.push(selDataPrepaid[i]);
        }
        kony.application.dismissLoadingScreen();
        kony.print("selData prepaid-->" + JSON.stringify(selData));
    }
    if (isPrepaid === false && isPostpaid === false) {
        frmBulkPaymentConfirmKA.bulkpaymentsegmant.removeAll();
        frmBulkPaymentConfirmKA.flxBack.setVisibility(false);
        frmBulkPaymentConfirmKA.bulkpaymentsegmant.setData(selData);
        frmBulkPaymentConfirmKA.btnBillsScreen.text = geti18Value("i18n.common.gotoBillsScreen");
    }
    //serv_PayPostpaidBulkPayment();
}

function PayPostPaidBulkPaymentSuccess(res) {
    checkPostpaid = 0;
    var status = "",
        details = {};
    kony.print("Success PayPostPaidBulkPaymentSuccess-->" + JSON.stringify(res));
    kony.application.dismissLoadingScreen();
    if (kony.store.getItem("langPrefObj") === "en") {
        details = {
            left: "1%",
            width: "98%"
        };
    } else {
        details = {
            right: "1%",
            width: "98%"
        };
    }
    //frmBulkPaymentConfirmKA.bulkpaymentsegmant.setData([]);
    frmBulkPaymentConfirmKA.bulkpaymentsegmant.widgetDataMap = {
        payeename: "NickName",
        //     flxIcon1: "initial",
        accountnumber: "billingTitle",
        lblBillerType: "BillerCode",
        dueAmount: "dueAmount",
        //     btnBillsPayAccounts:"btnSetting",
        //     flxDetails:"hiddueamount",
        paidAmount: "dueamount"
    };
    isPostpaid = false;
    if (res.LoopDataset !== undefined && res.LoopDataset.length > 0) {
        for (var i in res.LoopDataset) {
            kony.print("hassan Response POSTPAID -->" + res.LoopDataset);
            if (res.LoopDataset[i].ErrorCode === "00000" || res.LoopDataset[i].ErrorCode === "0" || res.LoopDataset[i].ErrorCode == 0 && res.LoopDataset[i].referenceId !== "" && res.LoopDataset[i].referenceId !== null) {
                status = {
                    "text": geti18Value("i18n.requeststatus.referencenumber") + ":\n" + res.LoopDataset[i].referenceId,
                    "skin": "lblsknNumberGreen"
                };
            } else {
                status = {
                    "text": getErrorMessage(res.LoopDataset[i].ErrorCode),
                    "skin": "lblsknNumberRed"
                };
            }
            selDataPostpaid[i].NickName.centerY = "";
            selDataPostpaid[i].billingTitle.centerY = "";
            selDataPostpaid[i].dueAmount.centerY = "";
            /*if(kony.store.getItem("langPrefObj") === "en")
	       selDataPostpaid[i].dueAmount.left = "60%";
       else
       	   selDataPostpaid[i].dueAmount.right = "60%";*/
            selDataPostpaid[i].dueamount = status;
            //selDataPostpaid[i].dueamount.width = "100%";
            selDataPostpaid[i].dueamount.centerY = "";
            //selDataPostpaid[i].btnSetting = {"isVisible":false};
            //selDataPostpaid[i].initial = {"isVisible":false};
            //selDataPostpaid[i].hiddueamount = details;
            selDataPostpaid[i].template = flxBulkPaymentConfirm;
            selData.push(selDataPostpaid[i]);
        }
        kony.application.dismissLoadingScreen();
        kony.print("selData POSTPAID-->" + JSON.stringify(selData));
    }
    kony.print("selData POSTPAID-->" + JSON.stringify(selData));
    if (isPrepaid === false && isPostpaid === false) {
        frmBulkPaymentConfirmKA.bulkpaymentsegmant.removeAll();
        frmBulkPaymentConfirmKA.flxBack.setVisibility(false);
        frmBulkPaymentConfirmKA.bulkpaymentsegmant.setData(selData);
        frmBulkPaymentConfirmKA.btnBillsScreen.text = geti18Value("i18n.common.gotoBillsScreen");
    }
    //frmBulkPaymentConfirmKA.bulkpaymentsegmant.setData(selData);
    //frmBulkPaymentConfirmKA.show();
    //frmBulkPaymentKA.destroy();  
}

function validate_BULK_BILL_SELECTION() {
    try {
        if (!isEmpty(selectedIndex_BULK_PAYMENT)) {
            frmManagePayeeKA.btnPayNow.setVisibility(false);
            frmManagePayeeKA.btnBulkPay.setVisibility(true);
        } else {
            frmManagePayeeKA.btnPayNow.setVisibility(true);
            frmManagePayeeKA.btnBulkPay.setVisibility(false);
        }
    } catch (e) {
        kony.print("Exception_validate_BULK_BILL_SELECTION ::" + e);
    }
}

function handle_SELECTION_BULK_BILL(rowIndex) {
    try {
        var isMatchFound = false;
        for (var i = 0; i <= selectedIndex_BULK_PAYMENT.length; i++) {
            if (selectedIndex_BULK_PAYMENT.length == (i + 1)) selectedIndex_BULK_PAYMENT.pop();
            else if (isMatchFound) selectedIndex_BULK_PAYMENT[i] = selectedIndex_BULK_PAYMENT[i + 1];
            else if (parseInt(selectedIndex_BULK_PAYMENT[i]) == parseInt(rowIndex)) {
                selectedIndex_BULK_PAYMENT[i] = selectedIndex_BULK_PAYMENT[i + 1];
                isMatchFound = true;
            }
        }
    } catch (e) {
        kony.print("Exception_handle_SELECTION_BULK_BILL ::" + e);
    }
}

function show_COMISSION_CHARGES(response) {
    try {
        if (check_SUFFICIENT_BALANCE(response.p_debit_amt, kony.store.getItem("BillPayfromAcc").availableBalance)) {
            var amount = frmBulkPaymentKA.lblSelBillsAmt.text,
                exchangeRate = 0;
            amount = amount.substring(0, amount.length - 4);
            amount = parseFloat(amount.replace(/,/g, ""));
            frmBulkPaymentKA.lblVal.text = formatamountwithCurrency(response.p_debit_amt, kony.store.getItem("BillPayfromAcc").currencyCode) + " " + kony.store.getItem("BillPayfromAcc").currencyCode;
            exchangeRate = amount / parseFloat(response.p_debit_amt);
            exchangeRate = setDecimal(exchangeRate, 4);
            frmBulkPaymentKA.lblToCurr.text = exchangeRate + " " + kony.store.getItem("BillPayfromAcc").currencyCode;
            frmBulkPaymentKA.flxConversionAmt.setVisibility(true);
            frmBulkPaymentKA.lblNext.skin = "sknLblNextEnabled";
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
            frmBulkPaymentKA.flxConversionAmt.setVisibility(false);
            frmBulkPaymentKA.lblNext.skin = "sknLblNextDisabled";
            frmBulkPaymentKA.flxUnderlinePaymentModeBulk.skin = "sknFlxGreyLine";
        }
    } catch (e) {
        kony.print("Exception_show_COMISSION_CHARGES ::" + e);
    }
}

function check_SUFFICIENT_BALANCE(amountToPay, balance) {
    if (parseFloat(balance) > 0) return parseFloat(balance) >= parseFloat(amountToPay);
    return false;
}