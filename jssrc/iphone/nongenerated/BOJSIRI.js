function onPreShowfrmUserSettingsSiri() {
    if (isSiriEnabled == "N") {
        frmUserSettingsSiri.flxSwitchOffTouchLogin.isVisible = true;
        frmUserSettingsSiri.flxSwitchOnTouchLogin.isVisible = false;
        //1960
        frmUserSettingsSiri.flexContainerAccountTransfer.setVisibility(false);
        frmUserSettingsSiri.flexContainerAccountPayments.setVisibility(false);
        frmUserSettingsSiri.flxSetSiriLimit.setVisibility(false);
        frmUserSettingsSiri.btnUpdateLimit.setVisibility(false);
        frmUserSettingsSiri.lblAltText.text = geti18nkey("i18n.siri.enableSiri");
        //end
    } else {
        frmUserSettingsSiri.flxSwitchOffTouchLogin.isVisible = false;
        frmUserSettingsSiri.flxSwitchOnTouchLogin.isVisible = true;
        //1960
        frmUserSettingsSiri.flexContainerAccountTransfer.setVisibility(true);
        frmUserSettingsSiri.flexContainerAccountPayments.setVisibility(true);
        frmUserSettingsSiri.flxSetSiriLimit.setVisibility(true);
        frmUserSettingsSiri.btnUpdateLimit.setVisibility(true);
        kony.print("gblDefaultAccTransfer & gblDefaultAccPayment " + gblDefaultAccTransfer + "** " + gblDefaultAccPayment);
        if (gblDefaultAccTransfer !== "" && gblDefaultAccTransfer !== null && gblDefaultAccTransfer !== undefined) {
            frmUserSettingsSiri.lblAccountTransfer.setVisibility(true);
            frmUserSettingsSiri.lblAccountTransfer.text = gblDefaultAccTransfer;
            frmUserSettingsSiri.lblAccountTransfer1.setVisibility(true);
        } else {
            frmUserSettingsSiri.lblAccountTransfer.setVisibility(true);
            frmUserSettingsSiri.lblAccountTransfer.text = geti18nkey("i18n.siri.accountTransfer");
            frmUserSettingsSiri.lblAccountTransfer1.setVisibility(false);
        }
        if (gblDefaultAccPayment !== "" && gblDefaultAccPayment !== null && gblDefaultAccPayment !== undefined) {
            frmUserSettingsSiri.labelAccountPayments.setVisibility(true);
            frmUserSettingsSiri.labelAccountPayments.text = gblDefaultAccPayment;
            frmUserSettingsSiri.labelAccountPayments1.setVisibility(true);
        } else {
            frmUserSettingsSiri.labelAccountPayments.setVisibility(true);
            frmUserSettingsSiri.labelAccountPayments.text = geti18nkey("i18n.siri.accountPayment");
            frmUserSettingsSiri.labelAccountPayments1.setVisibility(false);
        }
        var lblAcctTransfer = frmUserSettingsSiri.lblAccountTransfer.text;
        var lblAcctPayment = frmUserSettingsSiri.labelAccountPayments.text;
        if (lblAcctTransfer != geti18nkey("i18n.siri.accountTransfer") && lblAcctPayment != geti18nkey("i18n.siri.accountPayment")) {
            frmUserSettingsSiri.btnUpdateLimit.setEnabled(true);
        } else {
            frmUserSettingsSiri.btnUpdateLimit.setEnabled(false);
        }
        kony.print("in preshow lblAcctTransfer and lblAcctPayment" + lblAcctTransfer + "**" + lblAcctPayment);
        frmUserSettingsSiri.lblAltText.text = geti18nkey("i18n.siri.disableSiri");
    }
    //end
    frmUserSettingsSiri.flxSiriinfo.setVisibility(false);
    if (siriTransactionLimit !== "" && siriTransactionLimit !== null && siriTransactionLimit !== undefined) {
        frmUserSettingsSiri.txtSiriLimit.text = siriTransactionLimit;
        animateLabel("UP", "lblSiriTransLimit", frmUserSettingsSiri.txtSiriLimit.text);
    } else {
        frmUserSettingsSiri.txtSiriLimit.text = Siri_Limit;
        animateLabel("UP", "lblSiriTransLimit", frmUserSettingsSiri.txtSiriLimit.text);
    }
    frmUserSettingsSiri.forceLayout();
}

function onPostShowfrmUserSettingsSiri() {
    if (siriTransactionLimit !== "" && siriTransactionLimit !== null && siriTransactionLimit !== undefined) {
        frmUserSettingsSiri.txtSiriLimit.text = siriTransactionLimit;
        animateLabel("UP", "lblSiriTransLimit", frmUserSettingsSiri.txtSiriLimit.text);
    } else {
        frmUserSettingsSiri.txtSiriLimit.text = Siri_Limit;
        animateLabel("UP", "lblSiriTransLimit", frmUserSettingsSiri.txtSiriLimit.text);
    }
    //1960
    if (gblDefaultAccTransfer !== "" && gblDefaultAccTransfer !== null && gblDefaultAccTransfer !== undefined) {
        frmUserSettingsSiri.lblAccountTransfer.setVisibility(true);
        frmUserSettingsSiri.lblAccountTransfer.text = gblDefaultAccTransfer;
        frmUserSettingsSiri.lblAccountTransfer1.setVisibility(true);
    } else {
        frmUserSettingsSiri.lblAccountTransfer.setVisibility(true);
        frmUserSettingsSiri.lblAccountTransfer.text = geti18nkey("i18n.siri.accountTransfer");
        frmUserSettingsSiri.lblAccountTransfer1.setVisibility(false);
    }
    if (gblDefaultAccPayment !== "" && gblDefaultAccPayment !== null && gblDefaultAccPayment !== undefined) {
        frmUserSettingsSiri.labelAccountPayments.setVisibility(true);
        frmUserSettingsSiri.labelAccountPayments.text = gblDefaultAccPayment;
        frmUserSettingsSiri.labelAccountPayments1.setVisibility(true);
    } else {
        frmUserSettingsSiri.labelAccountPayments.setVisibility(true);
        frmUserSettingsSiri.labelAccountPayments.text = geti18nkey("i18n.siri.accountPayment");
        frmUserSettingsSiri.labelAccountPayments1.setVisibility(false);
    }
    var lblAcctTransfer = frmUserSettingsSiri.lblAccountTransfer.text;
    var lblAcctPayment = frmUserSettingsSiri.labelAccountPayments.text;
    if (lblAcctTransfer != geti18nkey("i18n.siri.accountTransfer") && lblAcctPayment != geti18nkey("i18n.siri.accountPayment")) {
        frmUserSettingsSiri.btnUpdateLimit.setEnabled(true);
    } else {
        frmUserSettingsSiri.btnUpdateLimit.setEnabled(false);
    }
    kony.print("in postshow lblAcctTransfer and lblAcctPayment" + lblAcctTransfer + "**" + lblAcctPayment);
    //end
    frmUserSettingsSiri.forceLayout();
}

function onTextChangetxtSiriLimit() {
    try {
        if (frmUserSettingsSiri.lblSiriTransLimit.top !== "15%") animateLabel("UP", "lblSiriTransLimit", frmUserSettingsSiri.txtSiriLimit.text);
        var currency = getDefaultTransferCurr();
        kony.print("currency" + currency);
        var limit = 2;
        if (kony.string.equalsIgnoreCase(currency, "JOD") || kony.string.equalsIgnoreCase(currency, "BHD")) limit = 3;
        var value = fixDecimal(frmUserSettingsSiri.txtSiriLimit.text, limit);
        if (value.indexOf(".") == -1 && value.length > 5) {
            frmUserSettingsSiri.txtSiriLimit.text = value.substring(0, 5);
        } else {
            frmUserSettingsSiri.txtSiriLimit.text = value;
        }
        if (frmUserSettingsSiri.txtSiriLimit.text !== "" && frmUserSettingsSiri.txtSiriLimit.text !== null) frmUserSettingsSiri.lblClose1.setVisibility(true);
        else frmUserSettingsSiri.lblClose1.setVisibility(false);
    } catch (err) {
        kony.print("catch of onTextChangetxtSiriLimit :: " + err);
    }
}

function onDonetxtSiriLimit() {
    try {
        animateLabel("UP", "lblSiriTransLimit", frmUserSettingsSiri.txtSiriLimit.text);
        var currency = getDefaultTransferCurr();
        kony.print("currency" + currency);
        if (kony.string.equalsIgnoreCase(currency, "JOD") || kony.string.equalsIgnoreCase(currency, "BHD")) {
            limit = 3;
        } else {
            limit = 2;
        }
        var value = formatAmountwithcomma(frmUserSettingsSiri.txtSiriLimit.text, limit);
        frmUserSettingsSiri.txtSiriLimit.text = value;
    } catch (err) {
        kony.print("catch of onDonetxtSiriLimit :: " + err);
    }
}

function onTouchStarttxtSiriLimit() {
    try {
        if (!isEmpty(frmUserSettingsSiri.txtSiriLimit.text)) {
            frmUserSettingsSiri.borderBottom1.skin = "skntextFieldDividerGreen";
        } else {
            frmUserSettingsSiri.borderBottom1.skin = "skntextFieldDivider";
        }
        frmUserSettingsSiri.lblClose1.setVisibility(true);
    } catch (err) {
        kony.print("catch of onTouchStarttxtSiriLimit :: " + err);
    }
}

function askSiriPermission() {
    if (isAndroid()) {
        enableSiri();
    } else {
        com.siriSetup.enableSiriPermission(askSiriPermissionCallBack);
    }
}

function askSiriPermissionCallBack(response) {
    popupCommonAlertDimiss();
    kony.print("askSiriPermissionCallBack :: " + JSON.stringify(response));
    if (response.indexOf("YES") > -1) {
        enableSiri();
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.SIRI.enableMsgSettings"), popupCommonAlertDimiss, "");
    }
}

function enableSiri() {
    popupCommonAlertDimiss();
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    if (kony.sdk.isNetworkAvailable()) {
        var devID = deviceid;
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        var scopeObj = this;
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
        var Token = Dvfn(kony.store.getItem("soft_token"));
        kony.print("Perf Log: Token - start->" + Token);
        var FlowFlag = "N";
        if (frmUserSettingsSiri.flxSwitchOffTouchLogin.isVisible) {
            FlowFlag = "Y";
        }
        dataObject.addField("Language", Language);
        dataObject.addField("custId", custid);
        dataObject.addField("FlowFlag", FlowFlag);
        dataObject.addField("deviceId", devID);
        dataObject.addField("Token", Token);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("serviceOptions for enableSiri  : " + JSON.stringify(serviceOptions));
        modelObj.customVerb("SetSiriMethod", serviceOptions, enableSiriSuccessCallBack, enableSiriErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function enableSiriSuccessCallBack(response) {
    kony.print("enableSiriSuccessCallBack :: " + JSON.stringify(response));
    if (response.statusCode !== null && response.statusCode !== "" && response.statusCode == "S0024" && response.statusType == "S") {
        if (frmUserSettingsSiri.flxSwitchOffTouchLogin.isVisible) {
            frmUserSettingsSiri.flxSwitchOffTouchLogin.setVisibility(false);
            frmUserSettingsSiri.flxSwitchOnTouchLogin.setVisibility(true);
            //1960
            frmUserSettingsSiri.flexContainerAccountTransfer.setVisibility(true);
            frmUserSettingsSiri.flexContainerAccountPayments.setVisibility(true);
            frmUserSettingsSiri.flxSetSiriLimit.setVisibility(true);
            frmUserSettingsSiri.btnUpdateLimit.setVisibility(true);
            frmUserSettingsSiri.lblAltText.text = geti18nkey("i18n.siri.disableSiri");
            var lblAcctTransfer = frmUserSettingsSiri.lblAccountTransfer.text;
            var lblAcctPayment = frmUserSettingsSiri.labelAccountPayments.text;
            if (lblAcctTransfer != geti18nkey("i18n.siri.accountTransfer") && lblAcctPayment != geti18nkey("i18n.siri.accountPayment")) {
                frmUserSettingsSiri.btnUpdateLimit.setEnabled(true);
            } else {
                frmUserSettingsSiri.btnUpdateLimit.setEnabled(false);
            }
            //end
            isSiriEnabled = "Y";
            customAlertPopup(geti18Value("i18n.common.success"), geti18Value("i18n.Siri.SuccessEnableText"), showSiriUsage, "");
        } else {
            customAlertPopup(geti18Value("i18n.common.success"), geti18Value("i18n.Siri.SuccessdisableText"), popupCommonAlertDimiss, "");
            frmUserSettingsSiri.flxSwitchOffTouchLogin.setVisibility(true);
            frmUserSettingsSiri.flxSwitchOnTouchLogin.setVisibility(false);
            //1960
            frmUserSettingsSiri.flexContainerAccountTransfer.setVisibility(false);
            frmUserSettingsSiri.flexContainerAccountPayments.setVisibility(false);
            frmUserSettingsSiri.flxSetSiriLimit.setVisibility(false);
            frmUserSettingsSiri.btnUpdateLimit.setVisibility(false);
            frmUserSettingsSiri.lblAltText.text = geti18nkey("i18n.siri.enableSiri");
            //end
            isSiriEnabled = "N";
        }
    } else {
        customAlertPopup(geti18Value("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function showSiriUsage() {
    popupCommonAlertDimiss();
    frmUserSettingsSiri.flxSiriinfo.setVisibility(true);
}

function enableSiriErrorCallback(response) {
    kony.print("enableSiriErrorCallback-->" + JSON.stringify(response));
    customAlertPopup(geti18Value("i18n.NUO.Error"), geti18Value("i18n.common.defaultErrorMsg"), popupCommonAlertDimiss, "");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}
//first siri should be on
//second default account should be set
//third some limit has to be entered
function onClickbtnUpdateLimit() {
    if (isSiriEnabled === "Y") {
        //if(!isEmpty(gblDefaultAccTransfer)){
        //1960 fix need to uncomment after service works fine
        if (!isEmpty(frmUserSettingsSiri.lblAccountTransfer.text) && !isEmpty(frmUserSettingsSiri.labelAccountPayments.text)) {
            if (!isEmpty(frmUserSettingsSiri.txtSiriLimit.text)) {
                setSiriLimitcall(frmUserSettingsSiri.txtSiriLimit.text);
            } else {
                customAlertPopup(geti18Value("i18n.common.alert"), geti18Value("i18n.Siri.Limtiinvaliderror"), popupCommonAlertDimiss, "");
            }
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.Siri.addaccount"), popupCommonAlertDimiss, "");
        }
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.SIRI.trasnferLimitenable"), popupCommonAlertDimiss, "");
    }
}

function setSiriLimitcall(SiriLimit) {
    popupCommonAlertDimiss();
    if (kony.sdk.isNetworkAvailable()) {
        kony.print("Inside setSiriLimitcall :: " + SiriLimit);
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        var scopeObj = this;
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("DeviceRegistration", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");
        //1960 fix
        var accTransfer = frmUserSettingsSiri.lblAccountTransfer.text;
        var accPayment = frmUserSettingsSiri.labelAccountPayments.text;
        kony.print("accTransfer and accPayment " + accTransfer + " " + accPayment);
        dataObject.addField("Language", Language);
        dataObject.addField("custId", custid);
        dataObject.addField("SiriLimit", SiriLimit);
        dataObject.addField("accTransfer", accTransfer);
        dataObject.addField("accPayment", accPayment);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("serviceOptions for setSiriLimitcall :: " + JSON.stringify(serviceOptions));
        modelObj.customVerb("SetSiriTransaction", serviceOptions, SetSiriTransactionSuccessCallBack, SetSiriTransactionErrorCallback);
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}

function SetSiriTransactionSuccessCallBack(response) {
    kony.print("SetSiriTransactionSuccessCallBack--> :: " + JSON.stringify(response));
    if (response.statusCode !== null && response.statusCode !== "" && response.statusCode == "S0024" && response.statusType == "S") {
        siriTransactionLimit = frmUserSettingsSiri.txtSiriLimit.text;
        //1960
        gblDefaultAccTransfer = frmUserSettingsSiri.lblAccountTransfer.text;
        gblDefaultAccPayment = frmUserSettingsSiri.labelAccountPayments.text;
        kony.print("Account transfer and payments after save from Siri " + gblDefaultAccTransfer + " " + gblDefaultAccPayment);
        //end
        customAlertPopup(geti18Value("i18n.common.success"), geti18Value("i18n.Siri.SuccessupdateLimit"), popupCommonAlertDimiss, "");
    } else {
        customAlertPopup(geti18Value("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    }
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function SetSiriTransactionErrorCallback(response) {
    kony.print("SetSiriTransactionErrorCallback--> :: " + JSON.stringify(response));
    customAlertPopup(geti18Value("i18n.NUO.Error"), geti18Value("i18n.common.defaultErrorMsg"), popupCommonAlertDimiss, "");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}