var frmAddNewPayeeKAConfig = {
    "formid": "frmAddNewPayeeKA",
    "frmAddNewPayeeKA": {
        "entity": "Payee",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        },
    },
    "lblLanguage": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "Label",
            "field": "lang"
        }
    },
    "lblType": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "Label",
            "field": "p_txn_type"
        }
    },
    "tbxBillerName": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_BillerCode"
        }
    },
    "tbxServiceType": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_ServiceType"
        }
    },
    "tbxBillerNumber": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_BillingNo"
        }
    },
    "tbxIDType": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_IdType"
        }
    },
    "tbxBillerID": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_ID"
        }
    },
    "tbxNationality": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_Nation"
        }
    },
    "tbxPhoneNo": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_Phone"
        }
    },
    "tbxAddress": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_Address1"
        }
    },
    "tbxNickName": {
        "fieldprops": {
            "entity": "Payee",
            "widgettype": "TextField",
            "field": "p_NickName"
        }
    }
};