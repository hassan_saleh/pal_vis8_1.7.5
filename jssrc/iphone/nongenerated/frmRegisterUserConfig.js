var frmRegisterUserConfig = {
    "formid": "frmRegisterUser",
    "frmRegisterUser": {
        "entity": "NewUser",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        },
    },
    "lblCard": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "NewUser",
            "field": "cardnum"
        }
    },
    "lblPin": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "NewUser",
            "field": "pin"
        }
    },
    "lblLanguage": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "NewUser",
            "field": "Language"
        }
    }
}; //Type your code here