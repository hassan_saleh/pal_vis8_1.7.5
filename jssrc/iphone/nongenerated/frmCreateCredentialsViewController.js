/**
 * frmCreateCredentialsViewController
 * View Controller of frmCreateCredentials
 */
kony = kony || {};
kony.rb = kony.rb || {};
kony.rb.frmCreateCredentialsViewController = function() {};
/**
 * function createCredentialsInit
 * initializing onEndEditing and onDone functionality to check if username exits
 */
kony.rb.frmCreateCredentialsViewController.prototype.createCredentialsInit = function() {
    frmCreateCredentials.txtboxUsernameKA.onEndEditing = function() {
        frmCreateCredentials.flxUsernameRules.setVisibility(false);
        if (frmCreateCredentials.txtboxUsernameKA.text !== "") {
            if (kony.retailBanking.util.validation.isValidUsername(frmCreateCredentials.txtboxUsernameKA.text)) {
                frmCreateCredentials.lblUsernameErr.setVisibility(false);
                var params = {
                    "userName": frmCreateCredentials.txtboxUsernameKA.text
                };
                ShowLoadingScreen();
                var checkAvailability = applicationManager.getNewUserPresentationController();
                checkAvailability.checkUserAvailability(params);
            } else {
                frmCreateCredentials.lblAvailable.setVisibility(false);
                frmCreateCredentials.lblNotAvailable.setVisibility(false);
                frmCreateCredentials.lblUsernameErr.setVisibility(true);
            }
        }
    };
    frmCreateCredentials.answerFieldPassword.onEndEditing = function() {
        frmCreateCredentials.flxPasswordRules.setVisibility(false);
    };
    frmCreateCredentials.PasswordReEnter.onEndEditing = function() {
        if (frmCreateCredentials.PasswordReEnter.text !== "") {
            if (validateReenterPassword(frmCreateCredentials.answerFieldPassword.text, frmCreateCredentials.PasswordReEnter.text)) {
                frmCreateCredentials.imgTick.src = "tickblue.png";
                frmCreateCredentials.lblPasswordNotMatch.setVisibility(false);
            } else {
                frmCreateCredentials.imgTick.src = "tickgray.png";
                frmCreateCredentials.lblPasswordNotMatch.setVisibility(true);
            }
        }
    };
    frmCreateCredentials.PasswordReEnter.onDone = function() {
        if (frmCreateCredentials.PasswordReEnter.text !== "") {
            if (frmCreateCredentials.PasswordReEnter.text !== "" && validateReenterPassword(frmCreateCredentials.answerFieldPassword.text, frmCreateCredentials.PasswordReEnter.text)) {
                frmCreateCredentials.imgTick.src = "tickblue.png";
                frmCreateCredentials.lblPasswordNotMatch.setVisibility(false);
            } else {
                frmCreateCredentials.imgTick.src = "tickgray.png";
                frmCreateCredentials.lblPasswordNotMatch.setVisibility(true);
            }
        }
    };
};
/**
 * function onClickCreateAndContinue
 * on click of continue in frmCreateCredentials
 */
kony.rb.frmCreateCredentialsViewController.prototype.onClickCreateAndContinue = function() {
    this.username = frmCreateCredentials.txtboxUsernameKA.text;
    this.password = frmCreateCredentials.answerFieldPassword.text;
    this.reenter = frmCreateCredentials.PasswordReEnter.text;
    validateCredentials(this.username, this.password, this.reenter);
};
/**
 * function clearSkinsOfCreateCredentials
 * clear skins of frmCreateCredentials
 */
kony.rb.frmCreateCredentialsViewController.prototype.clearSkinsOfCreateCredentials = function() {
    frmCreateCredentials.txtboxUsernameKA.text = "";
    frmCreateCredentials.answerFieldPassword.text = "";
    frmCreateCredentials.PasswordReEnter.text = "";
    frmCreateCredentials.lblUserID.skin = "sknLatoRegular7C7C7CKA";
    frmCreateCredentials.lblLineKA2.skin = "sknLineEDEDEDKA";
    frmCreateCredentials.lblPassword.skin = "sknLatoRegular7C7C7CKA";
    frmCreateCredentials.lblLineKA.skin = "sknLineEDEDEDKA";
    frmCreateCredentials.lblReEnterPassword.skin = "sknLatoRegular7C7C7CKA";
    frmCreateCredentials.lblLineKA3.skin = "sknLineEDEDEDKA";
    frmCreateCredentials.imgTick.src = "tickgray.png";
    frmCreateCredentials.ImgRule1.src = "combinedshape.png";
    frmCreateCredentials.ImgRule2.src = "combinedshape.png";
    frmCreateCredentials.imgRule3.src = "combinedshape.png";
    frmCreateCredentials.lblRule1.skin = "sknlblD0021BregularKA";
    frmCreateCredentials.lblRule2.skin = "sknlblD0021BregularKA";
    frmCreateCredentials.lblRule3.skin = "sknlblD0021BregularKA";
    frmCreateCredentials.ImgUserRule1.src = "combinedshape.png";
    frmCreateCredentials.lblUsernameRule1.skin = "sknlblD0021BregularKA";
    frmCreateCredentials.lblPasswordNotMatch.setVisibility(false);
};
/**
 * function validatePassword
 * check validation for password
 */
function validatePassword(password) {
    var check = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if ((password.match(check))) {
        return true;
    } else {
        return false;
    }
}
/**
 * function validateReenterPassword
 * check validation for reenter password
 */
function validateReenterPassword(password, reenter) {
    if (password !== "" && reenter !== "" && password.length == reenter.length) {
        if (password == reenter) {
            return true;
        }
    } else {
        return false;
    }
}
/**
 * function validateCredentials
 * validate credentials
 */
function validateCredentials(username, password, reenter) {
    if (username) {
        frmCreateCredentials.lblUserID.skin = "sknLatoRegular7C7C7CKA";
        frmCreateCredentials.lblLineKA2.skin = "sknLineEDEDEDKA";
    } else {
        frmCreateCredentials.lblUserID.skin = "sknLblFd0021bLatoRegular";
        frmCreateCredentials.lblLineKA2.skin = "sknFlxBGe2e2e2B1pxD0021B";
    }
    if (password && isValidPassword(password)) {
        frmCreateCredentials.lblPassword.skin = "sknLatoRegular7C7C7CKA";
        frmCreateCredentials.lblLineKA.skin = "sknLineEDEDEDKA";
    } else {
        frmCreateCredentials.lblPassword.skin = "sknLblFd0021bLatoRegular";
        frmCreateCredentials.lblLineKA.skin = "sknFlxBGe2e2e2B1pxD0021B";
    }
    if (reenter && validateReenterPassword(password, reenter)) {
        frmCreateCredentials.lblReEnterPassword.skin = "sknLatoRegular7C7C7CKA";
        frmCreateCredentials.lblLineKA3.skin = "sknLineEDEDEDKA";
    } else {
        frmCreateCredentials.lblReEnterPassword.skin = "sknLblFd0021bLatoRegular";
        frmCreateCredentials.lblLineKA3.skin = "sknFlxBGe2e2e2B1pxD0021B";
        frmCreateCredentials.lblPasswordNotMatch.setVisibility(true);
    }
    if (username && isValidPassword(password) && validateReenterPassword(password, reenter)) {
        ShowLoadingScreen();
        var CreateCredentialsPC = applicationManager.getNewUserPresentationController();
        CreateCredentialsPC.createCredentials(username, password);
    }
}
/**
 * function onClickCancel
 * on click cancel in frmNewUserOnboardVerificationKA
 */
kony.rb.frmCreateCredentialsViewController.prototype.onClickCancel = function() {
    var PC = applicationManager.getNewUserPresentationController();
    PC.navToLogin();
};
/**
 * function resetUIIfUserExists
 * if username already exists
 */
kony.rb.frmCreateCredentialsViewController.prototype.resetUIIfUserNameExists = function() {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmCreateCredentials.lblNotAvailable.setVisibility(false);
    frmCreateCredentials.lblAvailable.setVisibility(true);
};
/**
 * function resetUIIfUserDoesNotExists
 * if username does not exist
 */
kony.rb.frmCreateCredentialsViewController.prototype.resetUIIfUserNameDoesNotExists = function() {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmCreateCredentials.lblNotAvailable.setVisibility(true);
    frmCreateCredentials.lblAvailable.setVisibility(false);
};
/**
 * function onTextChangeUserID
 * To clear existing labels if username textbox getting changed
 */
kony.rb.frmCreateCredentialsViewController.prototype.onTextChangeUserID = function() {
    frmCreateCredentials.lblNotAvailable.setVisibility(false);
    frmCreateCredentials.lblAvailable.setVisibility(false);
    frmCreateCredentials.flxUsernameRules.setVisibility(true);
    frmCreateCredentials.lblUsernameErr.setVisibility(false);
    var PC = applicationManager.getNewUserPresentationController();
    PC.resetSkins(kony.application.getCurrentForm(), "lblUserID", "lblLineKA2");
    if (kony.retailBanking.util.validation.isValidUsername(frmCreateCredentials.txtboxUsernameKA.text)) {
        frmCreateCredentials.ImgUserRule1.src = "check1x.png";
        frmCreateCredentials.lblUsernameRule1.skin = "sknlblF9c9c9c";
    } else {
        frmCreateCredentials.ImgUserRule1.src = "combinedshape.png";
        frmCreateCredentials.lblUsernameRule1.skin = "sknlblD0021BregularKA";
    }
};
/**
 * function onTextChangeEnterPassword
 * To enable greeen tick if password and reenter password matches
 */
kony.rb.frmCreateCredentialsViewController.prototype.onTextChangeEnterPassword = function() {
    var password = frmCreateCredentials.answerFieldPassword.text;
    var PC = applicationManager.getNewUserPresentationController();
    PC.resetSkins(kony.application.getCurrentForm(), "lblPassword", "lblLineKA");
    if (password !== "" && password !== null) {
        if (validateReenterPassword(password, frmCreateCredentials.PasswordReEnter.text)) {
            frmCreateCredentials.imgTick.src = "tickblue.png";
            frmCreateCredentials.lblPasswordNotMatch.setVisibility(false);
        } else {
            frmCreateCredentials.imgTick.src = "tickgray.png";
        }
        if (password && countMatch(password)) {
            frmCreateCredentials.ImgRule1.src = "check1x.png";
            frmCreateCredentials.lblRule1.skin = "sknlblF9c9c9c";
        } else {
            frmCreateCredentials.ImgRule1.src = "combinedshape.png";
            frmCreateCredentials.lblRule1.skin = "sknlblD0021BregularKA";
        }
        if (password !== "" && isInvalidCharacterExists(password)) {
            frmCreateCredentials.imgRule3.src = "check1x.png";
            frmCreateCredentials.lblRule3.skin = "sknlblF9c9c9c";
        } else {
            frmCreateCredentials.imgRule3.src = "combinedshape.png";
            frmCreateCredentials.lblRule3.skin = "sknlblD0021BregularKA";
        }
        if (password && expressionMatch(password)) {
            frmCreateCredentials.ImgRule2.src = "check1x.png";
            frmCreateCredentials.lblRule2.skin = "sknlblF9c9c9c";
        } else {
            frmCreateCredentials.ImgRule2.src = "combinedshape.png";
            frmCreateCredentials.lblRule2.skin = "sknlblD0021BregularKA";
        }
    }
    frmCreateCredentials.flxPasswordRules.setVisibility(true);
};
/**
 * function onTextChangeReEnterPassword
 * To enable greeen tick if password and reenter password matches
 */
kony.rb.frmCreateCredentialsViewController.prototype.onTextChangeReEnterPassword = function() {
    var PC = applicationManager.getNewUserPresentationController();
    PC.resetSkins(kony.application.getCurrentForm(), "lblReEnterPassword", "lblLineKA3");
    if (validateReenterPassword(frmCreateCredentials.answerFieldPassword.text, frmCreateCredentials.PasswordReEnter.text)) {
        frmCreateCredentials.imgTick.src = "tickblue.png";
        frmCreateCredentials.lblPasswordNotMatch.setVisibility(false);
    } else {
        frmCreateCredentials.imgTick.src = "tickgray.png";
    }
};
//Invalid Characters- &, %, <, >, +, ‘, \, =, Pipe, Space and also values like 11111111 should not be allowed
function isValidPassword(password) {
    if (password === null) return false;
    else if (password.indexOf(" ") != -1) {
        return false;
    } else if (!countMatch(password)) {
        return false;
    } else if (!isInvalidCharacterExists(password)) {
        return false;
    } else if (!checkforUniqueCharacters(password)) {
        return false;
    } else if (!expressionMatch(password)) {
        return false;
    }
    return true;
}

function isInvalidCharacterExists(value) {
    var regexp = "&%<>\/\+'=|\\";
    for (var i = 0; i < regexp.length; i++) {
        if (value.indexOf(regexp[i]) != -1) {
            return false;
        }
    }
    return true;
}

function checkforUniqueCharacters(val) {
    var start = val[0];
    for (var i = 1; i < val.length; i++) {
        if (start != val[i]) {
            return true;
        }
    }
    return false;
}

function expressionMatch(val) {
    var check = /^(?=.*\d)(?=.*[^\w\s&%<>\/\+'=|\\])(?=.*[a-z])(?=.*[A-Z]).*$/;
    if (!val.match(check)) {
        return false;
    }
    return true;
}

function countMatch(password) {
    if (password.length < 8 || password.length > 24) {
        return false;
    }
    return true;
}