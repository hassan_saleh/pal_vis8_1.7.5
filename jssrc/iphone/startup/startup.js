//startup.js
var globalhttpheaders = {};
var appConfig = {
    appId: "BOJMobileBanking",
    appName: "BOJ Mobile",
    appVersion: "1.7.5.2",
    isturlbase: "https://palmobwebuat.bankofjordan.com.ps/services",
    isDebug: true,
    isMFApp: true,
    appKey: "a36aeb93aaf08a535b72261fc701de7c",
    appSecret: "f93082270834165816204d01ab574792",
    serviceUrl: "https://palmobwebuat.bankofjordan.com.ps/authService/100000004/appconfig",
    svcDoc: {
        "selflink": "https://palmobwebuat.bankofjordan.com.ps/authService/100000004/appconfig",
        "messagingsvc": {
            "appId": "cb14d3a6-6813-4388-98c3-a44882aa8725",
            "url": "https://palmobwebuat.bankofjordan.com.ps/kpns/api/v1"
        },
        "integsvc": {
            "BOJGetAccDetailsNode": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetAccDetailsNode",
            "prAddBeneficiary": "https://palmobwebuat.bankofjordan.com.ps/services/prAddBeneficiary",
            "prGetAtmTransactionDetails": "https://palmobwebuat.bankofjordan.com.ps/services/prGetAtmTransactionDetails",
            "BOJPrfChngSms": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrfChngSms",
            "BranchATMLocator": "https://palmobwebuat.bankofjordan.com.ps/services/BranchATMLocator",
            "ATMLocator": "https://palmobwebuat.bankofjordan.com.ps/services/ATMLocator",
            "OTPController": "https://palmobwebuat.bankofjordan.com.ps/services/OTPController",
            "BOJDebitCrdTxnList": "https://palmobwebuat.bankofjordan.com.ps/services/BOJDebitCrdTxnList",
            "BOJPrExternalTransfer": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrExternalTransfer",
            "BOJGetCrCardDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetCrCardDetails",
            "BOJJmpRegister": "https://palmobwebuat.bankofjordan.com.ps/services/BOJJmpRegister",
            "BOJCancelCreditCard": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCancelCreditCard",
            "getTncProfileData": "https://palmobwebuat.bankofjordan.com.ps/services/getTncProfileData",
            "GetAccountListService": "https://palmobwebuat.bankofjordan.com.ps/services/GetAccountListService",
            "BOJsendUserName": "https://palmobwebuat.bankofjordan.com.ps/services/BOJsendUserName",
            "iWatchBOJPrGetTdDetails": "https://palmobwebuat.bankofjordan.com.ps/services/iWatchBOJPrGetTdDetails",
            "BojprDcActivation": "https://palmobwebuat.bankofjordan.com.ps/services/BojprDcActivation",
            "CancelCard": "https://palmobwebuat.bankofjordan.com.ps/services/CancelCard",
            "BOJBeneBranchList": "https://palmobwebuat.bankofjordan.com.ps/services/BOJBeneBranchList",
            "BOJRestServices": "https://palmobwebuat.bankofjordan.com.ps/services/BOJRestServices",
            "BOJPpPayment": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPpPayment",
            "BOJprCcActivation": "https://palmobwebuat.bankofjordan.com.ps/services/BOJprCcActivation",
            "BOJCnfOpenNewAcc": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCnfOpenNewAcc",
            "BOJSendSMSMobile": "https://palmobwebuat.bankofjordan.com.ps/services/BOJSendSMSMobile",
            "BojUserReg": "https://palmobwebuat.bankofjordan.com.ps/services/BojUserReg",
            "BOJATMList": "https://palmobwebuat.bankofjordan.com.ps/services/BOJATMList",
            "AllCardList": "https://palmobwebuat.bankofjordan.com.ps/services/AllCardList",
            "iWatchBOJPrGetLoanAccDetails": "https://palmobwebuat.bankofjordan.com.ps/services/iWatchBOJPrGetLoanAccDetails",
            "InformationSupport": "https://palmobwebuat.bankofjordan.com.ps/services/InformationSupport",
            "BOJEfwPaymentConfirm": "https://palmobwebuat.bankofjordan.com.ps/services/BOJEfwPaymentConfirm",
            "testcomm": "https://palmobwebuat.bankofjordan.com.ps/services/testcomm",
            "prGetBeneficiaryDetails": "https://palmobwebuat.bankofjordan.com.ps/services/prGetBeneficiaryDetails",
            "BOJGetDbCardDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetDbCardDetails",
            "pushNotificationsAuthService": "https://palmobwebuat.bankofjordan.com.ps/services/pushNotificationsAuthService",
            "BOJGetPalpayBillDetailsOtp": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayBillDetailsOtp",
            "OTPSMSController": "https://palmobwebuat.bankofjordan.com.ps/services/OTPSMSController",
            "PushNotification": "https://palmobwebuat.bankofjordan.com.ps/services/PushNotification",
            "prDcLinkedAccount": "https://palmobwebuat.bankofjordan.com.ps/services/prDcLinkedAccount",
            "BOJCnfSiCancellation": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCnfSiCancellation",
            "BOJPrGetTdDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrGetTdDetails",
            "BOJGetStandingInstructions": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetStandingInstructions",
            "ChangeUnamePwd": "https://palmobwebuat.bankofjordan.com.ps/services/ChangeUnamePwd",
            "RetailBankingBEServices": "https://palmobwebuat.bankofjordan.com.ps/services/RetailBankingBEServices",
            "BOJGetRegisteredEstmt": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetRegisteredEstmt",
            "BOJCreditCardPayment": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCreditCardPayment",
            "BOJChangeSMSNo": "https://palmobwebuat.bankofjordan.com.ps/services/BOJChangeSMSNo",
            "BOJGetPalpayQueryAuth": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayQueryAuth",
            "GetNotificationController": "https://palmobwebuat.bankofjordan.com.ps/services/GetNotificationController",
            "BOJUpdateFavorBeneficiary": "https://palmobwebuat.bankofjordan.com.ps/services/BOJUpdateFavorBeneficiary",
            "BOJEstmtConfirm": "https://palmobwebuat.bankofjordan.com.ps/services/BOJEstmtConfirm",
            "LoopExample": "https://palmobwebuat.bankofjordan.com.ps/services/LoopExample",
            "BOJprAccTransfer": "https://palmobwebuat.bankofjordan.com.ps/services/BOJprAccTransfer",
            "GetCommission": "https://palmobwebuat.bankofjordan.com.ps/services/GetCommission",
            "BOJCancelDebitCard": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCancelDebitCard",
            "DebitCardList": "https://palmobwebuat.bankofjordan.com.ps/services/DebitCardList",
            "BOJGetProfileData": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetProfileData",
            "GetDetails": "https://palmobwebuat.bankofjordan.com.ps/services/GetDetails",
            "BOJGetAccountDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetAccountDetails",
            "BOJGetPalpayComm": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayComm",
            "AccountTransactions": "https://palmobwebuat.bankofjordan.com.ps/services/AccountTransactions",
            "prGetExternalTransferComm": "https://palmobwebuat.bankofjordan.com.ps/services/prGetExternalTransferComm",
            "GoogleAPIs": "https://palmobwebuat.bankofjordan.com.ps/services/GoogleAPIs",
            "ManualUserCreation": "https://palmobwebuat.bankofjordan.com.ps/services/ManualUserCreation",
            "BOJGetCrCardHistTrans": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetCrCardHistTrans",
            "BOJGetPalpayBillDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayBillDetails",
            "BOJPalpayPpPaymentConfirm": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPalpayPpPaymentConfirm",
            "BOJPrJmpGetFees": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrJmpGetFees",
            "BOJGetStatisticsDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetStatisticsDetails",
            "BOJCrdtCrdChngSms": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCrdtCrdChngSms",
            "BOJJavaServices": "https://palmobwebuat.bankofjordan.com.ps/services/BOJJavaServices",
            "BOJUnClearTxn": "https://palmobwebuat.bankofjordan.com.ps/services/BOJUnClearTxn",
            "BOJPushNotification": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPushNotification",
            "BOJGetCrCardStmt": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetCrCardStmt",
            "BOJPrConfirmJmpPayments": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrConfirmJmpPayments",
            "BOJprCcInstantCashback": "https://palmobwebuat.bankofjordan.com.ps/services/BOJprCcInstantCashback",
            "LoopStatistics": "https://palmobwebuat.bankofjordan.com.ps/services/LoopStatistics",
            "BOJCheckCardPin": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCheckCardPin",
            "BOJGetWebChrgCardDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetWebChrgCardDetails",
            "BOJPrGetLoanAccDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrGetLoanAccDetails",
            "BOJBranchList": "https://palmobwebuat.bankofjordan.com.ps/services/BOJBranchList",
            "BOJGetExchRates": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetExchRates",
            "BOJDeleteBeneficiary": "https://palmobwebuat.bankofjordan.com.ps/services/BOJDeleteBeneficiary",
            "BOJPalpayPaymentConfirm": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPalpayPaymentConfirm",
            "BOJSendOTP": "https://palmobwebuat.bankofjordan.com.ps/services/BOJSendOTP",
            "TransferMoneySS": "https://palmobwebuat.bankofjordan.com.ps/services/TransferMoneySS",
            "SendPushMessage": "https://palmobwebuat.bankofjordan.com.ps/services/SendPushMessage",
            "BOJGetTransactionDetails": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetTransactionDetails",
            "BOJGetPalpayMappingCodes": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayMappingCodes",
            "BOJGetTransferFee": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetTransferFee",
            "NotificationController": "https://palmobwebuat.bankofjordan.com.ps/services/NotificationController",
            "prCcInternetMailorder": "https://palmobwebuat.bankofjordan.com.ps/services/prCcInternetMailorder"
        },
        "appId": "cb14d3a6-6813-4388-98c3-a44882aa8725",
        "name": "RetailBankingServices",
        "reportingsvc": {
            "session": "https://palmobwebuat.bankofjordan.com.ps/services/IST",
            "custom": "https://palmobwebuat.bankofjordan.com.ps/services/CMS"
        },
        "baseId": "8c8d242a-7d68-4dd4-a28a-16a9ec274953",
        "login": [{
            "alias": "LinkedIn",
            "type": "oauth2",
            "prov": "LinkedIn",
            "url": "https://palmobwebuat.bankofjordan.com.ps/authService/100000004"
        }, {
            "mandatory_fields": [],
            "alias": "CustomLogin",
            "type": "basic",
            "prov": "CustomLogin",
            "url": "https://palmobwebuat.bankofjordan.com.ps/authService/100000004"
        }],
        "services_meta": {
            "BOJGetAccDetailsNode": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetAccDetailsNode"
            },
            "RBObjects": {
                "metadata_url": "https://palmobwebuat.bankofjordan.com.ps/services/metadata/v1/RBObjects",
                "type": "objectsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/data/v1/RBObjects"
            },
            "prAddBeneficiary": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/prAddBeneficiary"
            },
            "prGetAtmTransactionDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/prGetAtmTransactionDetails"
            },
            "BOJPrfChngSms": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrfChngSms"
            },
            "BranchATMLocator": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BranchATMLocator"
            },
            "ATMLocator": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/ATMLocator"
            },
            "OTPController": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/OTPController"
            },
            "BOJDebitCrdTxnList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJDebitCrdTxnList"
            },
            "BOJPrExternalTransfer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrExternalTransfer"
            },
            "BOJGetCrCardDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetCrCardDetails"
            },
            "BOJJmpRegister": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJJmpRegister"
            },
            "BOJCancelCreditCard": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCancelCreditCard"
            },
            "getTncProfileData": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/getTncProfileData"
            },
            "GetAccountListService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/GetAccountListService"
            },
            "BOJsendUserName": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJsendUserName"
            },
            "iWatchBOJPrGetTdDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/iWatchBOJPrGetTdDetails"
            },
            "BojprDcActivation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BojprDcActivation"
            },
            "CancelCard": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/CancelCard"
            },
            "BOJBeneBranchList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJBeneBranchList"
            },
            "BOJRestServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJRestServices"
            },
            "BOJPpPayment": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPpPayment"
            },
            "BOJprCcActivation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJprCcActivation"
            },
            "BOJCnfOpenNewAcc": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCnfOpenNewAcc"
            },
            "BOJSendSMSMobile": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJSendSMSMobile"
            },
            "BojUserReg": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BojUserReg"
            },
            "BOJATMList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJATMList"
            },
            "AllCardList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/AllCardList"
            },
            "iWatchBOJPrGetLoanAccDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/iWatchBOJPrGetLoanAccDetails"
            },
            "InformationSupport": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/InformationSupport"
            },
            "BOJEfwPaymentConfirm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJEfwPaymentConfirm"
            },
            "testcomm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/testcomm"
            },
            "prGetBeneficiaryDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/prGetBeneficiaryDetails"
            },
            "BOJGetDbCardDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetDbCardDetails"
            },
            "pushNotificationsAuthService": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/pushNotificationsAuthService"
            },
            "BOJGetPalpayBillDetailsOtp": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayBillDetailsOtp"
            },
            "OTPSMSController": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/OTPSMSController"
            },
            "PushNotification": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/PushNotification"
            },
            "prDcLinkedAccount": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/prDcLinkedAccount"
            },
            "BOJCnfSiCancellation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCnfSiCancellation"
            },
            "BOJPrGetTdDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrGetTdDetails"
            },
            "BOJGetStandingInstructions": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetStandingInstructions"
            },
            "ChangeUnamePwd": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/ChangeUnamePwd"
            },
            "RetailBankingBEServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/RetailBankingBEServices"
            },
            "BOJGetRegisteredEstmt": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetRegisteredEstmt"
            },
            "BOJCreditCardPayment": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCreditCardPayment"
            },
            "BOJChangeSMSNo": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJChangeSMSNo"
            },
            "BOJGetPalpayQueryAuth": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayQueryAuth"
            },
            "GetNotificationController": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/GetNotificationController"
            },
            "BOJUpdateFavorBeneficiary": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJUpdateFavorBeneficiary"
            },
            "BOJEstmtConfirm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJEstmtConfirm"
            },
            "LoopExample": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/LoopExample"
            },
            "BOJprAccTransfer": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJprAccTransfer"
            },
            "GetCommission": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/GetCommission"
            },
            "BOJCancelDebitCard": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCancelDebitCard"
            },
            "DebitCardList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/DebitCardList"
            },
            "BOJGetProfileData": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetProfileData"
            },
            "GetDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/GetDetails"
            },
            "BOJGetAccountDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetAccountDetails"
            },
            "BOJGetPalpayComm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayComm"
            },
            "AccountTransactions": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/AccountTransactions"
            },
            "prGetExternalTransferComm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/prGetExternalTransferComm"
            },
            "GoogleAPIs": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/GoogleAPIs"
            },
            "ManualUserCreation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/ManualUserCreation"
            },
            "BOJGetCrCardHistTrans": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetCrCardHistTrans"
            },
            "BOJGetPalpayBillDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayBillDetails"
            },
            "BOJPalpayPpPaymentConfirm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPalpayPpPaymentConfirm"
            },
            "BOJPrJmpGetFees": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrJmpGetFees"
            },
            "BOJGetStatisticsDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetStatisticsDetails"
            },
            "BOJCrdtCrdChngSms": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCrdtCrdChngSms"
            },
            "BOJJavaServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJJavaServices"
            },
            "BOJUnClearTxn": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJUnClearTxn"
            },
            "BOJPushNotification": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPushNotification"
            },
            "BOJGetCrCardStmt": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetCrCardStmt"
            },
            "BOJPrConfirmJmpPayments": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrConfirmJmpPayments"
            },
            "BOJprCcInstantCashback": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJprCcInstantCashback"
            },
            "LoopStatistics": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/LoopStatistics"
            },
            "BOJCheckCardPin": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJCheckCardPin"
            },
            "BOJGetWebChrgCardDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetWebChrgCardDetails"
            },
            "BOJPrGetLoanAccDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPrGetLoanAccDetails"
            },
            "BOJBranchList": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJBranchList"
            },
            "BOJGetExchRates": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetExchRates"
            },
            "BOJDeleteBeneficiary": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJDeleteBeneficiary"
            },
            "BOJPalpayPaymentConfirm": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJPalpayPaymentConfirm"
            },
            "BOJSendOTP": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJSendOTP"
            },
            "TransferMoneySS": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/TransferMoneySS"
            },
            "SendPushMessage": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/SendPushMessage"
            },
            "BOJGetTransactionDetails": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetTransactionDetails"
            },
            "BOJGetPalpayMappingCodes": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetPalpayMappingCodes"
            },
            "BOJGetTransferFee": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/BOJGetTransferFee"
            },
            "NotificationController": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/NotificationController"
            },
            "prCcInternetMailorder": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://palmobwebuat.bankofjordan.com.ps/services/prCcInternetMailorder"
            }
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "Error", "Crash"],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        isI18nLayoutConfigEnabled: true,
        APILevel: 7000
    })
};

function appInit(params) {
    skinsInit();
    kony.application.setCheckBoxSelectionImageAlignment(constants.CHECKBOX_SELECTION_IMAGE_ALIGNMENT_RIGHT);
    kony.application.setDefaultTextboxPadding(false);
    kony.application.setRespectImageSizeForImageWidgetAlignment(true);
    initializeMVCTemplates();
    initializeUserWidgets();
    initializemainSegmentTemplate();
    initializesegacntstatementstmplt();
    initializesegaddnewpaye();
    initializesegBulkPaymentConfirmation();
    initializesegbulkpaymenttmp();
    initializesegCheckReOrderKA();
    initializesegDebitCardTrans();
    initializesegdirectionInfoKA();
    initializesegFAQRow();
    initializesegFAQSection();
    initializesegGetAllIPSAlias();
    initializesegInformation();
    initializesegJMPBene();
    initializesegListViewKA();
    initializesegmanagecardlesstmplt();
    initializesegmanagecardstmplt();
    initializesegmanagepayeetmplt();
    initializesegmentAndroidIcon();
    initializesegmentHeader();
    initializesegmentheadertmplt();
    initializesegmentPayPerson();
    initializesegmentWithChevron();
    initializesegmentWithDoubleLabel();
    initializesegmentWithIcon();
    initializesegmentWithLabel();
    initializesegmentWithoutIcon();
    initializesegmentWithSwitch();
    initializesegMessagesBoldTmplDraftKA();
    initializesegMessagesBoldTmplKA();
    initializesegMessagesTmplKA();
    initializesegphonecnct();
    initializesegSelectProductKA();
    initializesegShowAcc();
    initializesegTmpData();
    initializesegtmpltchecknocolor();
    initializesegtransactionchecktmplt();
    initializesegTransferAddExternal();
    initializetempCurrencySectionHeader();
    initializetempJomopayPopup();
    initializetempNUOClassificationTypesKA();
    initializetempP2PphonePayeeKA();
    initializetempP2PphonePayeeSectionsKA();
    initializetempPayPeopleKA();
    initializetempSectionHeaderP2P();
    initializetmpAccountDetailsScreen();
    initializetmpAccountLandingKA();
    initializetmpAccountSMSDisabled();
    initializetmpAccountSMSEnabled();
    initializetmpAccountsPayBill();
    initializetmpAccountType();
    initializetmpAccountTypes();
    initializetmpAccPrecheckNoTransactions();
    initializetmpAddressSearchResults();
    initializetmpAlert();
    initializetmpAllBeneHeader();
    initializetmpAtmServices();
    initializetmpBillList();
    initializetmpBudgetListKA();
    initializetmpCalculateFx();
    initializetmpCardNumber();
    initializetmpCardsReorder();
    initializetmpChooseAccountsSettings();
    initializetmpCurrency();
    initializetmpDeviceRegstrationDisabled();
    initializetmpDeviceRegstrationEnabled();
    initializetmpExchangeRates();
    initializetmpFrequrncyData();
    initializetmpJomopaycontacts();
    initializetmpJomopayList();
    initializetmplateCompanyNameKA();
    initializetmpLinkedAccounts();
    initializetmpLocationAtmBranch();
    initializetmplsegLegendKA();
    initializetmpMonths();
    initializetmpNavigationOptKA();
    initializetmpPOT();
    initializetmpProductBundle();
    initializetmpQuickBalanceSeg();
    initializetmpSearchResultsKA();
    initializetmpSegAccountsTran();
    initializetmpSegAccTranNoData();
    initializetmpSegBotUserInput();
    initializetmpSegCardsLanding();
    initializetmpSegLoginMethods();
    initializetmpSegPaymentHistory();
    initializetmpSegRequestStatus();
    initializetmpsegSIList();
    initializetmpServiceTypeDataPrePaid();
    initializetmpTransactionBiller();
    initializetmpTransactionStatus();
    initializetmpTwoLabelsWithHyphen();
    initializetmpVersionKA();
    initializetmSegReleaseNote();
    initializetransactionTemplateColor();
    initializetransactionTemplateNoColor();
    initializetmpAuthHeader();
    initializeiphoneFooter();
    initializeiphoneFooterAr();
    initializetabBarAccounts();
    initializetabBarDeposits();
    initializetabBarMore();
    initializetabBarTransfer();
    initializeTemp0da2fb8b2bfdc47();
    initializemapAtm();
    initializemapATMBranch();
    initializemapATMBranchWithoutLocation();
    Form1Globals();
    frmAccountAlertsKAGlobals();
    frmAccountDetailKAGlobals();
    frmAccountDetailsScreenGlobals();
    frmAccountInfoKAGlobals();
    frmAccountsLandingKAGlobals();
    frmAccountsReorderKAGlobals();
    frmAccountTypeGlobals();
    frmAddExternalAccountHomeGlobals();
    frmAddExternalAccountKAGlobals();
    frmAddNewPayeeKAGlobals();
    frmAddNewPayeePalGlobals();
    frmAddNewPrePayeeKAGlobals();
    frmAlertHistoryGlobals();
    frmAlertsKAGlobals();
    frmApplyNewCardsConfirmKAGlobals();
    frmApplyNewCardsKAGlobals();
    frmATMPOSLimitGlobals();
    frmAuthorizationAlternativesGlobals();
    frmBillerListFormGlobals();
    frmBillerListFormPalGlobals();
    frmBillsGlobals();
    frmBillsPalGlobals();
    frmBotSplashGlobals();
    frmBulkPaymentConfirmKAGlobals();
    frmBulkPaymentKAGlobals();
    frmCancelCardKAGlobals();
    frmCardlessSendMessageGlobals();
    frmCardlessTransactionGlobals();
    frmCardLinkedAccountsGlobals();
    frmCardLinkedAccountsConfirmGlobals();
    frmCardOperationsKAGlobals();
    frmCardsAddNicknameGlobals();
    frmCardsLandingKAGlobals();
    frmCardsListPreferencesGlobals();
    frmCardStatementKAGlobals();
    frmChatBotsGlobals();
    frmCheckingJointGlobals();
    frmCheckReOrderListKAGlobals();
    frmCheckReOrderSuccessKAGlobals();
    frmchequeimagesGlobals();
    frmChequeStatusGlobals();
    frmConfirmationCardKAGlobals();
    frmConfirmationPasswordKAGlobals();
    frmConfirmationPersonalDetailsKAGlobals();
    frmConfirmationUserNameKAGlobals();
    frmConfirmCashWithDrawGlobals();
    frmConfirmCashWithDrawQRCodeKAGlobals();
    frmConfirmDepositKAGlobals();
    frmConfirmNPPRegistrationGlobals();
    frmConfirmP2PRegistrationKAGlobals();
    frmConfirmPayBillGlobals();
    frmConfirmPrePayBillGlobals();
    frmConfirmStandingInstructionsGlobals();
    frmConfirmTransferKAGlobals();
    frmCongratulationsGlobals();
    frmContactUsKAGlobals();
    frmCreateCredentialsGlobals();
    frmCreditCardNumberGlobals();
    frmCreditCardPaymentGlobals();
    frmCreditCardsKAGlobals();
    frmCurrencyGlobals();
    frmDefaultLoginMethodKAGlobals();
    frmDepositPayLandingKAGlobals();
    frmDeviceDeRegistrationKAGlobals();
    frmDeviceRegisterationIncorrectPinActicvationKAGlobals();
    frmDeviceregistrarionSuccessKAGlobals();
    frmDeviceRegistrationGlobals();
    frmDeviceRegistrationOptionsKAGlobals();
    frmDirectionsKAGlobals();
    frmEditAccountSettingsGlobals();
    frmEditPayeeKAGlobals();
    frmEditPayeePalGlobals();
    frmEditPayeeSuccessBillPayKAGlobals();
    frmEditPayeeSuccessKAGlobals();
    frmEnableInternetTransactionKAGlobals();
    frmEnlargeAdKAGlobals();
    frmEnrolluserLandingKAGlobals();
    frmEnterLocationKAGlobals();
    frmEnterPersonalDetailsKAGlobals();
    frmEPSGlobals();
    frmEstatementConfirmKAGlobals();
    frmEstatementLandingKAGlobals();
    frmFaceIDCaptureGlobals();
    frmFacialAuthEnableGlobals();
    frmFilterSIGlobals();
    frmFilterTransactionGlobals();
    frmFrequencyListGlobals();
    frmFxRateGlobals();
    frmGetIPSAliasGlobals();
    frmInformationDetailsKAGlobals();
    frmInformationKAGlobals();
    frmInstantCashGlobals();
    frmIPSHomeGlobals();
    frmIPSManageBeneGlobals();
    frmIPSRegistrationGlobals();
    frmJoMoPayGlobals();
    frmJomoPayAccountListGlobals();
    frmJOMOPayAddBillerGlobals();
    frmJoMoPayConfirmationGlobals();
    frmJomopayContactsGlobals();
    frmJoMoPayLandingGlobals();
    frmJoMoPayQRConfirmGlobals();
    frmJomopayRegCountryGlobals();
    frmJoMoPayRegistrationGlobals();
    frmLanguageChangeGlobals();
    frmLoanPostponeGlobals();
    frmLocationDetailsKAGlobals();
    frmLocatorATMDetailsKAGlobals();
    frmLocatorBranchDetailsKAGlobals();
    frmLocatorKAGlobals();
    frmLoginAuthSuccessKAGlobals();
    frmLoginKAGlobals();
    frmManageCardLessGlobals();
    frmManageCardsKAGlobals();
    frmManagePayeeKAGlobals();
    frmMoreGlobals();
    frmMoreCheckReorderKAGlobals();
    frmMoreFaqKAGlobals();
    frmMoreForeignExchangeRatesKAGlobals();
    frmMoreInterestRatesKAGlobals();
    frmMoreLandingKAGlobals();
    frmMorePrivacyPolicyKAGlobals();
    frmMoreTermsAndConditionsKAGlobals();
    frmMyAccountSettingsKAGlobals();
    frmMyMoneyListKAGlobals();
    frmNewBillDetailsGlobals();
    frmNewBillKAGlobals();
    frmNewSubAccountConfirmGlobals();
    frmNewSubAccountLandingNewGlobals();
    frmNewTransferKAGlobals();
    frmNewUserOnboardVerificationKAGlobals();
    frmOpenTermDepositGlobals();
    frmOrderCheckBookGlobals();
    frmOrderChequeBookConfirmGlobals();
    frmPalpayBillInformationGlobals();
    frmPalpayHomeGlobals();
    frmPayBillHomeGlobals();
    frmPayBillHomePalGlobals();
    frmPayeeDetailsKAGlobals();
    frmPayeeTransactionsKAGlobals();
    frmPaymentDashboardGlobals();
    frmPendingWithdrawSummaryGlobals();
    frmPickAProductKAGlobals();
    frmPinEntryStep1Globals();
    frmPinEntryStep2Globals();
    frmPinEntrySuccessGlobals();
    frmPostLoginAdvertisementKAGlobals();
    frmPOTGlobals();
    frmPreferredAccountsKAGlobals();
    frmPrePaidPayeeDetailsKAGlobals();
    frmProductBundleGlobals();
    frmReasonsListScreenGlobals();
    frmRecentDepositKAGlobals();
    frmRecentTransactionDetailsKAGlobals();
    frmRegisterUserGlobals();
    frmReleaseNotesKAGlobals();
    frmRequestPinCardGlobals();
    frmRequestStatementAccountsGlobals();
    frmRequestStatementAccountsConfirmGlobals();
    frmrequestStatusGlobals();
    frmScheduledTransactionDetailsKAGlobals();
    frmSearchResultsKAGlobals();
    frmSelectBankNameGlobals();
    frmSelectDetailsBeneGlobals();
    frmSetDefaultPageKAGlobals();
    frmSetLocatorDistanceFilterKAGlobals();
    frmSettingsKAGlobals();
    frmSetupFaceIdSettingsKAGlobals();
    frmSetUpPinSettingsGlobals();
    frmShowAllBeneficiaryGlobals();
    frmSMSNotificationGlobals();
    frmSplashGlobals();
    frmStandingInstructionsGlobals();
    frmStopCardKAGlobals();
    frmSuccessFormKAGlobals();
    frmSuccessFormQRCodeKAGlobals();
    frmTermsAndConditionsKAGlobals();
    frmTermsAndCondtionsGlobals();
    frmtransactionChequeKAGlobals();
    frmTransactionDetailKAGlobals();
    frmTransactionDetailsGlobals();
    frmTransactionDetailsPFMKAGlobals();
    frmTransferStatusGlobals();
    frmUserSettingsMyProfileKAGlobals();
    frmUserSettingsPinLoginKAGlobals();
    frmUserSettingsSiriGlobals();
    frmUserSettingsSIRILIMITGlobals();
    frmUserSettingsTouchIdKAGlobals();
    frmWebChargeGlobals();
    PopupCancelAndReplaceCardGlobals();
    popupCommonAlertGlobals();
    PopupDeviceRegistrationGlobals();
    popupNotificationInfoGlobals();
    popupStopCardGlobals();
    setAppBehaviors();
};
kony.visualizer.actions.postAppInitCallBack = function(eventObj) {
    return AS_AppEvents_a1f7494243ba459d87b81e142176930a(eventObj);
};

function themeCallBack() {
    initializeGlobalVariables();
    callAppMenu();
    kony.application.setApplicationInitializationEvents({
        init: appInit,
        preappinit: AS_AppEvents_b70e24eaba84433d9d1808d9ee81b773,
        appservice: function(eventObject) {
            var value = AS_AppEvents_d7d33e4974bd4754b472728a81aff528(eventObject);
            return value;
        },
        postappinit: kony.visualizer.actions.postAppInitCallBack,
        showstartupform: function() {
            frmLanguageChange.show();
        }
    });
};

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};

function loadResources() {
    globalhttpheaders = {};
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
kony.i18n.setLocaleLayoutConfig({
    "ar": {
        "mirrorContentAlignment": true,
        "mirrorFlexPositionProperties": true,
        "mirrorFlowHorizontalAlignment": false
    },
    "en": {
        "mirrorContentAlignment": false,
        "mirrorFlexPositionProperties": false,
        "mirrorFlowHorizontalAlignment": false
    }
});
//This is the entry point for the application.When Locale comes,Local API call will be the entry point.
kony.i18n.setDefaultLocaleAsync("en", onSuccess, onFailure, null);
debugger;