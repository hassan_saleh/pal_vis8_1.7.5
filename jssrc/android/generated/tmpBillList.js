function initializetmpBillList() {
    mainBillListContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "mainBillListContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    mainBillListContainer.setDefaultUnit(kony.flex.DP);
    var submainContainer1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "submainContainer1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    submainContainer1.setDefaultUnit(kony.flex.DP);
    var lblPaymentDate1 = new kony.ui.Label({
        "id": "lblPaymentDate1",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.bills.IssueDate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPayeeRef1 = new kony.ui.Label({
        "id": "lblPayeeRef1",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.requeststatus.referencenumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmount1 = new kony.ui.Label({
        "id": "lblAmount1",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    submainContainer1.add(lblPaymentDate1, lblPayeeRef1, lblAmount1);
    var submainContainer2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "submainContainer2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    submainContainer2.setDefaultUnit(kony.flex.DP);
    var lblPaymentDate2 = new kony.ui.Label({
        "id": "lblPaymentDate2",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPayeeRef2 = new kony.ui.Label({
        "id": "lblPayeeRef2",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "40%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmount2 = new kony.ui.Label({
        "id": "lblAmount2",
        "isVisible": true,
        "left": "10%",
        "skin": "sknLblNextDisabled",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "70%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    submainContainer2.add(lblPaymentDate2, lblPayeeRef2, lblAmount2);
    mainBillListContainer.add(submainContainer1, submainContainer2);
}