function initializetmpAccountSMSEnabled() {
    flxSMSNotificationEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxSMSNotificationEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSMSNotificationEnabled.setDefaultUnit(kony.flex.DP);
    var flxSMSSwitchDIsabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSSwitchDIsabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d21c6ccb47a04204823befbf63db9b1e,
        "right": "5%",
        "skin": "sknflxWhiteOpacity60",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 3
    }, {}, {});
    flxSMSSwitchDIsabled.setDefaultUnit(kony.flex.DP);
    var flxInnerDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxInnerDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxInnerDisabled.setDefaultUnit(kony.flex.DP);
    flxInnerDisabled.add();
    var flxILineDisabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "14dp",
        "id": "flxILineDisabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": 10,
        "skin": "sknLineDarkBlueOpacity60",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxILineDisabled.setDefaultUnit(kony.flex.DP);
    flxILineDisabled.add();
    flxSMSSwitchDIsabled.add(flxInnerDisabled, flxILineDisabled);
    var flxSMSSwitchEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxSMSSwitchEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c1834d9307e744d6adee27f5b68f4e87,
        "right": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 3
    }, {}, {});
    flxSMSSwitchEnabled.setDefaultUnit(kony.flex.DP);
    var flxInnerEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxInnerEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    flxInnerEnabled.setDefaultUnit(kony.flex.DP);
    flxInnerEnabled.add();
    var flxLineEnabled = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "45%",
        "clipBounds": true,
        "height": "14dp",
        "id": "flxLineEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "isModalContainer": false,
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    flxLineEnabled.setDefaultUnit(kony.flex.DP);
    flxLineEnabled.add();
    flxSMSSwitchEnabled.add(flxInnerEnabled, flxLineEnabled);
    var flxUnderLineSMS = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderLineSMS",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "flxUnderLine",
        "top": "98%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderLineSMS.setDefaultUnit(kony.flex.DP);
    flxUnderLineSMS.add();
    var lblAccountType = new kony.ui.Label({
        "id": "lblAccountType",
        "isVisible": true,
        "left": "5%",
        "maxNumberOfLines": 1,
        "skin": "lblAccountType",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "23%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "5%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "45%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAlertArrow = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAlertArrow",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c1834d9307e744d6adee27f5b68f4e87,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxAlertArrow.setDefaultUnit(kony.flex.DP);
    var lblRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblRightIcon",
        "isVisible": true,
        "right": "3%",
        "skin": "sknBOJttfwhitee",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAlertArrow.add(lblRightIcon);
    flxSMSNotificationEnabled.add(flxSMSSwitchDIsabled, flxSMSSwitchEnabled, flxUnderLineSMS, lblAccountType, lblAccountNumber, flxAlertArrow);
}