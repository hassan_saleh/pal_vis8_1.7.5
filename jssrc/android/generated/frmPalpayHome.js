function addWidgetsfrmPalpayHome() {
    frmPalpayHome.setDefaultUnit(kony.flex.DP);
    var managePayeeFlx = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "managePayeeFlx",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    managePayeeFlx.setDefaultUnit(kony.flex.DP);
    var headerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "16%",
        "id": "headerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    headerContainer.setDefaultUnit(kony.flex.DP);
    var flxTop = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxTop",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTop.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d7b5e09f012f47019512d409cc6ae3c6,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0%",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.listTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "70dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxIcons = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxIcons",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "75%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "25%",
        "zIndex": 1
    }, {}, {});
    flxIcons.setDefaultUnit(kony.flex.DP);
    var btnAdd = new kony.ui.Button({
        "focusSkin": "slIconFocus",
        "height": "100%",
        "id": "btnAdd",
        "isVisible": true,
        "left": "50%",
        "onClick": AS_Button_be4a1fb4a0af49fc870353544264f3ef,
        "skin": "slIcon",
        "text": "u",
        "top": "0%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxIcons.add(btnAdd);
    flxTop.add(flxBack, lblTitle, flxIcons);
    var flxSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "28%",
        "id": "flxSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSearch.setDefaultUnit(kony.flex.DP);
    var lblSearch = new kony.ui.Label({
        "height": "100%",
        "id": "lblSearch",
        "isVisible": true,
        "left": "0%",
        "skin": "sknSearchIcon",
        "text": "h",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var TextField0d22204377e6e4a = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "focusSkin": "sknTxtBoxSearch",
        "height": "100%",
        "id": "TextField0d22204377e6e4a",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "10%",
        "placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
        "secureTextEntry": false,
        "skin": "sknTxtBoxSearch",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "90%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderLineSearch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderLineSearch",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "92%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderLineSearch.setDefaultUnit(kony.flex.DP);
    flxUnderLineSearch.add();
    flxSearch.add(lblSearch, TextField0d22204377e6e4a, flxUnderLineSearch);
    headerContainer.add(flxTop, flxSearch);
    var contantContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "76.50%",
        "id": "contantContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contantContainer.setDefaultUnit(kony.flex.DP);
    var lblRequestLimitHint = new kony.ui.Label({
        "height": "10%",
        "id": "lblRequestLimitHint",
        "isVisible": false,
        "left": "3%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.bulkpayment.hint"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSearchResult = new kony.ui.Label({
        "centerX": "50%",
        "height": "66%",
        "id": "lblSearchResult",
        "isVisible": false,
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.payee.noBillFound"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var managePayeeSegment = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "accountnumber": "",
            "btnBillsPayAccounts": "0",
            "btnDelete": "",
            "btnEditBillerDetails": "",
            "dueAmount": "Account Number",
            "lblBillerType": "",
            "lblBulkSelection": "Label",
            "lblInitial": "",
            "lblTick": "r",
            "payeename": "",
            "payeenickname": ""
        }],
        "groupCells": false,
        "height": "99%",
        "id": "managePayeeSegment",
        "isVisible": true,
        "left": "0%",
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "slSegSendMoney",
        "rowTemplate": flxManagePayeeNormal,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "top": "0%",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "accountnumber": "accountnumber",
            "btnBillsPayAccounts": "btnBillsPayAccounts",
            "btnDelete": "btnDelete",
            "btnEditBillerDetails": "btnEditBillerDetails",
            "contactListDivider": "contactListDivider",
            "dueAmount": "dueAmount",
            "flxAnimate": "flxAnimate",
            "flxButtonHolder": "flxButtonHolder",
            "flxDetails": "flxDetails",
            "flxIcon1": "flxIcon1",
            "flxIconContainer": "flxIconContainer",
            "flxManagePayeeNormal": "flxManagePayeeNormal",
            "flxToAnimate": "flxToAnimate",
            "lblBillerType": "lblBillerType",
            "lblBulkSelection": "lblBulkSelection",
            "lblInitial": "lblInitial",
            "lblTick": "lblTick",
            "payeename": "payeename",
            "payeenickname": "payeenickname",
            "verticalDivider": "verticalDivider"
        },
        "width": "100%",
        "zIndex": 2
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var FlexScrollContainer0i2f101e839d84d = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": true,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "68%",
        "horizontalScrollIndicator": true,
        "id": "FlexScrollContainer0i2f101e839d84d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
        "skin": "slFSbox",
        "top": "0%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexScrollContainer0i2f101e839d84d.setDefaultUnit(kony.flex.DP);
    FlexScrollContainer0i2f101e839d84d.add();
    contantContainer.add(lblRequestLimitHint, lblSearchResult, managePayeeSegment, FlexScrollContainer0i2f101e839d84d);
    var bottomContantContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "7%",
        "id": "bottomContantContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    bottomContantContainer.setDefaultUnit(kony.flex.DP);
    var btnPayNow = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "100%",
        "id": "btnPayNow",
        "isVisible": true,
        "onClick": AS_Button_cdfc1794183a4c59a13487708c33b3d5,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PayNow"),
        "width": "70%",
        "zIndex": 12
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBulkPay = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "100%",
        "id": "btnBulkPay",
        "isVisible": false,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.bulkpayment.title"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    bottomContantContainer.add(btnPayNow, btnBulkPay);
    managePayeeFlx.add(headerContainer, contantContainer, bottomContantContainer);
    var managePayeeEmptyFlx = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "managePayeeEmptyFlx",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    managePayeeEmptyFlx.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "skin": "CopyslFbox0i5e0ac7d37b04e",
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxEmptyBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxEmptyBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_edcb17630bd5425d8cf9181f70b5e0a4,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxEmptyBack.setDefaultUnit(kony.flex.DP);
    var CopylblBackIcon0d8e74d2e4b8547 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblBackIcon0d8e74d2e4b8547",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0fc35e23ff9274f = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblBack0fc35e23ff9274f",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxEmptyBack.add(CopylblBackIcon0d8e74d2e4b8547, CopylblBack0fc35e23ff9274f);
    var lblEmbtyTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblEmbtyTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.emptyTitile"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "47dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxEmptyBack, lblEmbtyTitle);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": 0,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var mainContainerInner = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainContainerInner",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContainerInner.setDefaultUnit(kony.flex.DP);
    var imgBeneficiary = new kony.ui.Image2({
        "centerX": "50%",
        "height": "25%",
        "id": "imgBeneficiary",
        "isVisible": true,
        "skin": "slImage",
        "src": "icon_beneficiary.png",
        "top": "8%",
        "width": "74%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblMessageTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "8%",
        "id": "lblMessageTitle",
        "isVisible": true,
        "skin": "sknLblMsg",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.PayYourBills"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMessage = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblMessage",
        "isVisible": true,
        "skin": "sknLblDetail",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.emptyStatement"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "2%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Button0g0d327848c0a47 = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "10%",
        "id": "Button0g0d327848c0a47",
        "isVisible": false,
        "maxWidth": "90%",
        "onClick": AS_Button_e504988ef22149e1a638c27f103dfdee,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.AddBill"),
        "top": "5%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnPayABill = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "8%",
        "id": "btnPayABill",
        "isVisible": true,
        "maxWidth": "90%",
        "onClick": AS_Button_a8f6dc1e33344395ba0958a1729f9768,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.payABill"),
        "top": "15%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    mainContainerInner.add(imgBeneficiary, lblMessageTitle, lblMessage, Button0g0d327848c0a47, btnPayABill);
    flxMain.add(mainContainerInner);
    managePayeeEmptyFlx.add(flxHeader, flxMain);
    frmPalpayHome.add(managePayeeFlx, managePayeeEmptyFlx);
};

function frmPalpayHomeGlobals() {
    frmPalpayHome = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPalpayHome,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmPalpayHome",
        "init": AS_Form_d4954d6d14144e02a149ab369fd862d7,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "preShow": function(eventobject) {
            AS_Form_edee6f7991b14169bab410a43189e89d(eventobject);
        },
        "skin": "sknmainGradient",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_i292849c65b243f6a349cd8c25b8e8d4,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};