function addWidgetsfrmPalpayBillInformation() {
    frmPalpayBillInformation.setDefaultUnit(kony.flex.DP);
    var mainInputContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainInputContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainInputContainer.setDefaultUnit(kony.flex.DP);
    var headerInputContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "headerInputContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0.00%",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0.00%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    headerInputContainer.setDefaultUnit(kony.flex.DP);
    var backButtonContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "backButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_c3fa155fcd6949cb81e22afd4b4c9aad,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    backButtonContainer.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    backButtonContainer.add(lblBackIcon, lblBack);
    var lblTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitle",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.payABill"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNext = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNext",
        "isVisible": true,
        "left": "86%",
        "onTouchStart": AS_Label_d5229a2b46bc4a8badf0e8bc93f016bc,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    headerInputContainer.add(backButtonContainer, lblTitle, lblNext);
    var submainInputContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "submainInputContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    submainInputContainer.setDefaultUnit(kony.flex.DP);
    var flxNickName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.00%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxNickName",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2.00%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNickName.setDefaultUnit(kony.flex.DP);
    var tbxNickName = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "25%",
        "centerX": "50%",
        "height": "55%",
        "id": "tbxNickName",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": 35,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_be01a7a30b5e49b58ebe64685876bbce,
        "onEndEditing": AS_TextField_e53b71ed11114c2c9e4a0315c06db786,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineNickName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineNickName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "75%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineNickName.setDefaultUnit(kony.flex.DP);
    flxUnderlineNickName.add();
    var lblNickName = new kony.ui.Label({
        "id": "lblNickName",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Bene.NickName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "34%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNickName.add(tbxNickName, flxUnderlineNickName, lblNickName);
    var flxBillerCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxBillerCategory",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBillerCategory.setDefaultUnit(kony.flex.DP);
    var flxUnderlineBillerCategory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerCategory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderlineBillerCategory.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerCategory.add();
    var flxBillerCategoryHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxBillerCategoryHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_a34ee4f91bd146bba3c45d681491d7a3,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillerCategoryHolder.setDefaultUnit(kony.flex.DP);
    var lblBillerCategory = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblBillerCategory",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.BillerCategory"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowBillerCategory = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowBillerCategory",
        "isVisible": true,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerCategoryHolder.add(lblBillerCategory, lblArrowBillerCategory);
    flxBillerCategory.add(flxUnderlineBillerCategory, flxBillerCategoryHolder);
    var flxBillerName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxBillerName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBillerName.setDefaultUnit(kony.flex.DP);
    var flxUnderlineBillerName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerName",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderlineBillerName.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerName.add();
    var flxBillerNameHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxBillerNameHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_e04348bd02bb4dd4a28988352a951319,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillerNameHolder.setDefaultUnit(kony.flex.DP);
    var lblBillerName = new kony.ui.Label({
        "height": "100%",
        "id": "lblBillerName",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowBillerName = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowBillerName",
        "isVisible": true,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerNameHolder.add(lblBillerName, lblArrowBillerName);
    flxBillerName.add(flxUnderlineBillerName, flxBillerNameHolder);
    var flxServiceType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxServiceType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxServiceType.setDefaultUnit(kony.flex.DP);
    var flxUnderlineServiceType = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineServiceType",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderlineServiceType.setDefaultUnit(kony.flex.DP);
    flxUnderlineServiceType.add();
    var flxServiceTypeHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxServiceTypeHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_bdb3b86f2d284705ba55c7d7ddd7e44e,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxServiceTypeHolder.setDefaultUnit(kony.flex.DP);
    var lblServiceType = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblServiceType",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowServiceType = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowServiceType",
        "isVisible": true,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxServiceTypeHolder.add(lblServiceType, lblArrowServiceType);
    flxServiceType.add(flxUnderlineServiceType, flxServiceTypeHolder);
    var flxDenomination = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxDenomination",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDenomination.setDefaultUnit(kony.flex.DP);
    var flxUnderlineDenomination = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineDenomination",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderlineDenomination.setDefaultUnit(kony.flex.DP);
    flxUnderlineDenomination.add();
    var flxDenominationHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxDenominationHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f533559742bc4868b88d6864a8e7a2bf,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDenominationHolder.setDefaultUnit(kony.flex.DP);
    var lblDenomination = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblDenomination",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Denomination"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "85%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowDenomination = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowDenomination",
        "isVisible": true,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDenominationHolder.add(lblDenomination, lblArrowDenomination);
    flxDenomination.add(flxUnderlineDenomination, flxDenominationHolder);
    var flxBillerNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "85dp",
        "id": "flxBillerNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBillerNumber.setDefaultUnit(kony.flex.DP);
    var tbxBillerNumber = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "22%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "40%",
        "id": "tbxBillerNumber",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "maxTextLength": 35,
        "onDone": AS_TextField_aaeb7a0bf564402dbca1d154c572db63,
        "onTextChange": AS_TextField_gd79eaf602654d949b462d2d91402f11,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_f4b0940fcac546f5ad9bedb2f000a1ba,
        "onEndEditing": AS_TextField_f646fd9ec94c4e74a796ca48d8e428ee,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineBillerNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineBillerNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "75%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineBillerNumber.setDefaultUnit(kony.flex.DP);
    flxUnderlineBillerNumber.add();
    var lblBillerNumber = new kony.ui.Label({
        "id": "lblBillerNumber",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerNumber.add(tbxBillerNumber, flxUnderlineBillerNumber, lblBillerNumber);
    var lblserviceTypeHint = new kony.ui.Label({
        "id": "lblserviceTypeHint",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhite100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxRadioAccCardsSelection = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxRadioAccCardsSelection",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRadioAccCardsSelection.setDefaultUnit(kony.flex.DP);
    var btnBillsPayAccounts = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "id": "btnBillsPayAccounts",
        "isVisible": true,
        "left": "5%",
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "t",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBillsPayAccounts = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillsPayAccounts",
        "isVisible": true,
        "left": "15%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBillsPayCards = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "id": "btnBillsPayCards",
        "isVisible": true,
        "left": "55%",
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "s",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBillsPayCards = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillsPayCards",
        "isVisible": true,
        "left": "65%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRadioAccCardsSelection.add(btnBillsPayAccounts, lblBillsPayAccounts, btnBillsPayCards, lblBillsPayCards);
    var flxPaymentMode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxPaymentMode",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPaymentMode.setDefaultUnit(kony.flex.DP);
    var tbxPaymentMode = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "tbxPaymentMode",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlinePaymentMode = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlinePaymentMode",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderlinePaymentMode.setDefaultUnit(kony.flex.DP);
    flxUnderlinePaymentMode.add();
    var flxPaymentModeTypeHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxPaymentModeTypeHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_fcf95fa864604f9a93b0755347383483,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPaymentModeTypeHolder.setDefaultUnit(kony.flex.DP);
    var lblPaymentMode = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblPaymentMode",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "88%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowPaymentMode = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowPaymentMode",
        "isVisible": true,
        "left": "93%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentModeTypeHolder.add(lblPaymentMode, lblArrowPaymentMode);
    flxPaymentMode.add(tbxPaymentMode, flxUnderlinePaymentMode, flxPaymentModeTypeHolder);
    var flxAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "85dp",
        "id": "flxAmount",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAmount.setDefaultUnit(kony.flex.DP);
    var tbxAmount = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "22%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "40%",
        "id": "tbxAmount",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "0%",
        "maxTextLength": null,
        "onDone": AS_TextField_j9adda1147b0481a91cec7ea35f0a015,
        "onTextChange": AS_TextField_dff05d86739e4ccaba0a20f87b39b6e2,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_a7c29786181f46a3a650b88b8aefba21,
        "onEndEditing": AS_TextField_d099c205f30b41dc87ad26c1bebe9849,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "75%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderlineAmount.setDefaultUnit(kony.flex.DP);
    flxUnderlineAmount.add();
    var lblAmount = new kony.ui.Label({
        "id": "lblAmount",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmountCurrencyInput = new kony.ui.Label({
        "id": "lblAmountCurrencyInput",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblBack",
        "text": "JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "34%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxConversionAmtBillDetailsPre = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "41%",
        "id": "flxConversionAmtBillDetailsPre",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "60%",
        "zIndex": 1
    }, {}, {});
    flxConversionAmtBillDetailsPre.setDefaultUnit(kony.flex.DP);
    var lblValBillDetailsPre = new kony.ui.Label({
        "id": "lblValBillDetailsPre",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblCurr",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConversionAmtBillDetailsPre.add(lblValBillDetailsPre);
    flxAmount.add(tbxAmount, flxUnderlineAmount, lblAmount, lblAmountCurrencyInput, flxConversionAmtBillDetailsPre);
    var lblAmountNote = new kony.ui.Label({
        "id": "lblAmountNote",
        "isVisible": false,
        "left": "5%",
        "skin": "sknLblWhite128C",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.AmountFieldNote"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxMinimumAmountInput = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxMinimumAmountInput",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMinimumAmountInput.setDefaultUnit(kony.flex.DP);
    var lblUnderlineMinAmountInput = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineMinAmountInput",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineMinAmountInput.setDefaultUnit(kony.flex.DP);
    lblUnderlineMinAmountInput.add();
    var lblMinAmount1Input = new kony.ui.Label({
        "height": "40%",
        "id": "lblMinAmount1Input",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.MinimumAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMinAmount2Input = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblMinAmount2Input",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMinimumAmountInput.add(lblUnderlineMinAmountInput, lblMinAmount1Input, lblMinAmount2Input);
    var flxMaximumAmountInput = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxMaximumAmountInput",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMaximumAmountInput.setDefaultUnit(kony.flex.DP);
    var lblUnderlineMaxAmountInput = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineMaxAmountInput",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineMaxAmountInput.setDefaultUnit(kony.flex.DP);
    lblUnderlineMaxAmountInput.add();
    var lblMaxAmount1Input = new kony.ui.Label({
        "height": "40%",
        "id": "lblMaxAmount1Input",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.MaximumAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMaxAmount2Input = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblMaxAmount2Input",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMaximumAmountInput.add(lblUnderlineMaxAmountInput, lblMaxAmount1Input, lblMaxAmount2Input);
    var flxSpace = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "15%",
        "id": "flxSpace",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSpace.setDefaultUnit(kony.flex.DP);
    flxSpace.add();
    var lblMinAmountService = new kony.ui.Label({
        "id": "lblMinAmountService",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMaxAmountService = new kony.ui.Label({
        "id": "lblMaxAmountService",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCommissionService = new kony.ui.Label({
        "id": "lblCommissionService",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBankCommissionService = new kony.ui.Label({
        "id": "lblBankCommissionService",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransCommissionService = new kony.ui.Label({
        "id": "lblTransCommissionService",
        "isVisible": false,
        "left": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    submainInputContainer.add(flxNickName, flxBillerCategory, flxBillerName, flxServiceType, flxDenomination, flxBillerNumber, lblserviceTypeHint, flxRadioAccCardsSelection, flxPaymentMode, flxAmount, lblAmountNote, flxMinimumAmountInput, flxMaximumAmountInput, flxSpace, lblMinAmountService, lblMaxAmountService, lblCommissionService, lblBankCommissionService, lblTransCommissionService);
    mainInputContainer.add(headerInputContainer, submainInputContainer);
    var mainOTPOnQuery = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "mainOTPOnQuery",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainOTPOnQuery.setDefaultUnit(kony.flex.DP);
    var headerOTPOnQuery = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "headerOTPOnQuery",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    headerOTPOnQuery.setDefaultUnit(kony.flex.DP);
    var backButtonContainerOTPOnQuery = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "backButtonContainerOTPOnQuery",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_bef50443c4b6467bbf7acaa917cda756,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    backButtonContainerOTPOnQuery.setDefaultUnit(kony.flex.DP);
    var lblBackIconOTPOnQuery = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIconOTPOnQuery",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBackOTPOnQuery = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackOTPOnQuery",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    backButtonContainerOTPOnQuery.add(lblBackIconOTPOnQuery, lblBackOTPOnQuery);
    var lblTitleOTPOnQuery = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitleOTPOnQuery",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.OTPOnQueryTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNextOTPOnQuery = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNextOTPOnQuery",
        "isVisible": true,
        "left": "86%",
        "onTouchStart": AS_Label_d5229a2b46bc4a8badf0e8bc93f016bc,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    headerOTPOnQuery.add(backButtonContainerOTPOnQuery, lblTitleOTPOnQuery, lblNextOTPOnQuery);
    var flxOTP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "91%",
        "id": "flxOTP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOTP.setDefaultUnit(kony.flex.DP);
    var lblInstruction3 = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInstruction3",
        "isVisible": true,
        "skin": "sknLblWhite",
        "text": kony.i18n.getLocalizedString("i18.Transfer.sentOTP"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPhoneNum = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPhoneNum",
        "isVisible": false,
        "skin": "sknLblWhite",
        "text": "***** ****",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxEnterOTP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50.00%",
        "clipBounds": true,
        "height": "8%",
        "id": "flxEnterOTP",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_a8fa69bb42d6460b93caddc9825e79d5,
        "skin": "slFbox",
        "top": "40%",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    flxEnterOTP.setDefaultUnit(kony.flex.DP);
    var txtOTP1 = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your One Time Password and click next"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "focusSkin": "sknTxtBox",
        "height": "100%",
        "id": "txtOTP1",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "onTextChange": AS_TextField_e8a808ab82854110b4fc0e4d717e1fb1,
        "secureTextEntry": false,
        "skin": "sknTbxTrans",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "0%",
        "zIndex": 10
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblTxt1 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt1",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblOTP150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTxt2 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt2",
        "isVisible": true,
        "left": "19.20%",
        "skin": "sknLblOTP150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTxt3 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt3",
        "isVisible": true,
        "left": "36.40%",
        "skin": "sknLblOTP150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTxt4 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt4",
        "isVisible": true,
        "left": "53.60%",
        "skin": "sknLblOTP150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTxt5 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt5",
        "isVisible": true,
        "left": "70.80%",
        "skin": "sknLblOTP150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTxt6 = new kony.ui.Label({
        "height": "100%",
        "id": "lblTxt6",
        "isVisible": true,
        "left": "88%",
        "skin": "sknLblOTP150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxLine1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine1.setDefaultUnit(kony.flex.DP);
    flxLine1.add();
    var flxLine2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "17.20%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine2.setDefaultUnit(kony.flex.DP);
    flxLine2.add();
    var flxLine3 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine3",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "34.40%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine3.setDefaultUnit(kony.flex.DP);
    flxLine3.add();
    var flxLine4 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine4",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "51.60%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine4.setDefaultUnit(kony.flex.DP);
    flxLine4.add();
    var flxLine5 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": -0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine5",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "68.80%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine5.setDefaultUnit(kony.flex.DP);
    flxLine5.add();
    var flxLine6 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "2dp",
        "id": "flxLine6",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "86%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "width": "14%",
        "zIndex": 1
    }, {}, {});
    flxLine6.setDefaultUnit(kony.flex.DP);
    flxLine6.add();
    var txtOTP = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "height": "1px",
        "id": "txtOTP",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
        "left": "0dp",
        "onTextChange": AS_TextField_f847b669e34c4482a914f6ba17fc14d7,
        "secureTextEntry": false,
        "skin": "sknTbxTrans",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "top": "0dp",
        "width": "1px",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxEnterOTP.add(txtOTP1, lblTxt1, lblTxt2, lblTxt3, lblTxt4, lblTxt5, lblTxt6, flxLine1, flxLine2, flxLine3, flxLine4, flxLine5, flxLine6, txtOTP);
    var lblInvalidCredentialsKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblInvalidCredentialsKA",
        "isVisible": false,
        "right": "5%",
        "skin": "sknInvalidCredKA",
        "text": kony.i18n.getLocalizedString("i18n.transfers.incorrectOTP"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnResendOTP = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yLabel": "Resend the One time password"
        },
        "centerX": "49.95%",
        "focusSkin": "slButtonGreenFocus",
        "height": "6%",
        "id": "btnResendOTP",
        "isVisible": true,
        "onClick": AS_Button_he0fcebb0fde4a30892e2a2b8055a333,
        "skin": "slButtonGreen",
        "text": kony.i18n.getLocalizedString("i18n.pinLogin.reSendPassword"),
        "top": "70.03%",
        "width": "55%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxTemporaryOTP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxTemporaryOTP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "45%",
        "width": "90%",
        "zIndex": 2
    }, {}, {});
    flxTemporaryOTP.setDefaultUnit(kony.flex.DP);
    var txtTemporaryOTP = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "25%",
        "centerX": "50%",
        "height": "50%",
        "id": "txtTemporaryOTP",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "2%",
        "maxTextLength": 6,
        "onDone": AS_TextField_a53aea7a6f3a4da49b26054dc6e48047,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "50%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlineTemporaryOTP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineTemporaryOTP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "75%",
        "width": "50%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineTemporaryOTP.setDefaultUnit(kony.flex.DP);
    flxUnderlineTemporaryOTP.add();
    flxTemporaryOTP.add(txtTemporaryOTP, flxUnderlineTemporaryOTP);
    flxOTP.add(lblInstruction3, lblPhoneNum, flxEnterOTP, lblInvalidCredentialsKA, btnResendOTP, flxTemporaryOTP);
    mainOTPOnQuery.add(headerOTPOnQuery, flxOTP);
    var mainBillsListContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "mainBillsListContainer",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "skncontainerBkg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainBillsListContainer.setDefaultUnit(kony.flex.DP);
    var headerBillsListContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "headerBillsListContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    headerBillsListContainer.setDefaultUnit(kony.flex.DP);
    var backButtonBillsListContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "backButtonBillsListContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_b63a3f6cfefe4d1ea6d3cff84eed7bc5,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    backButtonBillsListContainer.setDefaultUnit(kony.flex.DP);
    var lblBackIconBillsList = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIconBillsList",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBackBillsList = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackBillsList",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    backButtonBillsListContainer.add(lblBackIconBillsList, lblBackBillsList);
    var lblTitleBillsList = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitleBillsList",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.billsListTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    headerBillsListContainer.add(backButtonBillsListContainer, lblTitleBillsList);
    var submainBillsListContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "centerX": "50%",
        "clipBounds": true,
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "submainBillsListContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": 0,
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    submainBillsListContainer.setDefaultUnit(kony.flex.DP);
    var SegmentBillsList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "lblAmount1": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmount2": "",
            "lblPayeeRef1": kony.i18n.getLocalizedString("i18n.requeststatus.referencenumber"),
            "lblPayeeRef2": "",
            "lblPaymentDate1": kony.i18n.getLocalizedString("i18n.bills.IssueDate"),
            "lblPaymentDate2": ""
        }, {
            "lblAmount1": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmount2": "",
            "lblPayeeRef1": kony.i18n.getLocalizedString("i18n.requeststatus.referencenumber"),
            "lblPayeeRef2": "",
            "lblPaymentDate1": kony.i18n.getLocalizedString("i18n.bills.IssueDate"),
            "lblPaymentDate2": ""
        }, {
            "lblAmount1": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmount2": "",
            "lblPayeeRef1": kony.i18n.getLocalizedString("i18n.requeststatus.referencenumber"),
            "lblPayeeRef2": "",
            "lblPaymentDate1": kony.i18n.getLocalizedString("i18n.bills.IssueDate"),
            "lblPaymentDate2": ""
        }],
        "groupCells": false,
        "height": "100%",
        "id": "SegmentBillsList",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_ea52e46df9844535973783210331c96d,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "slSegSendMoney",
        "rowTemplate": mainBillListContainer,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "lblAmount1": "lblAmount1",
            "lblAmount2": "lblAmount2",
            "lblPayeeRef1": "lblPayeeRef1",
            "lblPayeeRef2": "lblPayeeRef2",
            "lblPaymentDate1": "lblPaymentDate1",
            "lblPaymentDate2": "lblPaymentDate2",
            "mainBillListContainer": "mainBillListContainer",
            "submainContainer1": "submainContainer1",
            "submainContainer2": "submainContainer2"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    submainBillsListContainer.add(SegmentBillsList);
    mainBillsListContainer.add(headerBillsListContainer, submainBillsListContainer);
    var mainBillDetailsContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainBillDetailsContainer",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainBillDetailsContainer.setDefaultUnit(kony.flex.DP);
    var headerBillDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "headerBillDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    headerBillDetailsContainer.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f0a8fa690eb747c7b6a0d6111b8c5561,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIconBillDetails = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIconBillDetails",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBackBillDetails = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopylblBackBillDetails",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIconBillDetails, CopylblBackBillDetails);
    var lblTitleBillDetails = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "70%",
        "id": "lblTitleBillDetails",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.transfer.billDetails"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCloseBillDetails = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCloseBillDetails",
        "isVisible": true,
        "left": "90%",
        "onTouchEnd": AS_Label_bed4c69626984ddfbf3d5c7865d4d353,
        "right": 0,
        "skin": "sknPostpaidClose",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    headerBillDetailsContainer.add(flxBack, lblTitleBillDetails, lblCloseBillDetails);
    var submainBillDetailsContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "90%",
        "horizontalScrollIndicator": true,
        "id": "submainBillDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    submainBillDetailsContainer.setDefaultUnit(kony.flex.DP);
    var flxBillerBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "14%",
        "id": "flxBillerBillDetails",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillerBillDetails.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "top": "20%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "text": "BH",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial);
    var flxDetail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDetail",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "2%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    flxDetail.setDefaultUnit(kony.flex.DP);
    var lblName = new kony.ui.Label({
        "id": "lblName",
        "isVisible": true,
        "left": "0dp",
        "skin": "slBillerDetailMain",
        "text": "Nick Name",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblType = new kony.ui.Label({
        "id": "lblType",
        "isVisible": false,
        "left": "0dp",
        "skin": "slBillerType",
        "text": "Prepaid",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblAccNumBiller",
        "text": "48574839485",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDetail.add(lblName, lblType, lblAccountNumber);
    flxBillerBillDetails.add(flxIcon1, flxDetail);
    var flxPayeeReferenceBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxPayeeReferenceBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPayeeReferenceBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlinePayeeReferenceBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlinePayeeReferenceBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlinePayeeReferenceBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlinePayeeReferenceBillDetails.add();
    var lblPayeeReference1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblPayeeReference1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.PayeeReference"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPayeeReference2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblPayeeReference2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPayeeReferenceBillDetails.add(lblUnderlinePayeeReferenceBillDetails, lblPayeeReference1BillDetails, lblPayeeReference2BillDetails);
    var flxDenominationBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxDenominationBillDetails",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDenominationBillDetails.setDefaultUnit(kony.flex.DP);
    var tbxPaymentModeBillDetails = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxPaymentModeBillDetails",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlinePaymentModeBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlinePaymentModeBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlinePaymentModeBillDetails.setDefaultUnit(kony.flex.DP);
    flxUnderlinePaymentModeBillDetails.add();
    var flxPaymentModeTypeHolderBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxPaymentModeTypeHolderBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxPaymentModeTypeHolderBillDetails.setDefaultUnit(kony.flex.DP);
    var lblPaymentModeDetails = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblPaymentModeDetails",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Denomination"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowPaymentModeDetails = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowPaymentModeDetails",
        "isVisible": true,
        "left": "90%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentModeTypeHolderBillDetails.add(lblPaymentModeDetails, lblArrowPaymentModeDetails);
    flxDenominationBillDetails.add(tbxPaymentModeBillDetails, flxUnderlinePaymentModeBillDetails, flxPaymentModeTypeHolderBillDetails);
    var flxDueAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxDueAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDueAmountBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineDueAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineDueAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineDueAmountBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineDueAmountBillDetails.add();
    var lblDueAmount1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblDueAmount1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.DueAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDueAmount2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblDueAmount2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDueAmountBillDetails.add(lblUnderlineDueAmountBillDetails, lblDueAmount1BillDetails, lblDueAmount2BillDetails);
    var flxFeeBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxFeeBillDetails",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxFeeBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlinFeeBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlinFeeBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlinFeeBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlinFeeBillDetails.add();
    var lblFeeAmount1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblFeeAmount1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.FeeAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFeeAmount2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblFeeAmount2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFeeBillDetails.add(lblUnderlinFeeBillDetails, lblFeeAmount1BillDetails, lblFeeAmount2BillDetails);
    var flxMinimumAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxMinimumAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMinimumAmountBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineMinAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineMinAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineMinAmountBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineMinAmountBillDetails.add();
    var lblMinAmount1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblMinAmount1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.MinimumAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMinAmount2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblMinAmount2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMinimumAmountBillDetails.add(lblUnderlineMinAmountBillDetails, lblMinAmount1BillDetails, lblMinAmount2BillDetails);
    var flxMaxAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxMaxAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxMaxAmountBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineMaxAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineMaxAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineMaxAmountBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineMaxAmountBillDetails.add();
    var lblMaxAmount1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblMaxAmount1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.MaximumAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblMaxAmount2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblMaxAmount2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxMaxAmountBillDetails.add(lblUnderlineMaxAmountBillDetails, lblMaxAmount1BillDetails, lblMaxAmount2BillDetails);
    var flxCommissionBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxCommissionBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxCommissionBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineCommissionBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineCommissionBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineCommissionBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineCommissionBillDetails.add();
    var lblCommission1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblCommission1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.commission"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCommission2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblCommission2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCommissionBillDetails.add(lblUnderlineCommissionBillDetails, lblCommission1BillDetails, lblCommission2BillDetails);
    var flxBankCommissionBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxBankCommissionBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBankCommissionBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineBankCommissionBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineBankCommissionBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineBankCommissionBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineBankCommissionBillDetails.add();
    var lblBankCommission1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblBankCommission1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.bankCommission"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBankCommission2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblBankCommission2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBankCommissionBillDetails.add(lblUnderlineBankCommissionBillDetails, lblBankCommission1BillDetails, lblBankCommission2BillDetails);
    var flxTransferCommissionBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxTransferCommissionBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTransferCommissionBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineTransferCommissionBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineTransferCommissionBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineTransferCommissionBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineTransferCommissionBillDetails.add();
    var lblTransferCommission1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblTransferCommission1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.transferCommission"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransferCommission2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblTransferCommission2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTransferCommissionBillDetails.add(lblUnderlineTransferCommissionBillDetails, lblTransferCommission1BillDetails, lblTransferCommission2BillDetails);
    var flxTotalAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxTotalAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTotalAmountBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineTotalAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineTotalAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineTotalAmountBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineTotalAmountBillDetails.add();
    var lblTotalAmount1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblTotalAmount1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.TotalAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTotalAmount2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblTotalAmount2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTotalAmountBillDetails.add(lblUnderlineTotalAmountBillDetails, lblTotalAmount1BillDetails, lblTotalAmount2BillDetails);
    var flxIssueDateBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxIssueDateBillDetails",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxIssueDateBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineIssueDateBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineIssueDateBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineIssueDateBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineIssueDateBillDetails.add();
    var lblAmountIssueDate1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblAmountIssueDate1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.IssueDate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmountIssueDate2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblAmountIssueDate2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "text": "10/02/2018",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIssueDateBillDetails.add(lblUnderlineIssueDateBillDetails, lblAmountIssueDate1BillDetails, lblAmountIssueDate2BillDetails);
    var flxDueDateBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxDueDateBillDetails",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxDueDateBillDetails.setDefaultUnit(kony.flex.DP);
    var lblUnderlineDueDateBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineDueDateBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    lblUnderlineDueDateBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineDueDateBillDetails.add();
    var lblDueDate1BillDetails = new kony.ui.Label({
        "height": "40%",
        "id": "lblDueDate1BillDetails",
        "isVisible": true,
        "left": "0%",
        "skin": "sknLblSmall",
        "text": kony.i18n.getLocalizedString("i18n.bills.DueDate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": "60%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDueDate2BillDetails = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "lblDueDate2BillDetails",
        "isVisible": true,
        "skin": "sknLblBack",
        "text": "10/02/2018",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDueDateBillDetails.add(lblUnderlineDueDateBillDetails, lblDueDate1BillDetails, lblDueDate2BillDetails);
    var flxRadioAccCardsSelectionBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxRadioAccCardsSelectionBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRadioAccCardsSelectionBillDetails.setDefaultUnit(kony.flex.DP);
    var lblBillsPayAccountsBillDetails = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillsPayAccountsBillDetails",
        "isVisible": true,
        "left": "15%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBillsPayAccountsBillDetails = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknRDOWhiteBRDBOJFont",
        "id": "btnBillsPayAccountsBillDetails",
        "isVisible": true,
        "left": "5%",
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "t",
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBillsPayCardsBillDetails = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillsPayCardsBillDetails",
        "isVisible": false,
        "left": "65%",
        "skin": "sknTransferType",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBillsPayCardsBillDetails = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "id": "btnBillsPayCardsBillDetails",
        "isVisible": false,
        "left": "55%",
        "skin": "sknRDOWhiteBRDBOJFont",
        "text": "s",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxRadioAccCardsSelectionBillDetails.add(lblBillsPayAccountsBillDetails, btnBillsPayAccountsBillDetails, lblBillsPayCardsBillDetails, btnBillsPayCardsBillDetails);
    var flxPaymentModeBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxPaymentModeBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxPaymentModeBillDetails.setDefaultUnit(kony.flex.DP);
    var tbxPaymentMode1BillDetails = new kony.ui.TextBox2({
        "accessibilityConfig": {
            "a11yLabel": "Enter your Username"
        },
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "60%",
        "id": "tbxPaymentMode1BillDetails",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "maxTextLength": null,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var flxUnderlinePaymentMode1BillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlinePaymentMode1BillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlinePaymentMode1BillDetails.setDefaultUnit(kony.flex.DP);
    flxUnderlinePaymentMode1BillDetails.add();
    var flxPaymentModeTypeHolder1BillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "4%",
        "centerX": "50%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxPaymentModeTypeHolder1BillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_dd204a657fee4890bbe541c6f6d7ebda,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxPaymentModeTypeHolder1BillDetails.setDefaultUnit(kony.flex.DP);
    var lblPaymentMode1BillDetails = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblPaymentMode1BillDetails",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblArrowPaymentMode1BillDetails = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowPaymentMode1BillDetails",
        "isVisible": true,
        "left": "90%",
        "skin": "sknBackIconDisabled",
        "text": "o",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentModeTypeHolder1BillDetails.add(lblPaymentMode1BillDetails, lblArrowPaymentMode1BillDetails);
    flxPaymentModeBillDetails.add(tbxPaymentMode1BillDetails, flxUnderlinePaymentMode1BillDetails, flxPaymentModeTypeHolder1BillDetails);
    var flxAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "85dp",
        "id": "flxAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAmountBillDetails.setDefaultUnit(kony.flex.DP);
    var tbxAmountBillDetails = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "22%",
        "centerX": "50%",
        "focusSkin": "sknTxtBox",
        "height": "40%",
        "id": "tbxAmountBillDetails",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "0%",
        "onDone": AS_TextField_g8113d9dfd5447f78729725a368dc770,
        "onTextChange": AS_TextField_ba959f1e52564bbb885b9fe80cab94ad,
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "onBeginEditing": AS_TextField_aede424c32084eca8305d1d39bcade3f,
        "onEndEditing": AS_TextField_h88425be2db84d1f948bc2b9ea6aba3d,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblUnderlineAmountBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "lblUnderlineAmountBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "1%",
        "isModalContainer": false,
        "skin": "sknFlxGreyLine",
        "top": "75%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    lblUnderlineAmountBillDetails.setDefaultUnit(kony.flex.DP);
    lblUnderlineAmountBillDetails.add();
    var lblAmountBillDetails = new kony.ui.Label({
        "id": "lblAmountBillDetails",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxConversionAmtBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "41%",
        "id": "flxConversionAmtBillDetails",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "68%",
        "zIndex": 1
    }, {}, {});
    flxConversionAmtBillDetails.setDefaultUnit(kony.flex.DP);
    var lblValBillDetails = new kony.ui.Label({
        "id": "lblValBillDetails",
        "isVisible": true,
        "left": "20dp",
        "skin": "sknLblCurr",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFromCurrBillDetails = new kony.ui.Label({
        "id": "lblFromCurrBillDetails",
        "isVisible": false,
        "left": "55%",
        "skin": "sknLblCurr",
        "text": "1 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblToCurrBillDetails = new kony.ui.Label({
        "id": "lblToCurrBillDetails",
        "isVisible": false,
        "left": "73%",
        "skin": "sknLblCurr",
        "text": "0.746464 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEqualsBillDetails = new kony.ui.Label({
        "id": "lblEqualsBillDetails",
        "isVisible": false,
        "left": "69%",
        "skin": "sknLblCurr",
        "text": "=",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConversionAmtBillDetails.add(lblValBillDetails, lblFromCurrBillDetails, lblToCurrBillDetails, lblEqualsBillDetails);
    var lblCurrencyAmt = new kony.ui.Label({
        "id": "lblCurrencyAmt",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "34%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmountBillDetails.add(tbxAmountBillDetails, lblUnderlineAmountBillDetails, lblAmountBillDetails, flxConversionAmtBillDetails, lblCurrencyAmt);
    var btnPayNowBillDetails = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "9%",
        "id": "btnPayNowBillDetails",
        "isVisible": true,
        "onClick": AS_Button_a403f54c86924edf8bea9e6b03ae5532,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.cards.paynow"),
        "top": "10%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxSpaceBillDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxSpaceBillDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxSpaceBillDetails.setDefaultUnit(kony.flex.DP);
    flxSpaceBillDetails.add();
    submainBillDetailsContainer.add(flxBillerBillDetails, flxPayeeReferenceBillDetails, flxDenominationBillDetails, flxDueAmountBillDetails, flxFeeBillDetails, flxMinimumAmountBillDetails, flxMaxAmountBillDetails, flxCommissionBillDetails, flxBankCommissionBillDetails, flxTransferCommissionBillDetails, flxTotalAmountBillDetails, flxIssueDateBillDetails, flxDueDateBillDetails, flxRadioAccCardsSelectionBillDetails, flxPaymentModeBillDetails, flxAmountBillDetails, btnPayNowBillDetails, flxSpaceBillDetails);
    mainBillDetailsContainer.add(headerBillDetailsContainer, submainBillDetailsContainer);
    var mainConfirmContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainConfirmContainer",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "-1dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainConfirmContainer.setDefaultUnit(kony.flex.DP);
    var headerConfirmContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "headerConfirmContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "skncontainerBkg",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    headerConfirmContainer.setDefaultUnit(kony.flex.DP);
    var backButtonContainerConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "backButtonContainerConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ie23cc141b7c4a0da7f4ff440d659a3b,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    backButtonContainerConfirm.setDefaultUnit(kony.flex.DP);
    var lblBackIconConfirm = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackIconConfirm",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBackConfirm = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBackConfirm",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    backButtonContainerConfirm.add(lblBackIconConfirm, lblBackConfirm);
    var lblTitleConfirm = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTitleConfirm",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCloseConfirm = new kony.ui.Label({
        "centerX": "94%",
        "height": "100%",
        "id": "lblCloseConfirm",
        "isVisible": true,
        "onTouchEnd": AS_Label_je234e97a5fd4379833364aeae8fbb36,
        "skin": "sknCloseConfirm",
        "text": "O",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    headerConfirmContainer.add(backButtonContainerConfirm, lblTitleConfirm, lblCloseConfirm);
    var submainConfirmContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "91%",
        "horizontalScrollIndicator": true,
        "id": "submainConfirmContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    submainConfirmContainer.setDefaultUnit(kony.flex.DP);
    var flxImpDetailsConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxImpDetailsConfirm",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxImpDetailsConfirm.setDefaultUnit(kony.flex.DP);
    var iconContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "iconContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "top": "20%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    iconContainer.setDefaultUnit(kony.flex.DP);
    var initialsCategory = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "initialsCategory",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "text": "AI",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    iconContainer.add(initialsCategory);
    var lblBillerCategoryConfirm = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBillerCategoryConfirm",
        "isVisible": true,
        "skin": "sknBeneTitle",
        "text": "Airtel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxImpDetailsConfirm.add(iconContainer, lblBillerCategoryConfirm);
    var flxBillerNameConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxBillerNameConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillerNameConfirm.setDefaultUnit(kony.flex.DP);
    var lblBillerNameConfirm1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblBillerNameConfirm1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.billerName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBillerNameConfirm2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblBillerNameConfirm2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerNameConfirm.add(lblBillerNameConfirm1, lblBillerNameConfirm2);
    var flxServiceTypeConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxServiceTypeConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxServiceTypeConfirm.setDefaultUnit(kony.flex.DP);
    var lblServiceTypeConfirm1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblServiceTypeConfirm1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.ServiceType"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblServiceTypeConfirm2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblServiceTypeConfirm2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxServiceTypeConfirm.add(lblServiceTypeConfirm1, lblServiceTypeConfirm2);
    var flxDenominationConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxDenominationConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDenominationConfirm.setDefaultUnit(kony.flex.DP);
    var lblDenominationConfirm1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblDenominationConfirm1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.Denomination"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDenominationConfirm2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblDenominationConfirm2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDenominationConfirm.add(lblDenominationConfirm1, lblDenominationConfirm2);
    var flxBillerNumberConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxBillerNumberConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBillerNumberConfirm.setDefaultUnit(kony.flex.DP);
    var lblBillerNumberConfirm1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblBillerNumberConfirm1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.billsPay.BillerNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBillerNumberConfirm2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblBillerNumberConfirm2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBillerNumberConfirm.add(lblBillerNumberConfirm1, lblBillerNumberConfirm2);
    var flxPayeeReferenceConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxPayeeReferenceConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPayeeReferenceConfirm.setDefaultUnit(kony.flex.DP);
    var lblPayeeReference1Confirm = new kony.ui.Label({
        "height": "50%",
        "id": "lblPayeeReference1Confirm",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.PalPay.PayeeReference"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPayeeReference2Confirm = new kony.ui.Label({
        "height": "50%",
        "id": "lblPayeeReference2Confirm",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPayeeReferenceConfirm.add(lblPayeeReference1Confirm, lblPayeeReference2Confirm);
    var flxDueAmountConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxDueAmountConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDueAmountConfirm.setDefaultUnit(kony.flex.DP);
    var lblDueAmountConfirm1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblDueAmountConfirm1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.bills.DueAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDueAmountConfirm2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblDueAmountConfirm2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDueAmountConfirm.add(lblDueAmountConfirm1, lblDueAmountConfirm2);
    var flxFeeAmountConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxFeeAmountConfirm",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFeeAmountConfirm.setDefaultUnit(kony.flex.DP);
    var lblFeeAmountConfirm1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblFeeAmountConfirm1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.bills.FeeAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFeeAmountConfirm2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblFeeAmountConfirm2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "1 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFeeAmountConfirm.add(lblFeeAmountConfirm1, lblFeeAmountConfirm2);
    var flxLCAmountConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxLCAmountConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLCAmountConfirm.setDefaultUnit(kony.flex.DP);
    var lblPaidAmountConfirm1 = new kony.ui.Label({
        "height": "50%",
        "id": "lblPaidAmountConfirm1",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.bills.PaidAmount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPaidAmountConfirm2 = new kony.ui.Label({
        "height": "50%",
        "id": "lblPaidAmountConfirm2",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLCAmountConfirm.add(lblPaidAmountConfirm1, lblPaidAmountConfirm2);
    var flxSourceofFundingConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSourceofFundingConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSourceofFundingConfirm.setDefaultUnit(kony.flex.DP);
    var lblConfirmEmailTitleConfirm = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmEmailTitleConfirm",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.bills.SourceOfFund"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSOFAccountConfirm = new kony.ui.Label({
        "height": "50%",
        "id": "lblSOFAccountConfirm",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSourceofFundingConfirm.add(lblConfirmEmailTitleConfirm, lblSOFAccountConfirm);
    var flxSourceofFundAccountConfirm = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxSourceofFundAccountConfirm",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSourceofFundAccountConfirm.setDefaultUnit(kony.flex.DP);
    var lblSOFAccNumConfirm = new kony.ui.Label({
        "height": "50%",
        "id": "lblSOFAccNumConfirm",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSourceofFundAccountConfirm.add(lblSOFAccNumConfirm);
    var flxConfirmBtn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "2%",
        "clipBounds": true,
        "height": "10%",
        "id": "flxConfirmBtn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmBtn.setDefaultUnit(kony.flex.DP);
    var confirmButton = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "100%",
        "id": "confirmButton",
        "isVisible": true,
        "onClick": AS_Button_f09d23cfb1a04b67b2304d04c0610f70,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.P2P.confirm"),
        "top": "0%",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBillerNumberConfirm = new kony.ui.Label({
        "height": "50%",
        "id": "lblBillerNumberConfirm",
        "isVisible": false,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "9000719069",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxConfirmBtn.add(confirmButton, lblBillerNumberConfirm);
    submainConfirmContainer.add(flxImpDetailsConfirm, flxBillerNameConfirm, flxServiceTypeConfirm, flxDenominationConfirm, flxBillerNumberConfirm, flxPayeeReferenceConfirm, flxDueAmountConfirm, flxFeeAmountConfirm, flxLCAmountConfirm, flxSourceofFundingConfirm, flxSourceofFundAccountConfirm, flxConfirmBtn);
    mainConfirmContainer.add(headerConfirmContainer, submainConfirmContainer);
    frmPalpayBillInformation.add(mainInputContainer, mainOTPOnQuery, mainBillsListContainer, mainBillDetailsContainer, mainConfirmContainer);
};

function frmPalpayBillInformationGlobals() {
    frmPalpayBillInformation = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmPalpayBillInformation,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": false,
        "id": "frmPalpayBillInformation",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "postShow": AS_Form_a5c53935880843e4bf01f3cc2f705ef6,
        "preShow": function(eventobject) {
            AS_Form_a88f5bc5907543fa83dad899c3abcf4c(eventobject);
        },
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_e87d2041b4a645cca443adeab36ff5eb,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};