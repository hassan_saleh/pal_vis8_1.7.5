/*
 * Controller Extension class for frmJoMoPayLandingConfig
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
/**
 * Creates a new Form Controller Extension.
 * @class frmJoMoPayLandingConfigControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmJoMoPayLandingConfigControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmJoMoPayLandingConfigControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
            kony.print("fetchData Response::" + JSON.stringify(response));
            if (response._raw_response_.segJomopayDetails.records[0].jomopaylist !== undefined && response._raw_response_.segJomopayDetails.records[0].jomopaylist !== null) {
                //            frmJoMoPayLanding.lblNoHistory.setVisibility(false);
                var resData = JSON.stringify(response._raw_response_.segJomopayDetails.records[0].jomopaylist);
                response = {};
                response = {
                    segJomopayDetails: {}
                };
                response.segJomopayDetails = JSON.parse(resData);
            } else {
                //           frmJoMoPayLanding.lblNoHistory.setVisibility(true);
                response = {};
                response = {
                    segJomopayDetails: []
                };
            }
            scopeObj.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmJoMoPayLandingConfigControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            kony.print("processData::" + JSON.stringify(data));
            if (data.segJomopayDetails !== undefined && data.segJomopayDetails !== null && data.segJomopayDetails.length !== 0) {
                var date = new Date();
                var currday = date.getDate() + "";
                var currmonth = (date.getMonth() + 1) + "";
                var curryear = date.getFullYear() + "";
                if (currday < 10) {
                    currday = "0" + currday;
                }
                if (currmonth < 10) {
                    currmonth = "0" + currmonth;
                }
                kony.print("currday::" + currday + " currmonth::" + currmonth + " curryear::" + curryear);
                for (var i = 0; i < data.segJomopayDetails.length; i++) {
                    kony.print("data.segJomopayDetails.paymentdate" + i + " ::" + data.segJomopayDetails[i].paymentdate);
                    data.segJomopayDetails[i].paymentamt = formatamountwithCurrency(data.segJomopayDetails[i].paymentamt, data.segJomopayDetails[i].frmAccountCurr) + " " + data.segJomopayDetails[i].frmAccountCurr;
                    var year = data.segJomopayDetails[i].paymentdate.substr(0, 4);
                    var month = data.segJomopayDetails[i].paymentdate.substr(5, 2);
                    var day = data.segJomopayDetails[i].paymentdate.substr(8, 2);
                    var hour = data.segJomopayDetails[i].paymentdate.substr(11, 2);
                    var min = data.segJomopayDetails[i].paymentdate.substr(14, 2);
                    var suffix = "";
                    if (hour === 12) {
                        suffix = "PM";
                    }
                    if (hour > 12) {
                        hour = hour - 12;
                        suffix = "PM";
                    } else {
                        suffix = "AM";
                        if (hour === 0) {
                            hour = 12;
                        }
                    }
                    kony.print("day::" + day + " month::" + month + " year::" + year);
                    kony.print("Before" + data.segJomopayDetails[i].paymentdate);
                    if (day === currday && month === currmonth && year === curryear) {
                        data.segJomopayDetails[i].paymentdate = geti18Value("i18n.common.today") + " " + hour + ":" + min + " " + suffix;
                    } else {
                        data.segJomopayDetails[i].paymentdate = day + "-" + month + "-" + year + " " + hour + ":" + min + " " + suffix;
                    }
                    kony.print("After" + data.segJomopayDetails[i].paymentdate);
                    //  data[i].paymentdate=
                }
            }
            var scopeObj = this;
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmJoMoPayLandingConfigControllerExtension#
     */
    bindData: function(data) {
        try {
            kony.print("bindData:::::::" + JSON.stringify(data));
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            if (data["segJomopayDetails"]["segJomopayDetails"].getData() && data["segJomopayDetails"]["segJomopayDetails"].getData().length > 0) {
                kony.print("Inside Data:");
                formmodel.setViewAttributeByProperty("segJomopayDetails", "isVisible", true);
                formmodel.setViewAttributeByProperty("lblNoHistory", "isVisible", false);
            } else {
                formmodel.setViewAttributeByProperty("segJomopayDetails", "isVisible", false);
                formmodel.setViewAttributeByProperty("lblNoHistory", "isVisible", true);
                formmodel.setViewAttributeByProperty('lblNoHistory', "text", geti18nkey("i18n.accounts.noTransaction"));
            }
            this.$class.$superp.bindData.call(this, data);
            this.getController().getFormModel().formatUI();
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmJoMoPayLandingConfigControllerExtension#
     */
    saveData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record
            kony.sdk.mvvm.log.info("success saving record ", res);
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmJoMoPayLandingConfigControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmJoMoPayLandingConfigControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});