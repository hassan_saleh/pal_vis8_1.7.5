//frmExternalAccountFunctions
//Used To Set By Default Domestic Account
function newExternalAccountPreshow() {
    //   frmAddExternalAccountKA.ImgDomestic.src="radioselected.png";
    //   frmAddExternalAccountKA.ImgInternational.src="radiononselected.png"; 
    //   frmAddExternalAccountKA.externalAccountCountry.setVisibility(false);
    //   frmAddExternalAccountKA.externalRountingNumberContainer.setVisibility(true);
    //   frmAddExternalAccountKA.externalSwiftCodeKA.setVisibility(false);
    //   frmAddExternalAccountKA.lblCountryNameKA.text = "";
    //   frmAddExternalAccountKA.lblInternaionalAccountKA.text = false;
    //frmAddExternalAccountKA.lblAccountTypeKA.text = "Savings Account";
    kony.boj.displayParameter("BOJ");
}

function isDomestic(res) {
    if (res) {
        frmAddExternalAccountKA.ImgDomestic.src = "radioselected.png";
        frmAddExternalAccountKA.ImgInternational.src = "radiononselected.png";
        frmAddExternalAccountKA.externalAccountCountry.setVisibility(false);
        frmAddExternalAccountKA.externalRountingNumberContainer.setVisibility(true);
        frmAddExternalAccountKA.externalSwiftCodeKA.setVisibility(false);
        //     frmAddExternalAccountKA.lblCountryNameKA.text = ""; changed
        frmAddExternalAccountKA.lblInternaionalAccountKA.text = false;
    } else {
        frmAddExternalAccountKA.ImgDomestic.src = "radiononselected.png";
        frmAddExternalAccountKA.ImgInternational.src = "radioselected.png";
        frmAddExternalAccountKA.externalAccountCountry.setVisibility(true);
        frmAddExternalAccountKA.externalRountingNumberContainer.setVisibility(false);
        frmAddExternalAccountKA.externalSwiftCodeKA.setVisibility(true);
        frmAddExternalAccountKA.lblInternaionalAccountKA.text = true;
    }
}

function confirmAddExternalAccount() {
    var status = false;
    var benficiryName = frmAddExternalAccountKA.txtBenficiryNameKA.text;
    var accountNumber = frmAddExternalAccountKA.externalAccountNumberTextField.text;
    var countryStatus = frmAddExternalAccountKA.externalAccountCountry.isVisible;
    if (countryStatus) {
        if (frmAddExternalAccountKA.ListCountryKA.selectedKey == -1) {
            frmAddExternalAccountKA.CopyLabel0b353ede7efad4c.skin = "sknD0021BLatoSemiBold";
            frmAddExternalAccountKA.CopyborderBottom0a9a2bb59bddb4c.skin = "sknFlxBGe2e2e2B1pxD0021B";
        } else {
            frmAddExternalAccountKA.CopyLabel0b353ede7efad4c.skin = "sknsectionHeaderLabel";
            frmAddExternalAccountKA.CopyborderBottom0a9a2bb59bddb4c.skin = "sknpickBorder";
        }
        var swiftCode = frmAddExternalAccountKA.txtSwiftCodeKA.text;
        if ((benficiryName === "") || (benficiryName === null)) {
            frmAddExternalAccountKA.CopyLabel0010f3d10f13a48.skin = "sknD0021BLatoSemiBold";
            frmAddExternalAccountKA.CopyborderBottom0d3bf42d5faa845.skin = "sknFlxBGe2e2e2B1pxD0021B";
        } else {
            frmAddExternalAccountKA.CopyLabel0010f3d10f13a48.skin = "sknsectionHeaderLabel";
            frmAddExternalAccountKA.CopyborderBottom0d3bf42d5faa845.skin = "sknpickBorder";
        }
        if ((accountNumber === "") || (accountNumber === null) || accountNumber.length !== 16) {
            frmAddExternalAccountKA.CopyLabel063c64beaee2545.skin = "sknD0021BLatoSemiBold";
            frmAddExternalAccountKA.CopyborderBottom0a26a506ce2d546.skin = "sknFlxBGe2e2e2B1pxD0021B";
            frmAddExternalAccountKA.lblAccnoErr.setVisibility(false);
            if (accountNumber !== null && accountNumber.length > 0 && accountNumber.length !== 16) frmAddExternalAccountKA.lblAccnoErr.setVisibility(true);
        } else {
            frmAddExternalAccountKA.CopyLabel063c64beaee2545.skin = "sknsectionHeaderLabel";
            frmAddExternalAccountKA.CopyborderBottom0a26a506ce2d546.skin = "sknpickBorder";
            frmAddExternalAccountKA.lblAccnoErr.setVisibility(false);
        }
        if ((swiftCode === "") || (swiftCode === null)) {
            frmAddExternalAccountKA.CopyLabel0cca3849cee9048.skin = "sknD0021BLatoSemiBold";
            frmAddExternalAccountKA.CopyborderBottom0795c152f323b46.skin = "sknFlxBGe2e2e2B1pxD0021B";
        } else {
            frmAddExternalAccountKA.CopyLabel0cca3849cee9048.skin = "sknsectionHeaderLabel";
            frmAddExternalAccountKA.CopyborderBottom0795c152f323b46.skin = "sknpickBorder";
        }
        if (frmAddExternalAccountKA.ListCountryKA.selectedKey !== -1 && (benficiryName !== "") && (benficiryName !== null) && (accountNumber !== "") && (accountNumber !== null) && (accountNumber.length == 16) && (swiftCode !== "") && (swiftCode !== null)) {
            status = true;
        }
    } else {
        var routingNumber = frmAddExternalAccountKA.txtExternalRoutingNumberKA.text;
        if ((benficiryName === "") || (benficiryName === null)) {
            frmAddExternalAccountKA.CopyLabel0010f3d10f13a48.skin = "sknD0021BLatoSemiBold";
            frmAddExternalAccountKA.CopyborderBottom0d3bf42d5faa845.skin = "sknFlxBGe2e2e2B1pxD0021B";
        } else {
            frmAddExternalAccountKA.CopyLabel0010f3d10f13a48.skin = "sknsectionHeaderLabel";
            frmAddExternalAccountKA.CopyborderBottom0d3bf42d5faa845.skin = "sknpickBorder";
        }
        if ((accountNumber === "") || (accountNumber === null) || accountNumber.length !== 16) {
            frmAddExternalAccountKA.CopyLabel063c64beaee2545.skin = "sknD0021BLatoSemiBold";
            frmAddExternalAccountKA.CopyborderBottom0a26a506ce2d546.skin = "sknFlxBGe2e2e2B1pxD0021B";
            frmAddExternalAccountKA.lblAccnoErr.setVisibility(false);
            if (accountNumber !== null && accountNumber.length > 0 && accountNumber.length !== 16) frmAddExternalAccountKA.lblAccnoErr.setVisibility(true);
        } else {
            frmAddExternalAccountKA.lblAccnoErr.setVisibility(false);
            frmAddExternalAccountKA.CopyLabel063c64beaee2545.skin = "sknsectionHeaderLabel";
            frmAddExternalAccountKA.CopyborderBottom0a26a506ce2d546.skin = "sknpickBorder";
        }
        if ((routingNumber === "") || (routingNumber === null) || routingNumber.length !== 9) {
            frmAddExternalAccountKA.CopyLabel0a393696e101f42.skin = "sknD0021BLatoSemiBold";
            frmAddExternalAccountKA.CopyborderBottom0h7be17a2ef9247.skin = "sknFlxBGe2e2e2B1pxD0021B";
            frmAddExternalAccountKA.lblRoutingnoErr.setVisibility(false);
            if (routingNumber !== null && routingNumber.length > 0 && routingNumber.length !== 9) frmAddExternalAccountKA.lblRoutingnoErr.setVisibility(true);
        } else {
            frmAddExternalAccountKA.lblRoutingnoErr.setVisibility(false);
            frmAddExternalAccountKA.CopyLabel0a393696e101f42.skin = "sknsectionHeaderLabel";
            frmAddExternalAccountKA.CopyborderBottom0h7be17a2ef9247.skin = "sknpickBorder";
        }
        if ((benficiryName !== "") && (benficiryName !== null) && (accountNumber !== "") && (accountNumber !== null) && (accountNumber.length === 16) && (routingNumber !== "") && (routingNumber !== null) && (routingNumber.length == 9)) {
            status = true;
        }
    }
    return status;
}

function clearSkins() {
    try {
        frmAddExternalAccountKA.CopyLabel0b353ede7efad4c.skin = "sknsectionHeaderLabel";
        frmAddExternalAccountKA.CopyborderBottom0a9a2bb59bddb4c.skin = "sknpickBorder";
        kony.print("1");
        frmAddExternalAccountKA.CopyLabel0010f3d10f13a48.skin = "sknsectionHeaderLabel";
        frmAddExternalAccountKA.CopyborderBottom0d3bf42d5faa845.skin = "sknpickBorder";
        kony.print("2");
        frmAddExternalAccountKA.CopyLabel063c64beaee2545.skin = "sknsectionHeaderLabel";
        frmAddExternalAccountKA.CopyborderBottom0a26a506ce2d546.skin = "sknpickBorder";
        kony.print("3");
        frmAddExternalAccountKA.CopyLabel0cca3849cee9048.skin = "sknsectionHeaderLabel";
        frmAddExternalAccountKA.CopyborderBottom0h7be17a2ef9247.skin = "sknpickBorder";
        kony.print("4");
        //       frmAddExternalAccountKA.CopyLabel0010f3d10f13a48.skin="sknsectionHeaderLabel";
        //       frmAddExternalAccountKA.CopyborderBottom0d3bf42d5faa845.skin = "sknpickBorder";
        //       frmAddExternalAccountKA.CopyLabel063c64beaee2545.skin = "sknsectionHeaderLabel";
        //       frmAddExternalAccountKA.CopyborderBottom0a26a506ce2d546.skin = "sknpickBorder"; 
        frmAddExternalAccountKA.CopyLabel0a393696e101f42.skin = "sknsectionHeaderLabel";
        kony.print("5");
        frmAddExternalAccountKA.lblRoutingnoErr.setVisibility(false);
        frmAddExternalAccountKA.lblAccnoErr.setVisibility(false);
    } catch (e) {
        kony.print("Exception_clearSkins ::" + e);
    }
}