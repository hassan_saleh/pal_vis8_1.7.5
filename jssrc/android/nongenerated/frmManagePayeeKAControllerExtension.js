/*
 * Controller Extension class for frmManagePayeeKA
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
/**
 * Creates a new Form Controller Extension.
 * @class frmManagePayeeKAControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmManagePayeeKAControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmManagePayeeKAControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            kony.sdk.mvvm.log.info("success fetching data ", response);
            kony.print("fetch biller list ::" + JSON.stringify(response));
            var navigationObject = this.getController() && this.getController().getContextData();
            navigationObject.setCustomInfo("managepayeesegment", response.managepayeesegment);
            serv_getPrePaidBillerData(response, scopeObj);
            //var formattedResponse = scopeObj.getController().processData(response);
            //scopeObj.bindData(formattedResponse);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            var Message = geti18nkey("i18n.common.somethingwentwrong");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmManagePayeeKAControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;
            //       var processedData = this.$class.$superp.processData.call(this, data);
            //       kony.print("Before Processing\n" + JSON.stringify(data));
            kony.boj.AllPayeeList = process_POSTPAID_PREPAID_BILLER_LIST(data._raw_response_.managepayeesegment.records[0]);
            kony.print("hassan AllPayeeList ::" + JSON.stringify(kony.boj.AllPayeeList));
            for (var i in kony.boj.AllPayeeList) {
                if (isEmpty(kony.boj.AllPayeeList[i].NickName)) {
                    kony.boj.AllPayeeList[i].NickName = kony.boj.AllPayeeList[i].ServiceType;
                }
                kony.boj.AllPayeeList[i].initial = kony.boj.AllPayeeList[i].NickName.substring(0, 2).toUpperCase();
                kony.boj.AllPayeeList[i].icon = {
                    backgroundColor: kony.boj.getBackGroundColour(kony.boj.AllPayeeList[i].initial)
                };
                kony.boj.AllPayeeList[i].lblTick = "";
                kony.boj.AllPayeeList[i].btnSetting = {
                    text: "q"
                };
                kony.boj.AllPayeeList[i].lblBulkSelection = {
                    isVisible: false
                };
                if (kony.boj.AllPayeeList[i].category === "prepaid") {
                    //kony.boj.AllPayeeList[i].btnEditBillerDetails ={"isVisible": false}; // hassan edit prepaid biller
                    // kony.boj.AllPayeeList[i].verticalDivider ={"isVisible": false}; // hassan edit prepaid biller
                    kony.boj.AllPayeeList[i].btnEditBillerDetails = {
                        "isVisible": true
                    }; // hassan edit prepaid biller
                    kony.boj.AllPayeeList[i].verticalDivider = {
                        "isVisible": true
                    }; // hassan edit prepaid biller
                } else {
                    kony.boj.AllPayeeList[i].btnEditBillerDetails = {
                        "isVisible": true
                    };
                    kony.boj.AllPayeeList[i].verticalDivider = {
                        "isVisible": true
                    };
                }
                kony.boj.AllPayeeList[i].dueamount = geti18Value("i18n.bills.DueAmount") + " :  " + setDecimal(kony.boj.AllPayeeList[i].dueamount, 3) + " JOD";
            }
            var processedData = {
                "managepayeesegment": {
                    "managepayeesegment": kony.boj.AllPayeeList
                }
            };
            // 		for(var i in data["managepayeesegment"]["managepayeesegment"])
            //           data["managepayeesegment"]["managepayeesegment"][i].initial = data["managepayeesegment"]["managepayeesegment"][i].NickName.substring(0, 2).toUpperCase();
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            var Message = geti18nkey("i18n.common.somethingwentwrong");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmManagePayeeKAControllerExtension#
     */
    bindData: function(data) {
        try {
            refreshAllAccounts();
            selectedIndex_BULK_PAYMENT = [];
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            formmodel.setViewAttributeByProperty("btnPayNow", "isVisible", true);
            formmodel.setViewAttributeByProperty("btnBulkPay", "isVisible", false);
            loadBillerDetails = false;
            if (data["managepayeesegment"]["managepayeesegment"] !== undefined && data["managepayeesegment"]["managepayeesegment"].length != 0) {
                this.$class.$superp.bindData.call(this, data);
                this.getController().getFormModel().formatUI();
                kony.boj.selectedBillerType = "PostPaid";
                this.getController().showForm();
            } else {
                frmPayBillHome.show();
            }
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        } catch (err) {
            var Message = geti18nkey("i18n.common.somethingwentwrong");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmManagePayeeKAControllerExtension#
     */
    saveData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record
            kony.sdk.mvvm.log.info("success saving record ", res);
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmManagePayeeKAControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmManagePayeeKAControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            bill_BULK_PAYMENT_COUNT = 0;
            formmodel.showView();
            frmPrePaidPayeeDetailsKA.destroy();
            frmAddNewPayeeKA.destroy();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    navigateTo: function(formId, navObject) {
        this.$class.$superp.navigateTo.call(this, formId, navObject);
    },
    navigateToPayeeTransaction: function() {
        navigateToPayeeTransactionDetails("frmManagePayeeKA", "frmPayeeTransactionsKA", "managepayeesegment");
    }
});