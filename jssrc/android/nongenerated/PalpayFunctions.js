kony = kony || {};
kony.boj = kony.boj || {};
kony.boj.selectedPayee = "";
kony.boj.AllPayeeList = [];
kony.boj.deletePayeeFrom = "";
kony.boj.deleteFlagPayee = false;
kony.boj.lang = "";
var gblFlow = "";
var gblBillType = "";
var gblSegDataTypes = "";
var Codes;
var clickOperation = "";
var gblVendorDetails = {};
var palpayAccountDetails = {};
//var flgConvertedAmount = false;
// kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
//   var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService(“serviceName”);
//   appMFConfiguration.invokeOperation(“operationName”, {},{"paramKey": ParamValue},
//                                      successCall,
//                                      failCallBack});
function fetchBillersInformationPalpay() {
    /*KNYMobileFabric = new kony.sdk();
          KNYMobileFabric.init("de868f68e0db476e369ce1d2ec311877", "3e42682a543882869eaf937bbd29cf84", "https://mobileuat.bankofjordan.com/authService/100000044/appconfig", successCallback, errorCallback);

          function successCallback(res) {
              serviceName = "prGetPalpayMappingCodes";
              integrationObj = KNYMobileFabric.getIntegrationService(serviceName);
              operationName = "prGetPalpayMappingCodes";
              request_data = {
                  "usr": "konyuat",
                  "pass": "konyuat_1"
              };
              headers = {};
              integrationObj.invokeOperation(operationName, headers, request_data, operationSuccess, operationFailure);*/
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetPalpayMappingCodes");
    appMFConfiguration.invokeOperation("prGetPalpayMappingCodes", {}, {
        "lang": kony.store.getItem("langPrefObj")
    }, operationSuccess, operationFailure);
}

function operationSuccess(res) {
    //alert("Successfully invocation");
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen();
    kony.print(JSON.stringify(res));
    Codes = res;
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function operationFailure(res) {
    //   Codes={ "MappingCodes":[
    //     {
    //       "origincurrency": "ILS",
    //       "equivalentcurrency": "ILS",
    //       "maxvalue": "200",
    //       "amountisacumulative": "",
    //       "vendorid": "1",
    //       "fieldlabel": "",
    //       "paymenttypename": "Utility",
    //       "hasquerycommission": "",
    //       "vendorcurrency": "ILS",
    //       "logourl": "http://palpay.ps/wp-content/uploads/2017/01/wataniya.png",
    //       "minvalue": "5",
    //       "requriresqueryauth": "",
    //       "catid": "",
    //       "servicetype": "TOPUP",
    //       "catvalue": "",
    //       "hasminmaxpayment": "",
    //       "originamount": "10",
    //       "equivalentamount": "10",
    //       "validatorexplanation": "The Mobile number should be 10 Digits e.g. 0566123456",
    //       "categoryname": "",
    //       "requriresamountinquery": "",
    //       "vendorname": "Ooredoo Mobile Prepaid Recharge",
    //       "paymenttype": "2",
    //       "categoryid": ""
    //     },
    //     {
    //       "origincurrency": "",
    //       "equivalentcurrency": "",
    //       "maxvalue": "9999.99",
    //       "amountisacumulative": "",
    //       "vendorid": "2",
    //       "fieldlabel": "Jawwal Bill Payment",
    //       "paymenttypename": "Fullpayment",
    //       "hasquerycommission": "",
    //       "vendorcurrency": "ILS",
    //       "logourl": "http://palpay.ps/wp-content/uploads/2017/01/jawwal.png",
    //       "minvalue": "0.01",
    //       "requriresqueryauth": "",
    //       "catid": "",
    //       "servicetype": "Full Payment",
    //       "catvalue": "",
    //       "hasminmaxpayment": "",
    //       "originamount": "",
    //       "equivalentamount": "",
    //       "validatorexplanation": "please enter your mobile number starting with 059",
    //       "categoryname": "",
    //       "requriresamountinquery": "true",
    //       "vendorname": "Jawwal Bill Payment",
    //       "paymenttype": "3",
    //       "categoryid": ""
    //     },
    //     {
    //       "origincurrency": "",
    //       "equivalentcurrency": "",
    //       "maxvalue": "9999.99",
    //       "amountisacumulative": "",
    //       "vendorid": "2",
    //       "fieldlabel": "Jawwal Bill Payment",
    //       "paymenttypename": "Fullpayment",
    //       "hasquerycommission": "",
    //       "vendorcurrency": "ILS",
    //       "logourl": "http://palpay.ps/wp-content/uploads/2017/01/jawwal.png",
    //       "minvalue": "0.01",
    //       "requriresqueryauth": "",
    //       "catid": "",
    //       "servicetype": "Full Payment",
    //       "catvalue": "",
    //       "hasminmaxpayment": "",
    //       "originamount": "",
    //       "equivalentamount": "",
    //       "validatorexplanation": "please enter your mobile number starting with 059",
    //       "categoryname": "",
    //       "requriresamountinquery": "",
    //       "vendorname": "Jawwal Bill Payment",
    //       "paymenttype": "3",
    //       "categoryid": ""
    //     }]};
    //alert(res);
}
/*    function errorCallback(res) {
        //code for failure call back
    }

    }*/
kony.boj.setAnimationToPayeeSegmentPal = function() {
    var lang = kony.store.getItem("langPrefObj");
    frmPalpayHome.managepayeesegment.rowTemplate = flxManagePayeeNormal;
};

function SHOW_BILLER_LIST_PAL(flow) {
    try {
        bill_BULK_PAYMENT_COUNT = 0;
        var temp = [];
        if (flow === "all") {
            for (var j in kony.boj.AllPayeeList) {
                kony.boj.AllPayeeList[j].btnSetting.text = "q";
                kony.boj.AllPayeeList[j].btnEditBillerDetails = {
                    "isVisible": true
                };
                kony.boj.AllPayeeList[j].verticalDivider = {
                    "isVisible": true
                };
                temp.push(kony.boj.AllPayeeList[j]);
            }
        } else {
            for (var i in kony.boj.AllPayeeList) {
                kony.boj.AllPayeeList[i].btnSetting.text = "q";
                if (kony.boj.AllPayeeList[i].category === flow) {
                    kony.boj.AllPayeeList[i].initial = kony.boj.AllPayeeList[i].NickName.substring(0, 2).toUpperCase();
                    kony.boj.AllPayeeList[i].lblTick = "";
                    kony.boj.AllPayeeList[i].lblBulkSelection = {
                        skin: "sknBulkPaymentUnSelection",
                        text: ""
                    };
                    kony.boj.AllPayeeList[i].btnEditBillerDetails = {
                        "isVisible": true
                    };
                    kony.boj.AllPayeeList[i].verticalDivider = {
                        "isVisible": true
                    };
                    temp.push(kony.boj.AllPayeeList[i]);
                }
            }
        }
        frmPalpayHome.managePayeeSegment.removeAll();
        frmPalpayHome.managePayeeSegment.widgetDataMap = {
            payeename: "NickName",
            accountnumber: "BillingNumber",
            lblBillerType: "BillerCode",
            lblInitial: "initial",
            flxIcon1: "icon",
            lblTick: "lblTick",
            btnEditBillerDetails: "btnEditBillerDetails",
            verticalDivider: "verticalDivider",
            lblBulkSelection: "lblBulkSelection"
        };
        frmPalpayHome.managePayeeSegment.setData(temp);
        if (temp.length === 0) {
            frmPalpayHome.managePayeeSegment.isVisible = false;
            frmPalpayHome.lblSearchResult.isVisible = true;
        } else {
            frmPalpayHome.managePayeeSegment.isVisible = true;
            frmPalpayHome.lblSearchResult.isVisible = false;
        }
    } catch (e) {
        kony.print("Exception_show_BILLER_LIST ::" + e);
    }
}
kony.boj.preshowManagePaypal = function() {
    kony.boj.deleteFlagPayee = false;
    frmPalpayHome.managePayeeSegment.isVisible = true;
    frmPalpayHome.lblSearchResult.isVisible = false;
    if (!isEmpty(frmPalpayHome.tbxSearch.text)) {
        SHOW_BILLER_LIST_PAl("all");
    }
    frmManagePayePal.tbxSearch.text = "";
};

function fieldsValidation() {
    var checkAmt = false;
    if (gblFlow === "PayPalAddFlow") {
        if (frmPalpayBillInformation.flxUnderlineNickName.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineBillerCategory.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineBillerName.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineServiceType.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineDenomination.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineBillerNumber.skin === "sknFlxGreenLine") {
            frmPalpayBillInformation.lblNext.skin = "sknLblNextEnabled";
        } else {
            frmPalpayBillInformation.lblNext.skin = "sknLblNextDisabled";
        }
    } else if (gblFlow === "PayPalpayOtherFlow" && gblBillType === "prepaid") {
        if (!isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
            checkAmt = true;
        } else if (frmPalpayBillInformation.lblDenomination.text !== geti18Value("i18n.billsPay.Denomination")) {
            checkAmt = true;
        } else checkAmt = false;
        if (frmPalpayBillInformation.flxUnderlineBillerName.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineServiceType.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineDenomination.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineBillerNumber.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlinePaymentMode.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineAmount.skin === "sknFlxGreenLine" && checkAmt === true) {
            frmPalpayBillInformation.lblNext.skin = "sknLblNextEnabled";
        }
    } else if (gblFlow === "PayPalpayOtherFlow" && gblBillType === "postpaid") {
        if (frmPalpayBillInformation.flxUnderlineBillerName.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineServiceType.skin === "sknFlxGreenLine" && frmPalpayBillInformation.flxUnderlineBillerNumber.skin === "sknFlxGreenLine") {
            kony.print("Postpaid");
            frmPalpayBillInformation.lblNext.skin = "sknLblNextEnabled";
        } else {
            frmPalpayBillInformation.lblNext.skin = "sknLblNextDisabled";
        }
    }
}

function fillMappingData() {
    tempArr = [];
    var i = 0,
        j = 0,
        count = 0;
    frmBillerListFormPal.segDetails.setData([]);
    frmBillerListFormPal.segDetails.widgetDataMap = {
        lblData: "type",
        imgInitial: "billerImage",
        lblInitial: "initial",
        flxIcon1: "icon"
    };
    if (gblSegDataTypes == "BillerName") {
        frmBillerListFormPal.flxSearch.setVisibility(true);
        frmBillerListFormPal.flxHeader.height = "15%";
        frmBillerListFormPal.flxMain.height = "85%";
        frmBillerListFormPal.flxTop.height = "55%";
        fetchBillersInformationPalpay();
        frmBillerListFormPal.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.BillerList");
        kony.print("Vendor List:" + JSON.stringify(Codes));
        count = 0;
        for (i in Codes.VendorList) {
            count = 0;
            for (j in Codes.MappingCodes) {
                if (Codes.VendorList[i].vendorname !== "") {
                    if (Codes.VendorList[i].vendorid === Codes.MappingCodes[j].vendorid) {
                        Codes.VendorList[i].billerImage = {
                            "src": Codes.MappingCodes[j].logourl,
                            "isVisible": true
                        };
                        Codes.VendorList[i].type = Codes.VendorList[i].vendorname;
                        Codes.VendorList[i].initial = {
                            "isVisible": false
                        };
                        Codes.VendorList[i].icon = {
                            "isVisible": false
                        };
                    }
                }
                count++;
            }
            tempArr.push(Codes.VendorList[i]);
        }
    } else if (gblSegDataTypes == "ServiceType") {
        frmBillerListFormPal.flxSearch.setVisibility(false);
        frmBillerListFormPal.flxHeader.height = "9%";
        frmBillerListFormPal.flxMain.height = "91%";
        frmBillerListFormPal.flxTop.height = "100%";
        var serviceArray = [];
        var check;
        count = 0;
        frmBillerListFormPal.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.BillerList");
        for (i in Codes.MappingCodes) {
            check = false;
            //kony.print("Codes.MappingCodes[i].vendorname "+Codes.VendorList[i].vendorname);
            kony.print("frmPalpayBillInformation.lblBillerName.text " + frmPalpayBillInformation.lblBillerName.text);
            if (Codes.MappingCodes[i].vendorid == gblVendorDetails.vendorid && Codes.MappingCodes[i].paymenttypename !== "") {
                // Codes.MappingCodes[i].initial = Codes.MappingCodes[i].paymenttypename.substring(0, 2).toUpperCase();
                Codes.MappingCodes[i].initial = {
                    "isVisible": true,
                    "text": Codes.MappingCodes[i].paymenttypename.substring(0, 2).toUpperCase()
                };
                Codes.MappingCodes[i].type = Codes.MappingCodes[i].paymenttypename;
                Codes.MappingCodes[i].icon = {
                    backgroundColor: kony.boj.getBackGroundColour(i)
                };
                Codes.MappingCodes[i].billerImage = {
                    "isVisible": false
                };
                gblVendorDetails.paymenttype = Codes.MappingCodes[i].paymenttype;
                gblVendorDetails.paymenttypename = Codes.MappingCodes[i].paymenttypename;
                serviceArray.push(Codes.MappingCodes[i].paymenttype);
                kony.print("serviceArray " + JSON.stringify(serviceArray));
                kony.print("COunt i " + i);
                for (j in serviceArray) {
                    kony.print("serviceArray[j] " + j + "  " + serviceArray[j]);
                    kony.print("Codes.MappingCodes[i].paymenttype " + "  " + Codes.MappingCodes[i].paymenttype);
                    if (serviceArray[j] === Codes.MappingCodes[i].paymenttype) check = true;
                }
                if (count === 0) check = false;
                kony.print("check " + check);
                if (!check) tempArr.push(Codes.MappingCodes[i]);
                count++;
            }
        }
    } else if (gblSegDataTypes == "Denomination") {
        frmBillerListFormPal.flxSearch.setVisibility(false);
        frmBillerListFormPal.flxHeader.height = "9%";
        frmBillerListFormPal.flxMain.height = "91%";
        frmBillerListFormPal.flxTop.height = "100%";
        frmBillerListFormPal.lblTitle.text = kony.i18n.getLocalizedString("i18n.billsPay.BillerList");
        for (i in Codes.MappingCodes) {
            if (Codes.MappingCodes[i].vendorid == gblVendorDetails.vendorid && Codes.MappingCodes[i].paymenttype == gblVendorDetails.paymenttype && Codes.MappingCodes[i].equivalentamount !== "") {
                //Codes.MappingCodes[i].initial = Codes.MappingCodes[i].paymenttypename.substring(0, 2).toUpperCase();
                Codes.MappingCodes[i].initial = {
                    "isVisible": true,
                    "text": Codes.MappingCodes[i].paymenttypename.substring(0, 2).toUpperCase()
                };
                Codes.MappingCodes[i].type = Codes.MappingCodes[i].equivalentamount + " " + Codes.MappingCodes[i].equivalentcurrency;
                Codes.MappingCodes[i].icon = {
                    backgroundColor: kony.boj.getBackGroundColour(i)
                };
                Codes.MappingCodes[i].billerImage = {
                    "isVisible": false
                };
                tempArr.push(Codes.MappingCodes[i]);
            }
        }
    }
    //   kony.boj.Biller[kony.boj.selectedBillerType].selectedList = tempArr;
    frmBillerListFormPal.segDetails.setData(tempArr);
    frmBillerListFormPal.show();
}

function goToNextScreenPal() {
    if (frmPalpayBillInformation.lblNext.skin === "sknLblNextEnabled" && gblFlow === "PayPalAddFlow") {
        //Nickname
    } else if (frmPalpayBillInformation.lblNext.skin === "sknLblNextEnabled" && gblFlow === "PayPalpayOtherFlow" && gblBillType === "prepaid") {
        gblVendorDetails.billingnumber = frmPalpayBillInformation.tbxBillerNumber.text;
        if (isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
            gblVendorDetails.totalamount = frmPalpayBillInformation.lblDenomination.text;
            frmPalpayBillInformation.lblDenominationConfirm2.text = frmPalpayBillInformation.lblDenomination.text + " " + gblVendorDetails.vendorcurrency;
            frmPalpayBillInformation.lblDueAmountConfirm2.text = gblVendorDetails.totalamount + " " + gblVendorDetails.vendorcurrency;
            frmPalpayBillInformation.flxDenominationConfirm.setVisibility(true);
        } else {
            gblVendorDetails.totalamount = frmPalpayBillInformation.tbxAmount.text;
            frmPalpayBillInformation.lblDueAmountConfirm2.text = gblVendorDetails.totalamount + " " + gblVendorDetails.vendorcurrency;
            frmPalpayBillInformation.flxDenominationConfirm.setVisibility(false);
        }
        fetchBillsCommissions();
        if (parseFloat(palpayAccountDetails.availableBalance) <= parseFloat(gblVendorDetails.exchangedAmount)) {
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        } else {
            frmPalpayBillInformation.lblBillerNameConfirm2.text = frmPalpayBillInformation.lblBillerName.text;
            frmPalpayBillInformation.lblServiceTypeConfirm2.text = frmPalpayBillInformation.lblServiceType.text;
            frmPalpayBillInformation.lblBillerNumberConfirm2.text = gblVendorDetails.billingnumber;
            frmPalpayBillInformation.lblPaidAmountConfirm2.text = gblVendorDetails.exchangedAmount + " " + palpayAccountDetails.currencyCode;
            frmPalpayBillInformation.lblSOFAccountConfirm.text = frmPalpayBillInformation.lblPaymentMode.text;
            frmPalpayBillInformation.flxPayeeReferenceConfirm.setVisibility(false);
            frmPalpayBillInformation.lblSOFAccNumConfirm.text = frmPalpayBillInformation.tbxPaymentMode.text;
            frmPalpayBillInformation.mainConfirmContainer.setVisibility(true);
            frmPalpayBillInformation.mainInputContainer.setVisibility(false);
        }
    } else if (frmPalpayBillInformation.lblNext.skin === "sknLblNextEnabled" && gblFlow === "PayPalpayOtherFlow" && gblBillType === "postpaid" && frmPalpayBillInformation.mainBillDetailsContainer.isVisible === false) {
        gblVendorDetails.billingnumber = frmPalpayBillInformation.tbxBillerNumber.text;
        if (frmPalpayBillInformation.mainInputContainer.isVisible === true && gblVendorDetails.requriresamountinquery === "true") {
            frmPalpayBillInformation.mainOTPOnQuery.setVisibility(true);
            frmPalpayBillInformation.mainInputContainer.setVisibility(false);
            getBillOTP();
        } else if (frmPalpayBillInformation.mainOTPOnQuery.isVisible === true && gblVendorDetails.requriresamountinquery === "true") {
            checkBillOTP();
        } else {
            //alert(custid + " " + gblVendorDetails.vendorid + " " + gblVendorDetails.billingnumber);
            fetchBillsDetails();
        }
    } else if (gblFlow === "PayPalpayOtherFlow" && gblBillType === "postpaid" && frmPalpayBillInformation.mainBillDetailsContainer.isVisible === true) {
        frmPalpayBillInformation.flxDenominationConfirm.setVisibility(false);
        frmPalpayBillInformation.flxPayeeReferenceConfirm.setVisibility(true);
        frmPalpayBillInformation.lblBillerNameConfirm2.text = frmPalpayBillInformation.lblBillerName.text;
        frmPalpayBillInformation.lblServiceTypeConfirm2.text = frmPalpayBillInformation.lblServiceType.text;
        frmPalpayBillInformation.lblBillerNumberConfirm2.text = gblVendorDetails.billingnumber;
        if (isEmpty(gblVendorDetails.payeeref)) {
            frmPalpayBillInformation.lblPayeeReference2Confirm.text = gblVendorDetails.billingcycle;
        } else {
            frmPalpayBillInformation.lblPayeeReference2Confirm.text = gblVendorDetails.payeeref;
        }
        frmPalpayBillInformation.lblDueAmountConfirm2.text = frmPalpayBillInformation.tbxAmountBillDetails.text + " " + gblVendorDetails.vendorcurrency;
        frmPalpayBillInformation.lblPaidAmountConfirm2.text = gblVendorDetails.exchangedAmount + " " + palpayAccountDetails.currencyCode;
        frmPalpayBillInformation.lblSOFAccountConfirm.text = frmPalpayBillInformation.lblPaymentMode1BillDetails.text;
        frmPalpayBillInformation.lblSOFAccNumConfirm.text = frmPalpayBillInformation.tbxPaymentMode1BillDetails.text;
        frmPalpayBillInformation.mainConfirmContainer.setVisibility(true);
        frmPalpayBillInformation.mainInputContainer.setVisibility(false);
        frmPalpayBillInformation.mainBillsListContainer.setVisibility(false);
        frmPalpayBillInformation.mainBillDetailsContainer.setVisibility(false);
    }
}

function addBillFlow() {
    gblFlow = "PayPalAddFlow";
    frmPalpayBillInformation.lblTitle.text = geti18Value("i18n.billsPay.AddNewBill");
    frmPalpayBillInformation.flxRadioAccCardsSelection.setVisibility(false);
    frmPalpayBillInformation.flxPaymentMode.setVisibility(false);
    frmPalpayBillInformation.flxAmount.setVisibility(false);
    frmConfirmPayBillPal.flxDueAmount.setVisibility(false);
    frmConfirmPayBillPal.flxLCAmount.setVisibility(false);
    frmConfirmPayBillPal.flxSourceofFunding.setVisibility(false);
    frmConfirmPayBillPal.flxSourceofFundAccount.setVisibility(false);
}

function payOtherBillsFlow() {
    gblFlow = "PayPalpayOtherFlow";
    frmPalpayBillInformation.lblTitle.text = geti18Value("i18n.PalPay.payABill");
    frmPalpayBillInformation.flxNickName.setVisibility(false);
    frmPalpayBillInformation.flxDenomination.setVisibility(false);
    frmPalpayBillInformation.flxRadioAccCardsSelection.setVisibility(false);
    frmPalpayBillInformation.flxPaymentMode.setVisibility(false);
    frmPalpayBillInformation.flxAmount.setVisibility(false);
    //   frmConfirmPayBillPal.flxDueAmount.setVisibility(true);
    //   frmConfirmPayBillPal.flxLCAmount.setVisibility(true);
    //   frmConfirmPayBillPal.flxSourceofFunding.setVisibility(true);
    //   frmConfirmPayBillPal.flxSourceofFundAccount.setVisibility(true);
}

function paypalConfirmation() {
    if (gblFlow === "PayPalAddFlow") {
        //Call add bill service
    } else if (gblFlow === "PayPalpayOtherFlow") {
        //Call pay other bills service
    }
}

function back_NEWBILLSPALPAY() {
    customAlertPopup(geti18nkey("i18n.cards.Confirmation"), geti18Value("i18n.Bene.backFromBeneError"), onClickYesClose_NEWBILLSPALPAY, onClickNoClose_NEWBILLSPALPAY, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}

function onClickYesClose_NEWBILLSPALPAY() {
    popupCommonAlertDimiss();
    //   var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    //   var controller = INSTANCE.getFormController("frmManagePayeeKA");
    //   var navObject = new kony.sdk.mvvm.NavigationObject();
    //   navObject.setRequestOptions("managepayeesegment",{"headers":{},"queryParams": {"custId": custid,
    //                               "P_BENE_TYPE": "PRE"
    //                              }});
    //   controller.loadDataAndShowForm(navObject);
    billInformationOnDeviceBack();
}

function onClickNoClose_NEWBILLSPALPAY() {
    popupCommonAlertDimiss();
}

function reternSelectedItem(selectedItem) {
    if (gblSegDataTypes == "BillerName") {
        gblVendorDetails.vendorid = selectedItem.vendorid;
        gblVendorDetails.vendorname = selectedItem.vendorname;
        frmPalpayBillInformation.lblBillerName.text = gblVendorDetails.vendorname;
        frmPalpayBillInformation.lblserviceTypeHint.setVisibility(false);
        frmPalpayBillInformation.flxUnderlineBillerName.skin = "sknFlxGreenLine";
        var currencyCode = "";
        kony.print("currencies " + JSON.stringify(gblCurrList));
        for (var i in gblCurrList) {
            if (gblVendorDetails.vendorcurrency === gblCurrList[i].CURR_ISO) {
                gblVendorDetails.vendorcurrencyCode = gblCurrList[i].CURR_CODE;
            }
        }
        kony.print("NART venCur" + gblVendorDetails.vendorcurrency + "venCode" + "gblVendorDetails.vendorcurrencyCode");
        clearFields();
    } else if (gblSegDataTypes == "ServiceType") {
        gblVendorDetails.paymenttype = selectedItem.paymenttype;
        gblVendorDetails.paymenttypename = selectedItem.paymenttypename;
        gblVendorDetails.vendorcurrency = selectedItem.vendorcurrency;
        gblVendorDetails.validatorexplanation = selectedItem.validatorexplanation;
        frmPalpayBillInformation.lblserviceTypeHint.text = gblVendorDetails.validatorexplanation;
        frmPalpayBillInformation.lblserviceTypeHint.setVisibility(true);
        gblVendorDetails.minvalue = selectedItem.minvalue;
        gblVendorDetails.maxvalue = selectedItem.maxvalue;
        gblVendorDetails.categoryid = selectedItem.categoryid;
        gblVendorDetails.categoryidname = selectedItem.categoryname;
        gblVendorDetails.requriresamountinquery = selectedItem.requriresamountinquery;
        frmPalpayBillInformation.lblServiceType.text = gblVendorDetails.paymenttypename;
        frmPalpayBillInformation.flxUnderlineServiceType.skin = "sknFlxGreenLine";
        frmPalpayBillInformation.lblAmountCurrencyInput.text = gblVendorDetails.vendorcurrency;
        if (gblVendorDetails.paymenttype == 2) {
            gblBillType = "prepaid";
            frmPalpayBillInformation.lblMinAmount2Input.text = gblVendorDetails.minvalue;
            frmPalpayBillInformation.lblMaxAmount2Input.text = gblVendorDetails.maxvalue;
            frmPalpayBillInformation.flxDenomination.setVisibility(true);
            frmPalpayBillInformation.flxPaymentMode.setVisibility(true);
            frmPalpayBillInformation.flxMinimumAmountInput.setVisibility(true);
            frmPalpayBillInformation.flxMaximumAmountInput.setVisibility(true);
            frmPalpayBillInformation.flxAmount.setVisibility(true);
            frmPalpayBillInformation.lblAmountNote.setVisibility(true);
        } else {
            gblBillType = "postpaid";
            frmPalpayBillInformation.flxDenomination.setVisibility(false);
            frmPalpayBillInformation.flxPaymentMode.setVisibility(false);
            frmPalpayBillInformation.flxMinimumAmountInput.setVisibility(false);
            frmPalpayBillInformation.flxMaximumAmountInput.setVisibility(false);
            frmPalpayBillInformation.flxAmount.setVisibility(false);
            frmPalpayBillInformation.lblAmountNote.setVisibility(false);
        }
        clearFields();
    } else if (gblSegDataTypes == "Denomination") {
        gblVendorDetails.minvalue = selectedItem.minvalue;
        gblVendorDetails.maxvalue = selectedItem.maxvalue;
        animateLabel("UP", "lblAmount", frmPalpayBillInformation.tbxAmount.text);
        frmPalpayBillInformation.lblDenomination.text = selectedItem.equivalentamount;
        frmPalpayBillInformation.tbxAmount.text = selectedItem.equivalentamount;
        //     if(isEmpty(frmPalpayBillInformation.tbxAmount.text)){
        //       gblVendorDetails.totalamount = frmPalpayBillInformation.lblDenomination.text;
        //     }else{
        gblVendorDetails.totalamount = selectedItem.equivalentamount;
        //     }
        frmPalpayBillInformation.flxUnderlineDenomination.skin = "sknFlxGreenLine";
        frmPalpayBillInformation.flxUnderlineAmount.skin = "sknFlxGreenLine";
        frmPalpayBillInformation.lblMinAmount2Input.text = gblVendorDetails.minvalue + " " + gblVendorDetails.vendorcurrency;
        frmPalpayBillInformation.lblMaxAmount2Input.text = gblVendorDetails.maxvalue + " " + gblVendorDetails.vendorcurrency;
        clearFields();
        fetchBillsCommissions();
    }
    fieldsValidation();
    frmPalpayBillInformation.show();
}

function clearFields() {
    if (gblSegDataTypes == "BillerName") {
        frmPalpayBillInformation.lblServiceType.text = geti18Value("i18n.billsPay.ServiceType");
        gblVendorDetails.paymenttype = "";
        frmPalpayBillInformation.flxUnderlineServiceType.skin = "sknFlxGreyLine";
        frmPalpayBillInformation.lblDenomination.text = geti18Value("i18n.billsPay.Denomination");
        frmPalpayBillInformation.flxUnderlineDenomination.skin = "sknFlxGreyLine";
    } else if (gblSegDataTypes == "ServiceType") {
        frmPalpayBillInformation.lblDenomination.text = geti18Value("i18n.billsPay.Denomination");
        frmPalpayBillInformation.flxUnderlineDenomination.skin = "sknFlxGreyLine";
    }
}

function getCommisionFeePal() {
    kony.print("getcomfee gblTModule ::" + gblTModule);
    //  if(gblTModule=="web"){
    if (kony.sdk.isNetworkAvailable()) {
        //  if(frmWebCharge.lblHiddenAmount.text != 0  && frmWebCharge.lblHiddenAmount.text != "."){
        kony.print("Comm From web charge module");
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Transactions", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Transactions");
        palpayAccountDetails = kony.store.getItem("PalpayFromAcc");
        var currencyCode = "";
        kony.print("country " + JSON.stringify(gblCurrList));
        for (var i in gblCurrList) {
            if (gblVendorDetails.vendorcurrency === gblCurrList[i].CURR_ISO) {
                currencyCode = gblCurrList[i].CURR_CODE;
            }
        }
        dataObject.addField("p_trnsf_cur", "35");
        dataObject.addField("accountNumber", palpayAccountDetails.accountID);
        dataObject.addField("ExternalAccountNumber", "35");
        if (isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
            dataObject.addField("amount", frmPalpayBillInformation.lblDenomination.text);
        } else if (!isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
            dataObject.addField("amount", frmPalpayBillInformation.tbxAmount.text);
        }
        dataObject.addField("TransferFlag", "T");
        dataObject.addField("p_billId", "");
        dataObject.addField("p_billNo", "");
        kony.print("dataobject - commission :: " + JSON.stringify(dataObject));
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        modelObj.customVerb("getFeeData", serviceOptions, getFeesSuccessPal, getFeesFailurePal);
        return;
    }
    //  }
    else { //Network not available
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        //alert("" + kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"));
        return;
    }
    return;
}

function getFeesSuccessPal(res) {
    kony.application.dismissLoadingScreen();
    kony.print("Result Commission :: " + JSON.stringify(res));
    palpayAccountDetails = kony.store.getItem("PalpayFromAcc");
    if (!isEmpty(res)) {
        if (isEmpty(res.ref_code)) {
            var errorMessage = geti18Value("i18n.common.somethingwentwrong");
            customAlertPopup(geti18Value("i18n.Bene.Failed"), errorMessage, popupCommonAlertDimiss, "");
        }
        kony.print("Successfully recieceved the fees2 ::" + JSON.stringify(res));
        //frmConfirmTransferKA.lblWebFees.text= setDecimal(parseFloat(res.transferResponse[0].comm_chrg||0),3)+" JOD";
        //frmConfirmTransferKA.lblWebAmt.text = setDecimal(parseFloat(res.transferResponse[0].lc_amt),3)+" JOD";
        //frmConfirmTransferKA.lblAfterBal.text = setDecimal((parseFloat((frmConfirmTransferKA.lblCurrBal.text.replace(" ","").replace("JOD","").replace(",",""))||0)+parseFloat(res.transferResponse[0].lc_amt)),3)+" JOD";
        //frmConfirmTransferKA.lblAfterBal.text = setDecimal((parseFloat(res.transferResponse[0].comm_chrg||0)+parseFloat(res.transferResponse[0].lc_amt)),3)+" JOD";
        //  frmConfirmTransferKA.hiddenlblCommRate.text = res.transferResponse[0].comm_chrg;
        //frmConfirmTransferKA.hiddenlblExchComm.text = res.transferResponse[0].exch_comm_chrg;
        if (palpayAccountDetails.availableBalance < res.transferResponse[0].cr_amt) {
            frmPalpayBillInformation.lblNext.skin = "sknLblNextDisabled";
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.lessamountvalue"), popupCommonAlertDimiss, "");
        } else {
            frmPalpayBillInformation.mainConfirmContainer.setVisibility(true);
            frmPalpayBillInformation.mainInputContainer.setVisibility(false);
        }
        //frmConfirmTransferKA.hiddenlblLclAmt.text = res.transferResponse[0].fees;
    }
}

function getFeesFailurePal(err) {
    kony.application.dismissLoadingScreen();
    if (err === undefined || err === null || err === "") {
        kony.print("getfees err found undefined");
        customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    } else {
        customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    }
    //alert("Not Success recieceved the fees ::" + JSON.stringify(err));
}

function fetchBillsDetails() {
    request_data = {
        "custID": custid,
        "billercode": gblVendorDetails.vendorid,
        "billingno": gblVendorDetails.billingnumber
    };
    kony.print("request_data ::" + JSON.stringify(request_data));
    headers = {};
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetPalpayBillDetails");
    appMFConfiguration.invokeOperation("prGetPalpayBillDetails", {}, request_data, fetchBillsDetailsSuccess, fetchBillsDetailsFailure);
}

function fetchBillsDetailsSuccess(res) {
    try {
        //kony.print("response details ::"+JSON.stringify(res));
        var tempArray = [];
        if (isEmpty(res.billDetails[0].payeeref) && isEmpty(res.billDetails[0].billingcycle)) {
            customAlertPopup(geti18Value("i18n.PalPay.noBillMessageTitle"), geti18Value("i18n.PalPay.noBillMessage"), popupCommonAlertDimiss, "");
        } else {
            var refOrCycle;
            var totalAmountWithCurrency;
            frmPalpayBillInformation.SegmentBillsList.setData([]);
            frmPalpayBillInformation.SegmentBillsList.widgetDataMap = {
                lblPaymentDate2: "paymentdate",
                lblPayeeRef2: "refOrCycle",
                lblAmount2: "totalAmountWithCurrency"
            };
            for (var i in res.billDetails) {
                res.billDetails[i].totalAmountWithCurrency = res.billDetails[i].totalamount + " " + res.billDetails[i].fca_tocurrency;
                if (isEmpty(res.billDetails[i].payeeref)) {
                    res.billDetails[i].refOrCycle = res.billDetails[i].billingcycle;
                } else {
                    res.billDetails[i].refOrCycle = res.billDetails[i].payeeref;
                }
                kony.print("response details of  ::" + JSON.stringify(res.billDetails[i]));
                tempArray.push(res.billDetails[i]);
            }
            frmPalpayBillInformation.SegmentBillsList.setData(tempArray);
            frmPalpayBillInformation.flxDenominationConfirm.setVisibility(false);
            frmPalpayBillInformation.mainBillsListContainer.setVisibility(true);
            frmPalpayBillInformation.mainInputContainer.setVisibility(false);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        }
    } catch (e) {
        kony.print("Exception in fetchBillsDetailsSuccess " + e);
    }
}

function fetchBillsDetailsFailure(err) {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    kony.print("error details " + JSON.stringify(err));
    customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18nkey("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
}

function onSelectDetail(selectedData) {
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    gblVendorDetails.payeeref = selectedData.payeeref;
    gblVendorDetails.billingcycle = selectedData.billingcycle;
    //gblVendorDetails.date = selectedData.date;
    gblVendorDetails.totalamount = selectedData.totalamount;
    gblVendorDetails.mindueamount = selectedData.mindueamount;
    gblVendorDetails.maxdueamount = selectedData.maxdueamount;
    gblVendorDetails.billamount = selectedData.billamount;
    gblVendorDetails.commission = selectedData.commission;
    gblVendorDetails.bankcommission = selectedData.bankcommission;
    gblVendorDetails.commissionon = selectedData.commissionon;
    gblVendorDetails.transcommission = selectedData.transcommission;
    fetchBillsCommissions();
    if (isEmpty(gblVendorDetails.payeeref)) {
        frmPalpayBillInformation.lblPayeeReference2BillDetails.text = gblVendorDetails.billingcycle;
    } else {
        frmPalpayBillInformation.lblPayeeReference2BillDetails.text = gblVendorDetails.payeeref;
    }
    frmPalpayBillInformation.lblMinAmount2BillDetails.text = gblVendorDetails.mindueamount + " " + gblVendorDetails.vendorcurrency;
    frmPalpayBillInformation.lblMaxAmount2BillDetails.text = gblVendorDetails.maxdueamount + " " + gblVendorDetails.vendorcurrency;
    frmPalpayBillInformation.lblDueAmount2BillDetails.text = gblVendorDetails.billamount + " " + gblVendorDetails.vendorcurrency;
    frmPalpayBillInformation.lblCommission2BillDetails.text = gblVendorDetails.commission + " " + gblVendorDetails.vendorcurrency;
    frmPalpayBillInformation.lblBankCommission2BillDetails.text = gblVendorDetails.bankcommission + " " + gblVendorDetails.vendorcurrency;
    frmPalpayBillInformation.lblTransferCommission2BillDetails.text = gblVendorDetails.transcommission + " " + gblVendorDetails.vendorcurrency;
    frmPalpayBillInformation.lblTotalAmount2BillDetails.text = selectedData.totalamount + " " + gblVendorDetails.vendorcurrency;
    frmPalpayBillInformation.lblCurrencyAmt.text = gblVendorDetails.vendorcurrency;
    animateLabel("UP", "lblAmountBillDetails", "");
    //if (gblVendorDetails.paymenttype == 3)
    frmPalpayBillInformation.tbxAmountBillDetails.text = selectedData.billamount;
    if (!isEmpty(frmPalpayBillInformation.tbxPaymentMode1BillDetails.text)) {
        frmPalpayBillInformation.flxUnderlinePaymentMode1BillDetails.skin = "sknFlxGreenLine";
    }
    if (!isEmpty(frmPalpayBillInformation.tbxAmountBillDetails.text)) {
        frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxGreenLine";
    }
    frmPalpayBillInformation.mainBillDetailsContainer.setVisibility(true);
    frmPalpayBillInformation.mainBillsListContainer.setVisibility(false);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function fetchBillsCommissions() {
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    //palpayAccountDetails = kony.store.getItem("PalpayFromAcc");
    //alert("palpayAccountDetails " + JSON.stringify(palpayAccountDetails));
    var currencyCode = "";
    //kony.print(gblCurrList);
    for (var i in gblCurrList) {
        if (gblVendorDetails.vendorcurrency === gblCurrList[i].CURR_ISO) {
            currencyCode = gblCurrList[i].CURR_CODE;
        }
        if (palpayAccountDetails.currencyCode === gblCurrList[i].CURR_ISO) {
            palpayAccountDetails.accountcurrCode = gblCurrList[i].CURR_CODE;
        }
    }
    gblVendorDetails.vendorcurrencycode = currencyCode;
    request_data = {
        "db_branch_code": palpayAccountDetails.branchNumber,
        "db_acc_num": palpayAccountDetails.accountID,
        "db_curr_code": palpayAccountDetails.accountcurrCode,
        "cr_curr_code": gblVendorDetails.vendorcurrencycode,
        "cr_vendor_curr": gblVendorDetails.vendorcurrencycode,
        "bill_trans_amt": gblVendorDetails.totalamount,
        "bill_comm_amt": gblVendorDetails.transcommission,
        "bank_comm": gblVendorDetails.commission,
        "vendor_id": gblVendorDetails.vendorid,
        "vendor_b_name": gblVendorDetails.vendorname,
        "vendor_s_name": gblVendorDetails.vendorname,
        "fieldValue": gblVendorDetails.billingnumber,
        "payeeRef": gblVendorDetails.payeeref,
        "payment_type": gblVendorDetails.paymenttype,
        "payment_type_name": gblVendorDetails.paymenttypename,
        "category_id": gblVendorDetails.categoryid,
        "category_id_name": gblVendorDetails.categoryidname,
        "p_channel": "MOBILE",
        "p_user_ID": "BOJMOB"
    };
    headers = {};
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetPalpayComm");
    appMFConfiguration.invokeOperation("prGetPalpayCommAmt", {}, request_data, fetchBillsCommissionsSuccess, fetchBillsCommissionsFailure);
}

function fetchBillsCommissionsSuccess(res) {
    kony.print("comm response " + JSON.stringify(res));
    if ((palpayAccountDetails.accountcurrCode === gblVendorDetails.vendorcurrencycode)) {
        frmPalpayBillInformation.flxConversionAmtBillDetailsPre.setVisibility(false);
        frmPalpayBillInformation.flxConversionAmtBillDetails.setVisibility(false);
    } else {
        if (gblFlow === "PayPalpayOtherFlow" && gblBillType === "prepaid" && !isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
            frmPalpayBillInformation.lblValBillDetailsPre.text = res.transAmount + " " + palpayAccountDetails.currencyCode;
            frmPalpayBillInformation.flxConversionAmtBillDetailsPre.setVisibility(true);
        } else if (gblFlow === "PayPalpayOtherFlow" && gblBillType === "postpaid" && !isEmpty(frmPalpayBillInformation.tbxAmountBillDetails.text)) {
            frmPalpayBillInformation.lblValBillDetails.text = res.transAmount + " " + palpayAccountDetails.currencyCode;
            frmPalpayBillInformation.flxConversionAmtBillDetails.setVisibility(true);
        }
    }
    kony.print("comm response " + res.transAmount);
    if (!isEmpty(res.transAmount)) {
        gblVendorDetails.exchangedAmount = res.transAmount;
    } else {
        gblVendorDetails.exchangedAmount = 0;
    }
    //flgConvertedAmount = true;
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function fetchBillsCommissionsFailure(err) {
    kony.print(err);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}
kony.boj.onClickYesPalpayCloseDetails = function() {
    popupCommonAlertDimiss();
    frmPaymentDashboard.show();
    frmPalpayBillInformation.destroy();
    frmPalpayHome.destroy();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
};

function billInformationOnDeviceBack() {
    if (gblFlow === "PayPalpayOtherFlow" && frmPalpayBillInformation.mainInputContainer.isVisible === true) {
        frmPalpayHome.show();
        frmPalpayBillInformation.destroy();
        gblFlow = "";
        gblBillType = "";
        gblVendorDetails = {};
        palpayAccountDetails = {};
    } else if (gblFlow === "PayPalpayOtherFlow" && frmPalpayBillInformation.mainBillsListContainer.isVisible === true) {
        frmPalpayBillInformation.mainInputContainer.setVisibility(true);
        frmPalpayBillInformation.mainBillsListContainer.setVisibility(false);
    } else if (gblFlow === "PayPalpayOtherFlow" && frmPalpayBillInformation.mainBillDetailsContainer.isVisible === true && gblBillType === "postpaid") {
        frmPalpayBillInformation.mainBillsListContainer.setVisibility(true);
        frmPalpayBillInformation.mainBillDetailsContainer.setVisibility(false);
    } else if (gblFlow === "PayPalpayOtherFlow" && frmPalpayBillInformation.mainConfirmContainer.isVisible === true && gblBillType === "postpaid") {
        frmPalpayBillInformation.mainBillDetailsContainer.setVisibility(true);
        frmPalpayBillInformation.mainConfirmContainer.setVisibility(false);
    } else if (gblFlow === "PayPalpayOtherFlow" && frmPalpayBillInformation.mainConfirmContainer.isVisible === true && gblBillType === "prepaid") {
        frmPalpayBillInformation.mainInputContainer.setVisibility(true);
        frmPalpayBillInformation.mainConfirmContainer.setVisibility(false);
    }
}

function getBillOTP() {
    //alert(custid + " " + gblVendorDetails.vendorid + " " + gblVendorDetails.billingnumber);
    request_data = {
        "p_customer_id": custid,
        "p_BillerCode": gblVendorDetails.vendorid,
        "p_BillingNo": gblVendorDetails.billingnumber
    };
    headers = {};
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetPalpayQueryAuth");
    appMFConfiguration.invokeOperation("prGetPalpayQueryAuth", {}, request_data, getBillOTPSuccess, getBillOTPFailure);
}

function getBillOTPSuccess(res) {
    alert(res);
}

function getBillOTPFailure(err) {
    alert(res);
}

function checkBillOTP() {
    request_data = {
        "p_customer_id": custid,
        "p_BillerCode": gblVendorDetails.vendorid,
        "p_BillingNo": gblVendorDetails.billingnumber,
        "p_AuthCode": frmPalpayBillInformation.txtTemporaryOTP.text
    };
    headers = {};
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetPalpayBillDetailsOtp");
    appMFConfiguration.invokeOperation("prGetPalpayBillDetailsOtp", {}, request_data, checkBillOTPSuccess, checkBillOTPFailure);
}

function checkBillOTPSuccess(res) {
    try {
        if (res.returnCode === "00000") {
            if (isEmpty(res.billDetails[0].payeeref)) {
                customAlertPopup(geti18Value("i18n.PalPay.noBillMessageTitle"), geti18Value("i18n.PalPay.noBillMessage"), popupCommonAlertDimiss, "");
            } else {
                frmPalpayBillInformation.SegmentBillsList.setData([]);
                frmPalpayBillInformation.SegmentBillsList.widgetDataMap = {
                    lblPaymentDate2: "paymentdate",
                    lblPayeeRef2: "payeeref",
                    lblAmount2: "totalamount"
                };
                var tempArray = [];
                kony.print("response details ::" + JSON.stringify(res));
                for (var i in res.billDetails) {
                    kony.print("response details of  ::" + JSON.stringify(res.billDetails[i]));
                    tempArray.push(res.billDetails[i]);
                }
                frmPalpayBillInformation.SegmentBillsList.setData(tempArray);
                frmPalpayBillInformation.flxDenominationConfirm.setVisibility(false);
                frmPalpayBillInformation.mainBillsListContainer.setVisibility(true);
                frmPalpayBillInformation.mainOTPOnQuery.setVisibility(false);
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            }
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup(geti18Value("i18n.PalPay.noBillMessageTitle"), geti18Value("i18n.PalPay.noBillMessage"), popupCommonAlertDimiss, "");
        }
    } catch (e) {
        kony.print("Exception in fetchBillsDetailsSuccess " + e);
    }
}

function checkBillOTPFailure(err) {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    alert(err);
    kony.print("error details " + JSON.stringify(err));
    //customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18nkey("i18n.common.somethingwentwrong"),popupCommonAlertDimiss,"");
}
kony.boj.searchBillerListPalpay = function(searchTerm) {
    var tempArr = [];
    var serviceArray = [];
    searchTerm = searchTerm.toLowerCase();
    frmBillerListFormPal.segDetails.setData([]);
    switch (gblSegDataTypes) {
        case "BillerName":
            frmBillerListFormPal.segDetails.widgetDataMap = {
                lblData: "type",
                imgInitial: "billerImage",
                lblInitial: "initial",
                flxIcon1: "icon"
            };
            kony.print("befor Codes.VendorList " + JSON.stringify(Codes.VendorList));
            for (var i in Codes.VendorList) {
                for (var j in Codes.MappingCodes) {
                    if (Codes.VendorList[i].vendorname !== "") {
                        if (Codes.VendorList[i].vendorid === Codes.MappingCodes[j].vendorid) {
                            Codes.VendorList[i].billerImage = {
                                "src": Codes.MappingCodes[j].logourl,
                                "isVisible": true
                            };
                            Codes.VendorList[i].initial = {
                                "isVisible": false
                            };
                            Codes.VendorList[i].icon = {
                                "isVisible": false
                            };
                        }
                    }
                }
                kony.print("after Codes.VendorList " + JSON.stringify(Codes.VendorList));
                if (Codes.VendorList[i].vendorname.toLowerCase().indexOf(searchTerm) !== -1) {
                    kony.print("name Codes.VendorList " + Codes.VendorList[i].vendorname);
                    Codes.VendorList[i].type = Codes.VendorList[i].vendorname;
                    tempArr.push(Codes.VendorList[i]);
                }
            }
            frmBillerListFormPal.segDetails.setData(tempArr);
            break;
            //     case "ServiceType":
            //       frmBillerListFormPal.segDetails.widgetDataMap = {
            //         lblData: "type",
            //         lblInitial: "initial",
            //         flxIcon1: "icon"
            //       };
            //       for (i in Codes.MappingCodes) {
            //         check = false;
            //         if (Codes.MappingCodes[i].paymenttypename !== "" && Codes.MappingCodes[i].vendorid == gblVendorDetails.vendorid){
            //           Codes.MappingCodes[i].initial = {"isVisible":true,"text":Codes.MappingCodes[i].paymenttypename.substring(0, 2).toUpperCase()};
            //           Codes.MappingCodes[i].type = Codes.MappingCodes[i].paymenttypename;
            //           Codes.MappingCodes[i].icon = {backgroundColor: kony.boj.getBackGroundColour(i)};
            //           Codes.MappingCodes[i].billerImage={"isVisible":false};
            //           // count++;
            //           if(Codes.MappingCodes[i].paymenttypename.toLowerCase().indexOf(searchTerm) !== -1)
            //           {
            //             serviceArray.push(Codes.MappingCodes[i].paymenttype);
            //             for (var j in serviceArray){
            //               if (serviceArray[j] === Codes.MappingCodes[i].paymenttype)
            //                 check=true;
            //             }
            //             //             if (count === 0)
            //             //               check=false;
            //             kony.print("check " + check);
            //             if (!check)
            //               tempArr.push(Codes.MappingCodes[i]);
            //             count++;
            //           }
            //         }
            //       }
            //       frmBillerListFormPal.segDetails.setData(tempArr);
            //       break;
            //     case "Denomination":
            //       frmBillerListFormPal.segDetails.widgetDataMap = {
            //         lblData: "type",
            //         lblInitial: "initial",
            //         flxIcon1: "icon"
            //       };
            //       for (i in Codes.MappingCodes) {
            //         if (Codes.MappingCodes[i].equivalentamount !== "" &&
            //             Codes.MappingCodes[i].vendorid == gblVendorDetails.vendorid &&
            //             Codes.MappingCodes[i].equivalentamount.toLowerCase().indexOf(searchTerm) !== -1 ){
            //           Codes.MappingCodes[i].initial ={"isVisible":true,"text":Codes.MappingCodes[i].equivalentamount.substring(0, 2).toUpperCase()};
            //           Codes.MappingCodes[i].type = Codes.MappingCodes[i].equivalentamount;
            //           Codes.MappingCodes[i].icon = {backgroundColor: kony.boj.getBackGroundColour(i)};
            //           Codes.MappingCodes[i].billerImage={"isVisible":false};
            //           tempArr.push(Codes.MappingCodes[i]);
            //         }
            //       }
            //       frmBillerListFormPal.segDetails.setData(tempArr);
            //       break;
    }
};

function getDefaultPaymentAccountDetails() {
    try {
        var accountsData = kony.retailBanking.globalData.accountsDashboardData.accountsData;
        for (var i = 0; i < accountsData.length; i++) {
            if (accountsData[i].accountID === gblDefaultAccPayment) {
                kony.print("Debug Data : " + JSON.stringify(accountsData[i]));
                if (!isEmpty(accountsData[i].AccNickName)) {
                    palpayAccountDetails.accountName = accountsData[i].AccNickName;
                } else {
                    palpayAccountDetails.accountName = accountsData[i].accountName;
                }
                palpayAccountDetails.accountID = accountsData[i].accountNumber;
                palpayAccountDetails.branchNumber = accountsData[i].branchNumber;
                palpayAccountDetails.currencyCode = accountsData[i].currencyCode;
                palpayAccountDetails.availableBalance = accountsData[i].availableBalance;
                var availableBalPalpay = accountsData[i].availableBalance;
                //alert("availableBalPalpay1111 "+availableBalPalpay);
                //alert(palpayAccountDetails.availableBalance);
                availableBalPalpay = availableBalPalpay.replace(/,/g, "");
                //alert("availableBalPalpay2222 "+availableBalPalpay);
                palpayAccountDetails.availableBalance = parseFloat(availableBalPalpay);
                //alert(palpayAccountDetails.availableBalance);
                for (var j in gblCurrList) {
                    if (palpayAccountDetails.currencyCode === gblCurrList[j].CURR_ISO) {
                        palpayAccountDetails.accountcurrCode = gblCurrList[j].CURR_CODE;
                    }
                }
            }
        }
    } catch (e) {
        kony.print("gblDefaultAccPayment :: catch  :: " + e);
    }
}