var frmManagePayeeKAConfig = {
    "formid": "frmManagePayeeKA",
    "frmManagePayeeKA": {
        "entity": "Payee",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        },
    },
    "managepayeesegment": {
        "fieldprops": {
            "widgettype": "Segment",
            "entity": "Payee",
            "additionalFields": ["ServiceType"],
            "field": {
                "payeename": {
                    "widgettype": "Label",
                    "field": "NickName"
                },
                "accountnumber": {
                    "widgettype": "Label",
                    "field": "BillingNumber"
                },
                "lblBillerType": {
                    "widgettype": "Label",
                    "field": "BillerCode"
                },
                "lblInitial": {
                    "widgettype": "Label",
                    "field": "initial"
                },
                "flxIcon1": {
                    "widgettype": "FlexContainer",
                    "field": "icon"
                },
                "dueAmount": {
                    "widgettype": "Label",
                    "field": "dueamount"
                },
                "lblTick": {
                    "widgettype": "Label",
                    "field": "lblTick"
                },
                "btnEditBillerDetails": {
                    "widgettype": "Label",
                    "field": "btnEditBillerDetails"
                },
                "verticalDivider": {
                    "widgettype": "Label",
                    "field": "verticalDivider"
                },
                "lblBulkSelection": {
                    "widgettype": "Label",
                    "field": "lblBulkSelection"
                }
            }
        }
    }
};