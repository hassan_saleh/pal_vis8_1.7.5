//Type your code here
function showReleaseNotesForm() {
    kony.print("showReleaseNotesForm:");
    var segData = {};
    var segDataArray = [];
    if (!isEmpty(ObjReleaseNotes)) {
        for (var i in ObjReleaseNotes) {
            segData = {
                "imgBullet": "current_location.png",
                "lblReleaseNotes": ObjReleaseNotes[i].releaseNote
            };
            segDataArray.push(segData);
        }
        frmReleaseNotesKA.segReleaseNotes.setData(segDataArray);
        frmReleaseNotesKA.show();
    } else {
        alert("No Data");
    }
}