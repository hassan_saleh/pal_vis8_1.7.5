function iWatchAccTransactionCall(serv_Data, transaction_DATE) {
    kony.print("Iwatch :: Inside iWatchAccTransactionCall for :: " + JSON.stringify(serv_Data));
    if (kony.sdk.isNetworkAvailable()) {
        var scopeObj = this;
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects", options);
        var headers = {};
        var dataObject = new kony.sdk.dto.DataObject("Transactions");
        var Language = "eng"; //kony.store.getItem("langPrefObj") === "en" ? "eng" : "ara";
        var toDATE = "";
        var fromDATE = "";
        var no_TNX = "100";
        if (transaction_DATE !== undefined && transaction_DATE !== null && transaction_DATE !== "") {
            if (transaction_DATE.no_TNX !== undefined && transaction_DATE.no_TNX !== "") {
                no_TNX = transaction_DATE.no_TNX;
            } else if (transaction_DATE.toDATE !== "" && transaction_DATE.fromDATE !== "") {
                toDATE = transaction_DATE.toDATE;
                fromDATE = transaction_DATE.fromDATE;
                no_TNX = "";
            }
        }
        gblAccountsCurrencyType = serv_Data.currencyCode;
        dataObject.addField("custId", iWatchcustid);
        dataObject.addField("accountNumber", serv_Data.accountID);
        dataObject.addField("p_Branch", serv_Data.branchNumber);
        dataObject.addField("p_no_Of_Txn", "5");
        dataObject.addField("p_toDate", transaction_DATE.toDATE || "");
        dataObject.addField("p_fromDate", transaction_DATE.fromDATE || "");
        dataObject.addField("lang", Language);
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("Iwatch ::serviceOptions for iWatchAccTransactionCall : " + JSON.stringify(serviceOptions));
        objectService.customVerb("IwatchAccTransactions", serviceOptions, iWatchAccTransactionCallSuccessCallback, iWatchAccTransactionCallErrorCallback);
    } else {
        return_watchrequest({
            "transactions": [],
            "isUserLoggedIn": true,
            "isNetworkAvailable": false,
            "isRetry": true
        });
    }
}

function iWatchAccTransactionCallSuccessCallback(data) {
    kony.print("Iwatch ::iWatchAccTransactionCallSuccessCallback res :: " + JSON.stringify(data));
    if ((!isEmpty(data.Transactions))) {
        var tempdata = iWatchsetAccountDetailData(data.Transactions, "", "ss");
        if ((!isEmpty(tempdata))) return_watchrequest({
            "transactions": kony.retailBanking.globalData.accountsTransactionList.transaction,
            "isUserLoggedIn": true,
            "isNetworkAvailable": true,
            "isRetry": true
        });
        else return_watchrequest({
            "transactions": [],
            "isUserLoggedIn": true,
            "isNetworkAvailable": true,
            "isRetry": true
        });
    } else {
        return_watchrequest({
            "transactions": [],
            "isUserLoggedIn": true,
            "isNetworkAvailable": true,
            "isRetry": true
        });
    }
}

function iWatchAccTransactionCallErrorCallback(res) {
    kony.print("Iwatch ::iWatchAccTransactionCallErrorCallback  res :: " + JSON.stringify(res));
    return_watchrequest({
        "transactions": [],
        "isUserLoggedIn": true,
        "isNetworkAvailable": true,
        "isRetry": false
    });
}

function iWatchsetAccountDetailData(Data, recSeg, flag) {
    try {
        kony.print("Iwatch :: Inside iWatchsetAccountDetailData Data ::" + JSON.stringify(Data) + " ::  " + recSeg);
        var transacData = Data;
        var recentSegData = [];
        var scheduleSegData = [];
        var nextSet = 0;
        var total = 0;
        kony.print("Iwatch ::primaryKeyValueMap ::" + transacData.length);
        if (!isEmpty(transacData[0].transactionId)) {
            for (var i in transacData) {
                if (transacData[i]["amount"] == null || transacData[i]["amount"] == "null") {
                    transacData[i]["amount"] = "0.000";
                }
                transacData[i]["description"] = {
                    "text": transacData[i]["description"],
                    "isVisible": true
                };
                transacData[i]["amount"] = formatamountwithCurrency(transacData[i]["amount"], gblAccountsCurrencyType);
                kony.print("Iwatch ::transaction amount ::" + transacData[i]["amount"]);
                if (((transacData[i]["category"] != null) && (transacData[i]["category"] != undefined)) && (transacData[i]["category"] == "C")) {
                    transacData[i]["amount"] = {
                        "text": "+ " + transacData[i]["amount"] + " " + iwatch_ACCOUNT_DETAILS.currencyCode,
                        "isVisible": true
                    };
                    transacData[i]["category"] = "credit";
                } else if (((transacData[i]["category"] != null) && (transacData[i]["category"] != undefined)) && (transacData[i]["category"] == "D")) {
                    transacData[i]["amount"] = {
                        "text": "- " + transacData[i]["amount"] + " " + iwatch_ACCOUNT_DETAILS.currencyCode,
                        "isVisible": true
                    };
                    transacData[i]["category"] = "debit";
                }
                if (transacData[i]["category"] === undefined) {
                    kony.print("Iwatch ::inside uncleared transaction");
                    total = total + parseFloat(transacData[i]["amount"].replace(/,/g, ""));
                    transacData[i]["amount"] = {
                        "text": transacData[i]["amount"] + " " + iwatch_ACCOUNT_DETAILS.currencyCode,
                        "isVisible": true
                    };
                    var outp1 = transacData[i]["transactionDate"].split('-');
                    transacData[i]["transactionDate"] = {
                        "text": outp1[2] + "/" + outp1[1] + "/" + outp1[0],
                        "isVisible": true
                    };
                    recentSegData.push(transacData[i]);
                } else {
                    var outp = transacData[i]["transactionDate"].split('-');
                    transacData[i]["transactionDate"] = {
                        "text": outp[2] + "/" + outp[1] + "/" + outp[0],
                        "isVisible": true
                    };
                    kony.print("Iwatch ::formatted date ::" + transacData[i]["transactionDate"]);
                    scheduleSegData.push(transacData[i]); //kony.retailBanking.globalData.accountsTransactionList
                }
            }
            kony.retailBanking.globalData.accountsTransactionList = {
                "transaction": scheduleSegData,
                "unClearedTransaction": recentSegData,
                "unClearedTransactionTotal": total
            };
            var value1 = "";
            var value2 = "";
            var temp = "";
            kony.print("Iwatch ::scheduleSegData ::" + JSON.stringify(scheduleSegData[0]));
            for (var l = 0; l < scheduleSegData[0].length; l++) {
                for (var k = l + 1; k < scheduleSegData[0].length; k++) {
                    value1 = scheduleSegData[0][l].transactionDate.text.substring(3, 5) + "/" + scheduleSegData[0][l].transactionDate.text.substring(0, 2) + "/" + scheduleSegData[0][l].transactionDate.text.substring(6, scheduleSegData[0][l].transactionDate.text.length);
                    value2 = scheduleSegData[0][k].transactionDate.text.substring(3, 5) + "/" + scheduleSegData[0][k].transactionDate.text.substring(0, 2) + "/" + scheduleSegData[0][k].transactionDate.text.substring(6, scheduleSegData[0][k].transactionDate.text.length);
                    if ((new Date(value1) < new Date(value2))) {
                        temp = scheduleSegData[0][l];
                        scheduleSegData[0][l] = scheduleSegData[0][k];
                        scheduleSegData[0][k] = temp;
                    }
                }
            }
        } else {
            kony.retailBanking.globalData.accountsTransactionList = {
                "transaction": [],
                "unClearedTransaction": [],
                "unClearedTransactionTotal": 0
            };
        }
        return [scheduleSegData, recentSegData];
    } catch (err) {
        kony.print("Iwatch :: Catch of Error in setAccountDetailData :: " + err);
        return_watchrequest({
            "transactions": [],
            "isUserLoggedIn": true,
            "isNetworkAvailable": true,
            "isRetry": false
        });
    }
}