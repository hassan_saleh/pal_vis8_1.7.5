function initializetmpCardNumber() {
    flxCardNumber = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxCardNumber.setDefaultUnit(kony.flex.DP);
    var lblCardNum = new kony.ui.Label({
        "id": "lblCardNum",
        "isVisible": false,
        "left": "20%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardHolderName = new kony.ui.Label({
        "id": "lblCardHolderName",
        "isVisible": true,
        "left": "10%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCardNumberContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40%",
        "id": "flxCardNumberContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10%",
        "skin": "slFbox",
        "top": "25dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxCardNumberContainer.setDefaultUnit(kony.flex.DP);
    var lblCardType = new kony.ui.Label({
        "id": "lblCardType",
        "isVisible": true,
        "left": "0%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblHiddenCardNum = new kony.ui.Label({
        "id": "lblHiddenCardNum",
        "isVisible": false,
        "left": "0%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardNumberContainer.add(lblCardType, lblHiddenCardNum);
    flxCardNumber.add(lblCardNum, lblCardHolderName, flxCardNumberContainer);
}