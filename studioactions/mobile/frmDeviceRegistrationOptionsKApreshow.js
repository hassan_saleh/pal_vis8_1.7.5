function frmDeviceRegistrationOptionsKApreshow(eventobject) {
    return AS_Form_f73604e1824b44dbab530d4b72db6950(eventobject);
}

function AS_Form_f73604e1824b44dbab530d4b72db6950(eventobject) {
    diffFlowFlag = "LOGINFLOW";
    show_DEVICEREGISTRATIONOPTIONCONTENT();
    frmDeviceRegistrationOptionsKA.enableTouchID.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.login.cRegisterDevice"),
        "a11yLabel": "",
        "a11yHint": ""
    }
    frmDeviceRegistrationOptionsKA.noThanks.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.common.noThanks"),
        "a11yLabel": "",
        "a11yHint": ""
    }
}