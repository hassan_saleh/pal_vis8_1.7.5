function frmPaymentDashboard_btnPalpay_onClick(eventobject) {
    return AS_Button_e54d79bc1ef6476b97753a96d8dda45e(eventobject);
}

function AS_Button_e54d79bc1ef6476b97753a96d8dda45e(eventobject) {
    frmPalpayBillInformation.destroy();
    if (kony.store.getItem("langPrefObj") === "en") kony.boj.lang = "eng";
    else kony.boj.lang = "ara";
    gblTModule = "palpay";
    setgblcurrencyfullCountryListt(); //Added this line to fix Palstain errors -Nart Rawashdeh
    getAllAccounts();
    fetchBillersInformationPalpay();
    frmPalpayHome.show();
}