function frmPalpayBillInformation_txtTemporaryOTP_onDone(eventobject, changedtext) {
    return AS_TextField_a53aea7a6f3a4da49b26054dc6e48047(eventobject, changedtext);
}

function AS_TextField_a53aea7a6f3a4da49b26054dc6e48047(eventobject, changedtext) {
    if (!isEmpty(frmPalpayBillInformation.txtTemporaryOTP)) {
        frmPalpayBillInformation.flxUnderlineTemporaryOTP.skin = "sknFlxGreenLine";
    } else {
        frmPalpayBillInformation.flxUnderlineTemporaryOTP.skin = "sknFlxOrangeLine";
        checkBillOTP();
        goToNextScreenPal();
    }
}