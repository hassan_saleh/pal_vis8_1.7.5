function frmPalpayBillInformation_preShow(eventobject) {
    return AS_Form_a88f5bc5907543fa83dad899c3abcf4c(eventobject);
}

function AS_Form_a88f5bc5907543fa83dad899c3abcf4c(eventobject) {
    if (kony.store.getItem("langPrefObj") === "ar") {
        frmPalpayBillInformation.lblArrowBillerName.text = "j";
        frmPalpayBillInformation.lblArrowBillerCategory.text = "j"
        frmPalpayBillInformation.lblArrowServiceType.text = "j";
        frmPalpayBillInformation.lblArrowDenomination.text = "j";
        frmPalpayBillInformation.lblArrowPaymentMode.text = "j";
        frmPalpayBillInformation.lblArrowPaymentModeDetails.text = "j";
        frmPalpayBillInformation.lblArrowPaymentMode1BillDetails.text = "j";
    } else {
        frmPalpayBillInformation.lblArrowBillerName.text = "o";
        frmPalpayBillInformation.lblArrowBillerCategory.text = "o";
        frmPalpayBillInformation.lblArrowServiceType.text = "o";
        frmPalpayBillInformation.lblArrowDenomination.text = "o";
        frmPalpayBillInformation.lblArrowPaymentMode.text = "o";
        frmPalpayBillInformation.lblArrowPaymentModeDetails.text = "o";
        frmPalpayBillInformation.lblArrowPaymentMode1BillDetails.text = "o";
    }
    frmPalpayBillInformation.flxUnderlineBillerNumber.skin = "sknFlxGreyLine";
}