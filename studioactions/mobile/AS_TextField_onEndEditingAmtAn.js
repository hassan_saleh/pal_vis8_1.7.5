function AS_TextField_onEndEditingAmtAn(eventobject, changedtext) {
    return AS_TextField_h38731e15fc54580a10f911f0a3e069f(eventobject, changedtext);
}

function AS_TextField_h38731e15fc54580a10f911f0a3e069f(eventobject, changedtext) {
    var amount = frmNewTransferKA.txtBox.text;
    if (amount === null || amount === "" || amount == "." || amount === 0.00) {
        frmNewTransferKA.flxInvalidAmountField.setVisibility(true);
        frmNewTransferKA.lblLine3.skin = "sknFlxOrangeLine";
        frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
        frmNewTransferKA.lblNext.setEnabled(false);
    } else {
        frmNewTransferKA.txtBox.maxTextLength = 20;
        frmNewTransferKA.txtBox.text = formatAmountwithcomma(frmNewTransferKA.lblHiddenAmount.text, getDecimalNumForCurr(frmNewTransferKA.lblFromAccCurr.text));
    }
    getCommisionFee();
}