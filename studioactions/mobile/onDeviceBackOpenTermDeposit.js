function onDeviceBackOpenTermDeposit(eventobject) {
    return AS_Form_dcf426bf4eeb477e99ec28c5cc5177ec(eventobject);
}

function AS_Form_dcf426bf4eeb477e99ec28c5cc5177ec(eventobject) {
    if (frmOpenTermDeposit.flxConfirmDeposite.isVisible === true) {
        frmOpenTermDeposit.flxDepositSelection.setVisibility(true);
        frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);
        frmOpenTermDeposit.btnNextDeposite.setVisibility(true);
    } else {
        resetForm();
        frmAccountsLandingKA.show();
        frmOpenTermDeposit.destroy();
    }
}