function frmNewBillPal_tbxAmount_onEndEditing_And(eventobject, changedtext) {
    return AS_TextField_d099c205f30b41dc87ad26c1bebe9849(eventobject, changedtext);
}

function AS_TextField_d099c205f30b41dc87ad26c1bebe9849(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
        animateLabel("DOWN", "lblAmount", "");
        frmPalpayBillInformation.flxUnderlineAmount.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.flxUnderlineAmount.skin = "sknFlxGreenLine";
    }
    fieldsValidation();
}