function gotofrmAddNewPayeeKA(eventobject) {
    return AS_Button_1fe121a20c1d4d988b2b27103035291f(eventobject);
}

function AS_Button_1fe121a20c1d4d988b2b27103035291f(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddNewPayeeKA");
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navigationObject.setRequestOptions("form", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    listController.performAction("navigateTo", ["frmAddNewPayeeKA", navigationObject]);
}