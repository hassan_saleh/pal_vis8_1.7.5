function onTouchEndEditBillerClose(eventobject, x, y) {
    return AS_Label_d888c826e4a14fc180df1fcb18d1b18b(eventobject, x, y);
}

function AS_Label_d888c826e4a14fc180df1fcb18d1b18b(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.bills.editbillernameexit"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.cashWithdraw.cancel"));

    function onClickYesClose() {
        popupCommonAlertDimiss();
        frmManagePayeeKA.show();;
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}