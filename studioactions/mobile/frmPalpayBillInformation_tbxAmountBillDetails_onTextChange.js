function frmPalpayBillInformation_tbxAmountBillDetails_onTextChange(eventobject, changedtext) {
    return AS_TextField_ba959f1e52564bbb885b9fe80cab94ad(eventobject, changedtext);
}

function AS_TextField_ba959f1e52564bbb885b9fe80cab94ad(eventobject, changedtext) {
    gblVendorDetails.totalamount = frmPalpayBillInformation.tbxAmountBillDetails.text;
    if (!isEmpty(frmPalpayBillInformation.tbxAmountBillDetails)) {
        frmPalpayBillInformation.lblCurrencyAmt.setVisibility(true);
    } else {
        frmPalpayBillInformation.lblCurrencyAmt.setVisibility(false);
    }
}