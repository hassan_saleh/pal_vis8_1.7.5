function AS_FlexContainer_e0661f11a6a24800b1f01cedb0d83074(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseESP, onClickNoBackESP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseESP() {
        removeDatafrmTransfersformEPS();
        popupCommonAlertDimiss();
        frmPaymentDashboard.show();
    }

    function onClickNoBackESP() {
        popupCommonAlertDimiss();
    }
}