function afwanNavigatetoJomoPayConfirmation(eventobject) {
    return AS_Button_c69e583ce7414c5da676a6766197d949(eventobject);
}

function AS_Button_c69e583ce7414c5da676a6766197d949(eventobject) {
    frmJoMoPayConfirmation.lblTransferType.text = frmJoMoPay.lblTransferType.text;
    frmJoMoPayConfirmation.lblAccountType.text = frmJoMoPay.lblAccountType.text;
    frmJoMoPayConfirmation.lblType.text = frmJoMoPay.lblType.text;
    frmJoMoPayConfirmation.lblAmount.text = frmJoMoPay.txtFieldAmount.text + " JOD";
    frmJoMoPayConfirmation.lblPhoneNo.text = frmJoMoPay.txtAliasDetail.text;
    frmJoMoPayConfirmation.show();
}