function AS_onEndEditingEmailAndroid(eventobject, changedtext) {
    return AS_TextField_i4905b1ca56047e4ba00bda6aee542a5(eventobject, changedtext);
}

function AS_TextField_i4905b1ca56047e4ba00bda6aee542a5(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxEmail.text !== "") {
        frmAddExternalAccountKA.flxUnderlineEmail.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineEmail.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblEmail", frmAddExternalAccountKA.tbxEmail.text);
}