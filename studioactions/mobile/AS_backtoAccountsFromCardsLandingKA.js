function AS_backtoAccountsFromCardsLandingKA(eventobject) {
    return AS_FlexContainer_b816679eeaf947ea96c7d33caded6d6e(eventobject);
}

function AS_FlexContainer_b816679eeaf947ea96c7d33caded6d6e(eventobject) {
    if (frmCardsLandingKA.lblBack.text !== geti18Value("i18n.billsPay.Cards")) {
        if (kony.sdk.isNetworkAvailable()) {
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
            frmAccountsLandingKA.flxAccountsMain.left = "0%"
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            accountDashboardDataCall();
            kony.print("Nav accounts Dadhboard:::");
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        switch_CARDSLANDING_APPLYCARDS("flxScrollMain");
    }
}