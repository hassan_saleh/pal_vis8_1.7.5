function frmNewBillPal_tbxAmount_onDone(eventobject, changedtext) {
    return AS_TextField_j9adda1147b0481a91cec7ea35f0a015(eventobject, changedtext);
}

function AS_TextField_j9adda1147b0481a91cec7ea35f0a015(eventobject, changedtext) {
    //frmPalpayBillInformation.lblAmountNote.setVisibility(false);
    if (isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
        animateLabel("DOWN", "lblAmount", "");
        gblVendorDetails.totalamount = frmPalpayBillInformation.lblDenomination.text;
        //frmPalpayBillInformation.flxUnderlineAmount.skin = "sknFlxOrangeLine";
        frmPalpayBillInformation.lblAmountNote.setVisibility(false);
        frmPalpayBillInformation.flxConversionAmtBillDetailsPre.setVisibility(false);
    } else {
        //alert("Min: " + gblVendorDetails.minvalue + " Max: " + gblVendorDetails.maxvalue);
        if ((frmPalpayBillInformation.tbxAmount.text >= parseFloat(gblVendorDetails.minvalue)) && (frmPalpayBillInformation.tbxAmount.text <= parseFloat(gblVendorDetails.maxvalue))) {
            gblVendorDetails.totalamount = frmPalpayBillInformation.tbxAmount.text;
            frmPalpayBillInformation.flxUnderlineAmount.skin = "sknFlxGreenLine";
            if ((!isEmpty(frmPalpayBillInformation.tbxAmount.text)) && frmPalpayBillInformation.lblPaymentMode.text !== geti18Value('i18n.billsPay.Accounts')) frmPalpayBillInformation.lblAmountNote.setVisibility(true);
            fetchBillsCommissions();
        } else {
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.billpayment.amountvalidation"), popupCommonAlertDimiss, "");
            frmPalpayBillInformation.flxUnderlineAmount.skin = "sknFlxOrangeLine";
        }
    }
    fieldsValidation();
    fetchBillsCommissions();
}