function frmConfirmPayBillPal_lblCloseConfirm_onTouchEnd(eventobject, x, y) {
    return AS_Label_je234e97a5fd4379833364aeae8fbb36(eventobject, x, y);
}

function AS_Label_je234e97a5fd4379833364aeae8fbb36(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.bills.BillPaymentExit"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.cashWithdraw.cancel"));

    function onClickYesClose() {
        popupCommonAlertDimiss();
        //   if(EmptyList){
        //     frmPayBillHomePal.show();
        //   }else if(!EmptyList){
        frmPalpayHome.show();
        frmPalpayBillInformation.destroy();
        //   }
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}