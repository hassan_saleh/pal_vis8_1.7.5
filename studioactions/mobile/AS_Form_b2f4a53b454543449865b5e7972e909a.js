function AS_Form_b2f4a53b454543449865b5e7972e909a(eventobject) {
    frmApplyNewCardsKA.flxBack.accessibilityConfig = {
        "a11yLabel": "Back to main card screen",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.lblApplyCardTitle.accessibilityConfig = {
        "a11yLabel": "Cards Requests",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnNext.accessibilityConfig = {
        "a11yLabel": "Next button",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.lblAccountStaticText.accessibilityConfig = {
        "a11yLabel": "Card Type",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnPrimaryDebitCard.accessibilityConfig = {
        "a11yLabel": "Primary Debit Card",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnSuplementryDebitCard.accessibilityConfig = {
        "a11yLabel": "Supplementary Debit Card",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnSticker.accessibilityConfig = {
        "a11yLabel": "Sticker",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnReplaceDebitCards.accessibilityConfig = {
        "a11yLabel": "Replace Debit Card",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnChequeBook.accessibilityConfig = {
        "a11yLabel": "Cheque Book",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnPrimaryCreditCard.accessibilityConfig = {
        "a11yLabel": "Primary Credit Card",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnSuplementryCreditCard.accessibilityConfig = {
        "a11yLabel": "Supplementary Credit Card",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnPrimaryCreditCardReplace.accessibilityConfig = {
        "a11yLabel": "Replace Credit Card",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnUpgradeCreditCard.accessibilityConfig = {
        "a11yLabel": "Change Card Limit",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnWearables.accessibilityConfig = {
        "a11yLabel": "Wearable Credit Card",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnWebChargeCard.accessibilityConfig = {
        "a11yLabel": "Web Charge Card",
        "a11yValue": "",
        "a11yHint": ""
    }
    frmApplyNewCardsKA.btnReplacePrepaidCards.accessibilityConfig = {
        "a11yLabel": "Replace Prepaid Card",
        "a11yValue": "",
        "a11yHint": ""
    }
}