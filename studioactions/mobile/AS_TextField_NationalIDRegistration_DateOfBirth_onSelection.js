function AS_TextField_NationalIDRegistration_DateOfBirth_onSelection(eventobject, isValidDateSelected) {
    return AS_Calendar_e642fefaa6cc435f94d82fa4dbc74cdd(eventobject, isValidDateSelected);
}

function AS_Calendar_e642fefaa6cc435f94d82fa4dbc74cdd(eventobject, isValidDateSelected) {
    var field = frmRegisterUser.calDateOfBirth;
    animateLabel("UP", "lblDateOfBirthTitle", frmRegisterUser.calDateOfBirth.text);
    frmRegisterUser.lblDateOfBirth.text = field.dateComponents[2] + "-" + (parseInt(field.dateComponents[1]) < 10 ? "0" + field.dateComponents[1] : field.dateComponents[1]) + "-" + (parseInt(field.dateComponents[0]) < 10 ? "0" + field.dateComponents[0] : field.dateComponents[0])
    frmRegisterUser.lblDateOfBirth.setVisibility(true);
    isNextEnabledNat();
    frmRegisterUser.flxBorderDateOfBirth.skin = "skntextFieldDividerGreen";
}