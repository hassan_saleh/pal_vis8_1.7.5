function frmNewBillPal_confirmBtn_onClick(eventobject) {
    return AS_Button_f09d23cfb1a04b67b2304d04c0610f70(eventobject);
}

function AS_Button_f09d23cfb1a04b67b2304d04c0610f70(eventobject) {
    if (gblBillType === "postpaid") {
        confirmPaymentPalPay();
    } else if (gblBillType === "prepaid") {
        confirmPrepaidPaymentPalPay();
    }
}