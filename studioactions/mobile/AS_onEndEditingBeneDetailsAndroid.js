function AS_onEndEditingBeneDetailsAndroid(eventobject, changedtext) {
    return AS_TextField_cca2cfd66d634eefbb30b90c9384d455(eventobject, changedtext);
}

function AS_TextField_cca2cfd66d634eefbb30b90c9384d455(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxBeneRelation.text !== "") {
        frmAddExternalAccountKA.flxUnderlineBeneRelation.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineBeneRelation.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblBeneRelation", frmAddExternalAccountKA.tbxBeneRelation.text);
}