function frmNewBillPal_tbxAmountBillDetails_onEndEditing_IOS(eventobject, changedtext) {
    return AS_TextField_a7f96a50151b4716906db624115b988d(eventobject, changedtext);
}

function AS_TextField_a7f96a50151b4716906db624115b988d(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxAmountBillDetails.text)) {
        animateLabel("DOWN", "lblAmountBillDetails", "");
        frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxGreenLine";
    }
    //fieldsValidation();
}