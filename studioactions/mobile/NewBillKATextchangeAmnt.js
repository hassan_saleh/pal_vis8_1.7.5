function NewBillKATextchangeAmnt(eventobject, changedtext) {
    return AS_TextField_aedadc3617a44f70b3b6192768f3bea8(eventobject, changedtext);
}

function AS_TextField_aedadc3617a44f70b3b6192768f3bea8(eventobject, changedtext) {
    frmNewBillKA.tbxAmount.text = fixDecimal(frmNewBillKA.tbxAmount.text, "3");
    frmNewBillKA.lblHiddenAmount.text = frmNewBillKA.tbxAmount.text;
    nextFromNewBillScreen();
    changeUnderlineskin();
}