function frmNewBillPal_backBtnConfirm_onClick(eventobject) {
    return AS_FlexContainer_ie23cc141b7c4a0da7f4ff440d659a3b(eventobject);
}

function AS_FlexContainer_ie23cc141b7c4a0da7f4ff440d659a3b(eventobject) {
    if (gblFlow === "PayPalpayOtherFlow" && gblBillType === "prepaid") {
        frmPalpayBillInformation.mainInputContainer.setVisibility(true);
    } else if (gblFlow === "PayPalpayOtherFlow" && gblBillType === "postpaid") {
        frmPalpayBillInformation.mainBillDetailsContainer.setVisibility(true);
    }
    frmPalpayBillInformation.mainConfirmContainer.setVisibility(false);
}