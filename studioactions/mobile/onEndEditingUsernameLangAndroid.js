function onEndEditingUsernameLangAndroid(eventobject, changedtext) {
    return AS_TextField_e13fd592e51e4ebcbb00fe5c665c148e(eventobject, changedtext);
}

function AS_TextField_e13fd592e51e4ebcbb00fe5c665c148e(eventobject, changedtext) {
    if (frmLanguageChange.tbxusernameTextField.text !== null && frmLanguageChange.tbxusernameTextField.text !== "") {
        frmLanguageChange.borderBottom.skin = "skntextFieldDividerGreen";
    } else {
        frmLanguageChange.borderBottom.skin = "skntextFieldDivider";
    }
    animateLabel("DOWN", "lblUserName", frmLanguageChange.tbxusernameTextField.text);
    animateLabel("UP", "lblPassword", frmLanguageChange.passwordTextField.text);
}