function AS_FlexContainer_faaf63300c47427395ca01362075bfbd(eventobject) {
    if (frmEPS.lblNext.skin === "sknLblNextEnabled") {
        if (validate_SufficientBalance()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            SetupConfirmationScreen();
            //   GetAliasInfo();
            frmEPS.flxMain.setVisibility(false);
            frmEPS.flxConfirmEPS.setVisibility(true);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    } else {
        alert("fields required not fully added");
    }
}