function frmNewBillPal_tbxBillerNumber_onDone(eventobject, changedtext) {
    return AS_TextField_aaeb7a0bf564402dbca1d154c572db63(eventobject, changedtext);
}

function AS_TextField_aaeb7a0bf564402dbca1d154c572db63(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxBillerNumber.text)) {
        frmPalpayBillInformation.flxUnderlineBillerNumber.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.flxUnderlineBillerNumber.skin = "sknFlxGreenLine";
    }
    fieldsValidation();
}