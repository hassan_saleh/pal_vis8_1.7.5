function preShowfrmFilterTransaction(eventobject) {
    return AS_Form_bdefaf8bc28e451a9461234684a72847(eventobject);
}

function AS_Form_bdefaf8bc28e451a9461234684a72847(eventobject) {
    var month = (new Date().getMonth() + 1) + "";
    frmFilterTransaction.lblCurrentMonth.text = list_Month[month];
    frmFilterTransaction.lblYear.text = geti18Value("i18n.filtertransaction.year") + " " + new Date().getFullYear();
    var currForm = "";
    //#ifdef android
    currForm = kony.application.getCurrentForm().id;
    //#endif
    //#ifdef iphone
    currForm = kony.application.getPreviousForm().id;
    //#endif
    kony.print("previous form ::" + kony.application.getPreviousForm());
    if (currForm === "frmManageCardsKA") {
        frmFilterTransaction.lblCurrency.text = "JOD";
    } else {
        var value = kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)];
        frmFilterTransaction.lblCurrency.text = value.currencyCode;
    }
    frmFilterTransaction.flxAmountMinLine.skin = "skntextFieldDivider";
    frmFilterTransaction.flxAmountMaxLine.skin = "skntextFieldDivider";
    if (fromAccounts === true) {
        frmFilterTransaction.CopyLabel0ge4b0af66b9b44.isVisible = true;
        frmFilterTransaction.flxIncomming.isVisible = true;
        frmFilterTransaction.flxOutgoing.isVisible = true;
        frmFilterTransaction.flxCard.isVisible = false;
    } else if (fromCards == true) {
        frmFilterTransaction.CopyLabel0ge4b0af66b9b44.isVisible = false;
        frmFilterTransaction.flxIncomming.isVisible = true;
        frmFilterTransaction.flxOutgoing.isVisible = true;
        frmFilterTransaction.flxCard.isVisible = false;
    }
}