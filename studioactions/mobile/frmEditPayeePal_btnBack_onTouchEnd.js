function frmEditPayeePal_btnBack_onTouchEnd(eventobject, x, y) {
    return AS_Label_a626b32721584f8794db8e993b6d642f(eventobject, x, y);
}

function AS_Label_a626b32721584f8794db8e993b6d642f(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.bills.editbillernameexit"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.cashWithdraw.cancel"));

    function onClickYesClose() {
        popupCommonAlertDimiss();
        frmManagePayeeKA.show();;
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}