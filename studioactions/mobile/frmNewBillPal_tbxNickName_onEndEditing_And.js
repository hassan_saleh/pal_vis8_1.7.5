function frmNewBillPal_tbxNickName_onEndEditing_And(eventobject, changedtext) {
    return AS_TextField_e53b71ed11114c2c9e4a0315c06db786(eventobject, changedtext);
}

function AS_TextField_e53b71ed11114c2c9e4a0315c06db786(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxNickName.text)) {
        animateLabel("DOWN", "lblNickName", "");
        frmPalpayBillInformation.flxUnderlineNickName.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.flxUnderlineNickName.skin = "sknFlxGreenLine";
    }
}