function AS_TextField_iaa5d1ab89c14104afc9f6b5eee490b0(eventobject, changedtext) {
    onendeditingCaredless();
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = formatAmountwithcomma(frmCardlessTransaction.lblHiddenAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}