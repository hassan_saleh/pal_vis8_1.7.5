function AS_FlexContainer_d40983889904497cbc865c697e5fb6d5(eventobject) {
    if (kony.newCARD.applyCardsOption === true) {
        frmApplyNewCardsKA.show();
    } else {
        if (gblsubaccBranchList) {
            frmOrderCheckBook.show();
        } else if (gblrequestStatementBranch) {
            frmRequestStatementAccounts.show();
        } else if (gblRequestNewPinBranchList) {
            frmRequestPinCard.show();
        }
        /*else if(gblopenTermDepositeMI){// hassan open term deposite
          frmOpenTermDeposit.show();
         }*/
        else if (gblWorkflowType === "IPS") {
            frmEPS.show();
        } else {
            frmAddExternalAccountKA.show();
            kony.boj.addBeneNextStatus();
        }
    }
}