function onClickBackLinkedAccounts(eventobject) {
    return AS_FlexContainer_h47fec9a10404014a2ad7d3094e36ce1(eventobject);
}

function AS_FlexContainer_h47fec9a10404014a2ad7d3094e36ce1(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponser, onClickNoBack, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function onClickNoBack() {
        popupCommonAlertDimiss();
    }

    function userResponser() {
        frmManageCardsKA.show();
        frmCardLinkedAccounts.destroy();
        popupCommonAlertDimiss();
    }
}