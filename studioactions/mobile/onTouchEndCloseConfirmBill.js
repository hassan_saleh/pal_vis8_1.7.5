function onTouchEndCloseConfirmBill(eventobject, x, y) {
    return AS_Label_f330c2e5c5de446db41adde60324fa92(eventobject, x, y);
}

function AS_Label_f330c2e5c5de446db41adde60324fa92(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.bills.BillPaymentExit"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.cashWithdraw.cancel"));

    function onClickYesClose() {
        popupCommonAlertDimiss();
        frmPaymentDashboard.show();;
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}