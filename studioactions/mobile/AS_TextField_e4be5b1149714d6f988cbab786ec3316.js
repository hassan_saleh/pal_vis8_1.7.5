function AS_TextField_e4be5b1149714d6f988cbab786ec3316(eventobject, changedtext) {
    if (frmLanguageChange.v.text !== null && frmLanguageChange.tbxusernameTextField.text !== "") {
        frmLanguageChange.borderBottom.skin = "skntextFieldDividerGreen";
    } else {
        frmLanguageChange.borderBottom.skin = "skntextFieldDivider";
    }
    animateLabel("DOWN", "lblUserName", frmLanguageChange.tbxusernameTextField.text);
    animateLabel("UP", "lblPassword", frmLanguageChange.passwordTextField.text);
}