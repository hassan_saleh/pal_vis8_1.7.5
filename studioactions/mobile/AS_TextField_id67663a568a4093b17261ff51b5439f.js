function AS_TextField_id67663a568a4093b17261ff51b5439f(eventobject, changedtext) {
    try {
        if (frmLoginKA.tbxusernameTextField.text !== null && frmLoginKA.tbxusernameTextField.text !== "") {
            frmLoginKA.borderBottom.skin = "skntextFieldDividerGreen";
        } else {
            frmLoginKA.borderBottom.skin = "skntextFieldDivider";
        }
        animateLabel("DOWN", "lblUserName", frmLoginKA.tbxusernameTextField.text);
        animateLabel("UP", "lblPassword", frmLoginKA.passwordTextField.text);
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}