function AS_FlexContainer_cb725390932b40afa3cc68d2dc940393(eventobject, x, y) {
    if (gblCalculateFXLeft || gblCalculateFXRight || gblExchangeRatesFlag) {
        frmFxRate.show();
    } else if (gblTModule === "countryCode") {
        frmRegisterUser.show();
        frmCurrency.destroy();
    } else if (gblTModule === "loanPostpone") {
        frmLoanPostpone.show();
        frmCurrency.destroy();
        gblTModule = "";
    } else {
        frmNewTransferKA.show();
    }
    frmCurrency.tbxSearch.text = "";
}