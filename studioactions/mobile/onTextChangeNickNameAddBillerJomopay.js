function onTextChangeNickNameAddBillerJomopay(eventobject, changedtext) {
    return AS_TextField_c9d555dc789740f289489033bf621acb(eventobject, changedtext);
}

function AS_TextField_c9d555dc789740f289489033bf621acb(eventobject, changedtext) {
    if (new RegExp(/[^A-Za-z0-9\s]/g).test(frmJOMOPayAddBiller.txtNickName.text)) {
        frmJOMOPayAddBiller.flxBorderNickName.skin = "skntextFieldDividerOrange";
    } else {
        frmJOMOPayAddBiller.flxBorderNickName.skin = "skntextFieldDividerGreen";
    }
}