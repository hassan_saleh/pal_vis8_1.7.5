function AS_onEndEditingSwiftiOS(eventobject, changedtext) {
    return AS_TextField_a36f6668de914f8386432a44c6febb20(eventobject, changedtext);
}

function AS_TextField_a36f6668de914f8386432a44c6febb20(eventobject, changedtext) {
    if (frmAddExternalAccountKA.txtSwiftCodeKA.text !== "") {
        frmAddExternalAccountKA.flxUnderlineSwiftCode.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineSwiftCode.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblSwiftCode", frmAddExternalAccountKA.txtSwiftCodeKA.text);
}