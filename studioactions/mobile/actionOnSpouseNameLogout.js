function actionOnSpouseNameLogout(eventobject) {
    return AS_Button_b952976a2c18490ab0d851348f481cb2(eventobject);
}

function AS_Button_b952976a2c18490ab0d851348f481cb2(eventobject) {
    function SHOW_ALERT__c543188741b340f08b976b954dde5415_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT__c543188741b340f08b976b954dde5415_False() {}

    function SHOW_ALERT__c543188741b340f08b976b954dde5415_Callback(response) {
        if (response === true) {
            SHOW_ALERT__c543188741b340f08b976b954dde5415_True();
        } else {
            SHOW_ALERT__c543188741b340f08b976b954dde5415_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__c543188741b340f08b976b954dde5415_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}