function AS_TextField_NationalIDRegistration_ExpiryDate_onTouchStart(eventobject, x, y) {
    return AS_Calendar_a74ab14f90b54b749ec22e25fef8debc(eventobject, x, y);
}

function AS_Calendar_a74ab14f90b54b749ec22e25fef8debc(eventobject, x, y) {
    var caldate = new Date();
    frmRegisterUser.calIDExpiryDate.validStartDate = [caldate.getDate(), caldate.getMonth() + 1, caldate.getFullYear()];
    frmRegisterUser.calIDExpiryDate.validEndDate = [caldate.getDate(), caldate.getMonth() + 1, caldate.getFullYear() + 100];
}