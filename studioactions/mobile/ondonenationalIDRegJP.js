function ondonenationalIDRegJP(eventobject, changedtext) {
    return AS_TextField_cc0dca9b00994741ac95cec1b83895b3(eventobject, changedtext);
}

function AS_TextField_cc0dca9b00994741ac95cec1b83895b3(eventobject, changedtext) {
    animate_NEWCARDSOPTIOS("DOWN", "lblNationalID", frmJoMoPayRegistration.txtNationalID.text);
    animate_NEWCARDSOPTIOS("UP", "lblMobileNumberText", frmJoMoPayRegistration.txtPhoneNo.text);
    if (frmJoMoPayRegistration.txtNationalID.text !== "" && frmJoMoPayRegistration.txtNationalID.text !== null) {
        frmJoMoPayRegistration.flxBorderBenificiary.skin = "skntextFieldDividerGreen";
    } else {
        frmJoMoPayRegistration.flxBorderBenificiary.skin = "skntextFieldDividerJomoPay";
    }
}