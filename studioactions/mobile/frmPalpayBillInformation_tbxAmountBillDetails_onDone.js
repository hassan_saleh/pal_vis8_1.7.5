function frmPalpayBillInformation_tbxAmountBillDetails_onDone(eventobject, changedtext) {
    return AS_TextField_g8113d9dfd5447f78729725a368dc770(eventobject, changedtext);
}

function AS_TextField_g8113d9dfd5447f78729725a368dc770(eventobject, changedtext) {
    if ((frmPalpayBillInformation.tbxAmountBillDetails.text >= parseFloat(gblVendorDetails.mindueamount)) && (frmPalpayBillInformation.tbxAmountBillDetails.text <= parseFloat(gblVendorDetails.maxdueamount))) {
        frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxGreenLine";
        if ((!isEmpty(frmPalpayBillInformation.tbxAmountBillDetails.text)) && frmPalpayBillInformation.lblPaymentMode1BillDetails.text !== geti18Value('i18n.billsPay.Accounts')) frmPalpayBillInformation.lblCurrencyAmt.setVisibility(true);
        gblVendorDetails.totalamount = frmPalpayBillInformation.tbxAmountBillDetails.text;
        fetchBillsCommissions();
    } else {
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.billpayment.amountvalidation"), popupCommonAlertDimiss, "");
        frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxOrangeLine";
        frmPalpayBillInformation.lblCurrencyAmt.setVisibility(false);
    }
}