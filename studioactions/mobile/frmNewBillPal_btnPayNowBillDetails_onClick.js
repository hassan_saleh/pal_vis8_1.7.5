function frmNewBillPal_btnPayNowBillDetails_onClick(eventobject) {
    return AS_Button_a403f54c86924edf8bea9e6b03ae5532(eventobject);
}

function AS_Button_a403f54c86924edf8bea9e6b03ae5532(eventobject) {
    kony.print("postpaid accountDetails " + JSON.stringify(palpayAccountDetails));
    kony.print("postpaid gblVendorDetails " + JSON.stringify(gblVendorDetails));
    if (frmPalpayBillInformation.flxUnderlinePaymentMode1BillDetails.skin == "sknFlxGreenLine" && frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin == "sknFlxGreenLine") {
        if (parseFloat(palpayAccountDetails.availableBalance) <= parseFloat(gblVendorDetails.exchangedAmount)) {
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        } else if ((frmPalpayBillInformation.tbxAmountBillDetails.text >= parseFloat(gblVendorDetails.mindueamount)) && (frmPalpayBillInformation.tbxAmountBillDetails.text <= parseFloat(gblVendorDetails.maxdueamount))) {
            fetchBillsCommissions();
            //    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            //    if (flgConvertedAmount = true){
            goToNextScreenPal();
            //      flgConvertedAmount = false;
            //      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
            //    }
        } else {
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.billpayment.amountvalidation"), popupCommonAlertDimiss, "");
            frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxOrangeLine";
        }
    } else if (frmPalpayBillInformation.lblPaymentMode1BillDetails.text == geti18Value("i18n.billsPay.Accounts")) {
        frmPalpayBillInformation.flxUnderlinePaymentMode1BillDetails.skin = "sknFlxOrangeLine";
    } else if (frmPalpayBillInformation.lblAmountBillDetails.text == geti18Value("i18n.billsPay.Amount")) {
        frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxOrangeLine";
    }
}