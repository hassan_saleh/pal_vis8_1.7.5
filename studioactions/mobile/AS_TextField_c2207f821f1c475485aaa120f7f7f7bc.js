function AS_TextField_c2207f821f1c475485aaa120f7f7f7bc(eventobject, changedtext) {
    onendeditingCaredless();
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = formatAmountwithcomma(frmCardlessTransaction.lblHiddenAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}