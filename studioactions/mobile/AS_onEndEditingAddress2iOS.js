function AS_onEndEditingAddress2iOS(eventobject, changedtext) {
    return AS_TextField_b973966884fd4a23836cd027677e8543(eventobject, changedtext);
}

function AS_TextField_b973966884fd4a23836cd027677e8543(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxAddress2.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAddress2.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAddress2.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAddress2", frmAddExternalAccountKA.lblAddress2.text);
}