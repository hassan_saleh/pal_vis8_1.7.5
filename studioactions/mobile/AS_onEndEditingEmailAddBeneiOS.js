function AS_onEndEditingEmailAddBeneiOS(eventobject, changedtext) {
    return AS_TextField_i37a3abfc0b64025ba959bad7416aa78(eventobject, changedtext);
}

function AS_TextField_i37a3abfc0b64025ba959bad7416aa78(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxEmail.text !== "") {
        frmAddExternalAccountKA.flxUnderlineEmail.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineEmail.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblEmail", frmAddExternalAccountKA.tbxEmail.text);
}