function AS_onDoneNickNameAddBiller(eventobject, changedtext) {
    return AS_TextField_j79cec333d1a412fbe4debcf27b03175(eventobject, changedtext);
}

function AS_TextField_j79cec333d1a412fbe4debcf27b03175(eventobject, changedtext) {
    animateLabel("DOWN", "lblNickName", frmAddNewPayeeKA.tbxNickName.text);
    animateLabel("UP", "lblBillerNumber", frmAddNewPayeeKA.tbxBillerNumber.text);
    frmAddNewPayeeKA.tbxNickName.text = frmAddNewPayeeKA.tbxNickName.text.trim();
    if (frmAddNewPayeeKA.tbxNickName.text !== null && frmAddNewPayeeKA.tbxNickName.text !== "") {
        frmAddNewPayeeKA.flxUnderlineNickName.skin = "sknFlxGreenLine";
    } else {
        frmAddNewPayeeKA.flxUnderlineNickName.skin = "sknFlxOrangeLine";
    }
}