function AS_Button_dfbc534a3da744f0b3cf36df0321a067(eventobject) {
    if (frmCreditCardPayment.btnNext.skin === "jomopaynextEnabled") {
        amount = parseFloat(frmCreditCardPayment.txtFieldAmount.text);
        minamount = parseFloat(frmCreditCardPayment.lblPaymentValue.text);
        if (amount < minamount) {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.lessamountvalue"), popupCommonAlertDimiss, "");
        } else {
            frmCreditCardPayment.lblAccountNumber.text = frmCreditCardPayment.lblAccountType.text;
            if (frmCreditCardPayment.btnCard.text === "t") {
                frmCreditCardPayment.lblCard.text = frmCreditCardPayment.lblBOJCard.text;
                frmCreditCardPayment.lblCardValue.text = frmCreditCardPayment.lBoxCardList.selectedKeyValue[1];
                frmCreditCardPayment.lblDueDateValue.text = frmCreditCardPayment.lblDate.text;
                frmCreditCardPayment.flxDueDateConfirm.isVisible = true;
            } else {
                frmCreditCardPayment.lblCard.text = frmCreditCardPayment.lblOtherCard.text;
                frmCreditCardPayment.lblCardValue.text = frmCreditCardPayment.txtBoxOtherCard.text;
                frmCreditCardPayment.flxDueDateConfirm.isVisible = false;
            }
            frmCreditCardPayment.lblTUAmountValue.text = frmCreditCardPayment.lblTUAmount.text;
            frmCreditCardPayment.lblPVValue.text = frmCreditCardPayment.lblPaymentValue.text;
            frmCreditCardPayment.lblAmountValue.text = frmCreditCardPayment.txtFieldAmount.text;
            frmCreditCardPayment.flxBody.setVisibility(false);
            frmCreditCardPayment.flxConfirmPopUp.setVisibility(true);
        }
    }
}