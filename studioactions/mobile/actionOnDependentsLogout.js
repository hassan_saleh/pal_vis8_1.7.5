function actionOnDependentsLogout(eventobject) {
    return AS_Button_jfbef77fc0804ffabdbe5c269b9c0f78(eventobject);
}

function AS_Button_jfbef77fc0804ffabdbe5c269b9c0f78(eventobject) {
    function SHOW_ALERT_ide_onClick_hb1a711eccee4106b474dec12ec25331_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT_ide_onClick_hb1a711eccee4106b474dec12ec25331_False() {}

    function SHOW_ALERT_ide_onClick_hb1a711eccee4106b474dec12ec25331_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_hb1a711eccee4106b474dec12ec25331_True();
        } else {
            SHOW_ALERT_ide_onClick_hb1a711eccee4106b474dec12ec25331_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_hb1a711eccee4106b474dec12ec25331_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}