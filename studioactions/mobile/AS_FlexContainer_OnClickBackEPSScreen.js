function AS_FlexContainer_OnClickBackEPSScreen(eventobject) {
    return AS_FlexContainer_jc5faecf6023498da0ce1eadb2aa04d1(eventobject);
}

function AS_FlexContainer_jc5faecf6023498da0ce1eadb2aa04d1(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseESP, onClickNoBackESP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));

    function userResponseESP() {
        //   removeDatafrmTransfersformEPS();
        ResetFormIPSData();
        popupCommonAlertDimiss();
        //frmPaymentDashboard.show();
        frmIPSManageBene.show();
    }

    function onClickNoBackESP() {
        popupCommonAlertDimiss();
    }
}