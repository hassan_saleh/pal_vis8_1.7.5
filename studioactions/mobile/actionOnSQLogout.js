function actionOnSQLogout(eventobject) {
    return AS_Button_d3bdaa844ce34a4ea2d5371e0169cf69(eventobject);
}

function AS_Button_d3bdaa844ce34a4ea2d5371e0169cf69(eventobject) {
    function SHOW_ALERT__f572a3e7e9e84cb6bc4010f5c8099201_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT__f572a3e7e9e84cb6bc4010f5c8099201_False() {}

    function SHOW_ALERT__f572a3e7e9e84cb6bc4010f5c8099201_Callback(response) {
        if (response === true) {
            SHOW_ALERT__f572a3e7e9e84cb6bc4010f5c8099201_True();
        } else {
            SHOW_ALERT__f572a3e7e9e84cb6bc4010f5c8099201_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__f572a3e7e9e84cb6bc4010f5c8099201_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}