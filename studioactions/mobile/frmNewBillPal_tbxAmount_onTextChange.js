function frmNewBillPal_tbxAmount_onTextChange(eventobject, changedtext) {
    return AS_TextField_dff05d86739e4ccaba0a20f87b39b6e2(eventobject, changedtext);
}

function AS_TextField_dff05d86739e4ccaba0a20f87b39b6e2(eventobject, changedtext) {
    if (!isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
        frmPalpayBillInformation.lblAmountNote.setVisibility(true);
        gblVendorDetails.totalamount = frmPalpayBillInformation.tbxAmount.text;
    } else {
        frmPalpayBillInformation.lblAmountNote.setVisibility(false);
        frmPalpayBillInformation.flxConversionAmtBillDetailsPre.setVisibility(false);
        gblVendorDetails.totalamount = frmPalpayBillInformation.lblDenomination.text;
    }
    //fetchBillsCommissions();
}