function AS_Button_a38484c52b5440138e962d13e970a497(eventobject) {
    function SHOW_ALERT_ide_onClick_c989508599db4c149f521e4655fa95ce_True() {
        //closeAndroidNav();
        //deviceRegFrom = "logout";
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_c989508599db4c149f521e4655fa95ce_False() {}

    function SHOW_ALERT_ide_onClick_c989508599db4c149f521e4655fa95ce_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_c989508599db4c149f521e4655fa95ce_True();
        } else {
            SHOW_ALERT_ide_onClick_c989508599db4c149f521e4655fa95ce_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_c989508599db4c149f521e4655fa95ce_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}