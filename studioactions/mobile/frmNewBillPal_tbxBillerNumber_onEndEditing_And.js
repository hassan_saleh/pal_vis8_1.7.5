function frmNewBillPal_tbxBillerNumber_onEndEditing_And(eventobject, changedtext) {
    return AS_TextField_f646fd9ec94c4e74a796ca48d8e428ee(eventobject, changedtext);
}

function AS_TextField_f646fd9ec94c4e74a796ca48d8e428ee(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxBillerNumber.text)) {
        animateLabel("DOWN", "lblBillerNumber", "");
        frmPalpayBillInformation.flxUnderlineBillerNumber.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.flxUnderlineBillerNumber.skin = "sknFlxGreenLine";
    }
    fieldsValidation();
}