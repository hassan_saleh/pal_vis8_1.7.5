function AS_RegisterBack(eventobject, x, y) {
    return AS_FlexContainer_e46177579d944181acb70558283e40a7(eventobject, x, y);
}

function AS_FlexContainer_e46177579d944181acb70558283e40a7(eventobject, x, y) {
    if (gblFromModule == "RegisterUser") {
        frmRegisterUser.show();
    } else if (gblFromModule == "ChangeUsername") {
        frmSettingsKA.show();
    } else if (gblFromModule == "ChangePassword") {
        frmSettingsKA.show();
    } else {
        frmEnrolluserLandingKA.lblFlag.text = "F";
        if (gblReqField == "Username") {
            frmLoginKA.show();
        } else {
            frmRegisterUser.show();
        }
    }
    frmNewUserOnboardVerificationKA.destroy();
    //frmEnrolluserLandingKA.destroy();
}