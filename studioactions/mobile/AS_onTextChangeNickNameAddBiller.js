function AS_onTextChangeNickNameAddBiller(eventobject, changedtext) {
    return AS_TextField_g7b302fd4ef845969deaa12147f337f9(eventobject, changedtext);
}

function AS_TextField_g7b302fd4ef845969deaa12147f337f9(eventobject, changedtext) {
    kony.boj.storeDetailsAddBiller("NickName", frmAddNewPayeeKA.tbxNickName.text);
    if (frmAddNewPayeeKA.tbxNickName.text.match(/^[\s]/g) !== null) {
        frmAddNewPayeeKA.tbxNickName.text = frmAddNewPayeeKA.tbxNickName.text.replace(/\s/g, "");
    }
    if (kony.boj.isNextEnabledBiller()) frmAddNewPayeeKA.lblNext.skin = "sknLblNextEnabled";
    else frmAddNewPayeeKA.lblNext.skin = "sknLblNextDisabled";
}