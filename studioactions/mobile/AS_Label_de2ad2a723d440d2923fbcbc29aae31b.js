function AS_Label_de2ad2a723d440d2923fbcbc29aae31b(eventobject, x, y) {
    if (frmAddNewPayeeKA.lblNext.skin === "sknLblNextEnabled") {
        if (kony.boj.isNextEnabledBiller()) {
            if (kony.boj.isBillerRegistered()) customAlertPopup(geti18Value("i18n.common.AlreadyRegistered"), geti18Value("i18n.bills.BillerAlreadyRegistered"), popupCommonAlertDimiss, "");
            else kony.boj.onClickNextAddBiller();
        }
    }
}