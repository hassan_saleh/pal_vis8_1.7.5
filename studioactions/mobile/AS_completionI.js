function AS_completionI(eventobject) {
    return AS_Video_id036a0eb569434e976af47e55a02f8f(eventobject);
}

function AS_Video_id036a0eb569434e976af47e55a02f8f(eventobject) {
    var langFlag = kony.store.getItem("langFlagSettingObj");
    if (langFlag) {
        var langSelected = kony.store.getItem("langPrefObj");
        if (langSelected === "en") {
            onClickEnglish();
        } else {
            onClickArabic();
        }
        frmLanguageChange.flxMain.isVisible = false;
        frmLanguageChange.flxLogin.isVisible = true;
        kony.boj.maintainenceCHECK();
    } else {
        frmLanguageChange.show();
    }
}