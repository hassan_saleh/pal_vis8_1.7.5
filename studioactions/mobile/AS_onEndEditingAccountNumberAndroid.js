function AS_onEndEditingAccountNumberAndroid(eventobject, changedtext) {
    return AS_TextField_df3218145a6c426f87c9a330100d1b79(eventobject, changedtext);
}

function AS_TextField_df3218145a6c426f87c9a330100d1b79(eventobject, changedtext) {
    if (frmAddExternalAccountKA.externalAccountNumberTextField.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAccountNumber", frmAddExternalAccountKA.externalAccountNumberTextField.text);
}