function frmNewBillPal_tbxBillerNumber_onEndEditing_IOS(eventobject, changedtext) {
    return AS_TextField_i27c9770e0fa4cc0bc58950a7142390b(eventobject, changedtext);
}

function AS_TextField_i27c9770e0fa4cc0bc58950a7142390b(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxBillerNumber.text)) {
        animateLabel("DOWN", "lblBillerNumber", "");
        frmPalpayBillInformation.flxUnderlineBillerNumber.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.flxUnderlineBillerNumber.skin = "sknFlxGreenLine";
    }
    fieldsValidation();
}