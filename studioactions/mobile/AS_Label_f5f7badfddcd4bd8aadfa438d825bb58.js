function AS_Label_f5f7badfddcd4bd8aadfa438d825bb58(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.bills.editbillernameexit"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.cashWithdraw.cancel"));

    function onClickYesClose() {
        popupCommonAlertDimiss();
        frmManagePayeeKA.show();;
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}