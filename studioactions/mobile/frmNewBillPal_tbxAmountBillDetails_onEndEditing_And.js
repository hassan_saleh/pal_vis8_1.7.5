function frmNewBillPal_tbxAmountBillDetails_onEndEditing_And(eventobject, changedtext) {
    return AS_TextField_h88425be2db84d1f948bc2b9ea6aba3d(eventobject, changedtext);
}

function AS_TextField_h88425be2db84d1f948bc2b9ea6aba3d(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxAmountBillDetails.text)) {
        animateLabel("DOWN", "lblAmountBillDetails", "");
        frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.lblUnderlineAmountBillDetails.skin = "sknFlxGreenLine";
    }
    //fieldsValidation();
}