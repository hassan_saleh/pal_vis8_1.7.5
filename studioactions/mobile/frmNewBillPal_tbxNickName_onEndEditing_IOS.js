function frmNewBillPal_tbxNickName_onEndEditing_IOS(eventobject, changedtext) {
    return AS_TextField_dd994d1156af4b39a3cc05fb9c4f348e(eventobject, changedtext);
}

function AS_TextField_dd994d1156af4b39a3cc05fb9c4f348e(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxNickName.text)) {
        animateLabel("DOWN", "lblNickName", "");
        frmPalpayBillInformation.flxUnderlineNickName.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.flxUnderlineNickName.skin = "sknFlxGreenLine";
    }
}