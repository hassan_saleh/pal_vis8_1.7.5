function AS_Button_dd2b486072bb4dcaa537c47ac243955c(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddExternalAccountKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navObject.setRequestOptions("form", {
        "headers": {}
    });
    listController.performAction("navigateTo", ["frmAddExternalAccountKA", navObject]);
}