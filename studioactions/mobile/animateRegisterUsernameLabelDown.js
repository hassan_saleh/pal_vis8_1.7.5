function animateRegisterUsernameLabelDown(eventobject, changedtext) {
    return AS_TextField_e15c7c7bd578455698e45647494ae953(eventobject, changedtext);
}

function AS_TextField_e15c7c7bd578455698e45647494ae953(eventobject, changedtext) {
    animateLabel("DOWN", "lblUsername", frmEnrolluserLandingKA.tbxUsernameKA.text);
    animateLabel("UP", "lblPassword", frmEnrolluserLandingKA.tbxPasswordKA.text);
    frmEnrolluserLandingKA.flxUsernameValidation.isVisible = false;
    if (gblFromModule !== "RegisterUser" && gblFromModule !== "ChangePassword" && gblFromModule !== "ChangeUsername") {
        ShowLoadingScreen();
        callSave();
    }
}