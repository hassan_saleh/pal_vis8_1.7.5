function frmNewBillPal_tbxAmount_onEndEditing_IOS(eventobject, changedtext) {
    return AS_TextField_d3a4c169f50f46a3b0af0fc9d2fa2f01(eventobject, changedtext);
}

function AS_TextField_d3a4c169f50f46a3b0af0fc9d2fa2f01(eventobject, changedtext) {
    if (isEmpty(frmPalpayBillInformation.tbxAmount.text)) {
        animateLabel("DOWN", "lblAmount", "");
        frmPalpayBillInformation.flxUnderlineAmount.skin = "sknFlxOrangeLine";
    } else {
        frmPalpayBillInformation.flxUnderlineAmount.skin = "sknFlxGreenLine";
    }
    fieldsValidation();
}