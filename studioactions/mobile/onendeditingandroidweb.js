function onendeditingandroidweb(eventobject, changedtext) {
    return AS_TextField_c1cde23bc9cf42a38550b9128b8496c2(eventobject, changedtext);
}

function AS_TextField_c1cde23bc9cf42a38550b9128b8496c2(eventobject, changedtext) {
    frmWebCharge.flxBorderAmount.skin = "skntextFieldDividerGreen";
    if (frmWebCharge.lblCurrencyCode.text === "JOD") {
        if (frmWebCharge.txtFieldAmount.text !== "" && frmWebCharge.txtFieldAmount.text !== "0" && frmWebCharge.txtFieldAmount.text !== 0) {
            var amount = Number.parseFloat(frmWebCharge.txtFieldAmount.text).toFixed(3);
            frmWebCharge.txtFieldAmount.text = amount;
        }
    } else {
        if (frmWebCharge.txtFieldAmount.text !== "" && frmWebCharge.txtFieldAmount.text !== "0" && frmWebCharge.txtFieldAmount.text !== 0) {
            var amount = Number.parseFloat(frmWebCharge.txtFieldAmount.text).toFixed(2);
            frmWebCharge.txtFieldAmount.text = amount;
        }
    }
}