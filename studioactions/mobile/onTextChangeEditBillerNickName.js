function onTextChangeEditBillerNickName(eventobject, changedtext) {
    return AS_TextField_jd1d867de52a48858d5d062c92c79e48(eventobject, changedtext);
}

function AS_TextField_jd1d867de52a48858d5d062c92c79e48(eventobject, changedtext) {
    if (frmEditPayeeKA.txtBillerName.text.match(/^[\s]/g) !== null) {
        frmEditPayeeKA.txtBillerName.text = frmEditPayeeKA.txtBillerName.text.replace(/\s/g, "");
    }
    if (frmEditPayeeKA.txtBillerName.text === "") {
        frmEditPayeeKA.lblUnderline.skin = "sknFlxOrangeLine";
    } else {
        frmEditPayeeKA.lblUnderline.skin = "sknFlxGreenLine";
    }
}