function AS_TextField_dc3bd434f9e745f8b667249a24d29fff(eventobject, changedtext) {
    try {
        if (frmEstatementLandingKA.lblEmail1.top !== "15%") animateLabel("UP", "lblEmail1", frmEstatementLandingKA.txtEmail1.text);
        if (frmEstatementLandingKA.txtEmail1.text !== null && frmEstatementLandingKA.txtEmail1.text !== "" && isValidEmaill(frmEstatementLandingKA.txtEmail1.text)) {
            if (frmEstatementLandingKA.flxSwitchOnTouchLogin.isVisible && frmEstatementLandingKA.lblTermsandConditionsCheckBox.text === "p") frmEstatementLandingKA.lblNext.skin = "sknLblNextEnabled";
            else frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        } else {
            frmEstatementLandingKA.lblNext.skin = "sknLblNextDisabled";
        }
        frmEstatementLandingKA.lblInvalidEmail1.setVisibility(false);
    } catch (err) {
        kony.print("err:" + err);
    }
}