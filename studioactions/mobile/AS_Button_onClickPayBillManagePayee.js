function AS_Button_onClickPayBillManagePayee(eventobject) {
    return AS_Button_a8f6dc1e33344395ba0958a1729f9768(eventobject);
}

function AS_Button_a8f6dc1e33344395ba0958a1729f9768(eventobject) {
    gblFlow = "PayPalpayOtherFlow";
    payOtherBillsFlow();
    if (gblDefaultAccPayment !== "" && gblDefaultAccPayment !== null && gblDefaultAccPayment !== undefined) {
        getDefaultPaymentAccountDetails();
        kony.print("Debug Data :" + getDefaultPaymentAccountDetails());
        frmPalpayBillInformation.tbxPaymentMode.text = palpayAccountDetails.accountID;
        frmPalpayBillInformation.lblPaymentMode.text = palpayAccountDetails.accountName;
        frmPalpayBillInformation.tbxPaymentMode1BillDetails.text = palpayAccountDetails.accountID;
        frmPalpayBillInformation.lblPaymentMode1BillDetails.text = palpayAccountDetails.accountName;
        frmPalpayBillInformation.flxUnderlinePaymentMode.skin = "sknFlxGreenLine";
        frmPalpayBillInformation.flxUnderlinePaymentMode1BillDetails.skin = "sknFlxGreenLine";
    } else {
        palpayAccountDetails.accountID = "";
        palpayAccountDetails.accountcurrCode = "JOD";
        frmPalpayBillInformation.flxUnderlinePaymentMode.skin = "sknFlxGreyLine";
        frmPalpayBillInformation.flxUnderlinePaymentMode1BillDetails.skin = "sknFlxGreyLine";
        frmPalpayBillInformation.tbxPaymentMode1BillDetails.text = "";
        frmPalpayBillInformation.tbxPaymentMode1BillDetails.text = "";
        //frmPalpayBillInformation.lblPaymentMode1.text = geti18Value("i18n.billsPay.Accounts");
        //frmPalpayBillInformation.lblPaymentMode1BillDetails.text = geti18Value("i18n.billsPay.Accounts");
    }
    frmPalpayBillInformation.show();
}