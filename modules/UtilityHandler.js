kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.utilityHandler = function() {
    
};

kony.rb.utilityHandler.prototype.convertJSonToP2PRegistrationObject = function(internalP2PRegistrationJSON){
     kony.print("P2PRegistration Manager: Inside internalP2PRegistrationJSON");
     var internalP2PRegistrationRecord = new p2pRegistration(internalP2PRegistrationJSON.accountId,internalP2PRegistrationJSON.displayName,internalP2PRegistrationJSON.email,internalP2PRegistrationJSON.errmsg,internalP2PRegistrationJSON.isNpp,internalP2PRegistrationJSON.isZell,internalP2PRegistrationJSON.p2pRegId,internalP2PRegistrationJSON.phone,internalP2PRegistrationJSON.success); 
     return internalP2PRegistrationRecord;
};

kony.rb.utilityHandler.prototype.convertJSonToTransactionsObject = function(internalTransactionJSON) {

    kony.print("Transactions: Inside convertJSonToTransactionsObject");
    var internalTransactionRecord = new Transaction(internalTransactionJSON.accountID, internalTransactionJSON.accountNumber, internalTransactionJSON.amount,
        internalTransactionJSON.checkImage, internalTransactionJSON.checkImageBack, internalTransactionJSON.checkNumber, internalTransactionJSON.description, internalTransactionJSON.errmsg, internalTransactionJSON.ExternalAccountNumber,
        internalTransactionJSON.firstRecordNumber, internalTransactionJSON.frequencyEndDate, internalTransactionJSON.frequencyStartDate, internalTransactionJSON.frequencyType, internalTransactionJSON.fromAccountBalance, internalTransactionJSON.fromAccountName,
        internalTransactionJSON.fromAccountNumber, internalTransactionJSON.fromAccountType, internalTransactionJSON.fromCheckNumber, internalTransactionJSON.fromNickName, internalTransactionJSON.hasDepositImage, internalTransactionJSON.isScheduled,
        internalTransactionJSON.lastRecordNumber, internalTransactionJSON.numberOfRecurrences, internalTransactionJSON.payeeId, internalTransactionJSON.payeeNickName, internalTransactionJSON.payPersonEmail, internalTransactionJSON.payPersonName,
        internalTransactionJSON.payPersonPhone, internalTransactionJSON.personId, internalTransactionJSON.referenceId, internalTransactionJSON.scheduledDate, internalTransactionJSON.searchAmount, internalTransactionJSON.searchDateRange,
        internalTransactionJSON.searchDescription, internalTransactionJSON.searchEndDate, internalTransactionJSON.searchMaxAmount, internalTransactionJSON.searchMinAmount, internalTransactionJSON.searchStartDate, internalTransactionJSON.searchTransactionType,
        internalTransactionJSON.searchType, internalTransactionJSON.statusDescription, internalTransactionJSON.success, internalTransactionJSON.toAccountName, internalTransactionJSON.toAccountNumber, internalTransactionJSON.toAccountType,
        internalTransactionJSON.toCheckNumber, internalTransactionJSON.transactionComments, internalTransactionJSON.transactionDate, internalTransactionJSON.transactionId, internalTransactionJSON.transactionsNotes, internalTransactionJSON.transactionType, internalTransactionJSON.otp, internalTransactionJSON.validDate, internalTransactionJSON.cashlessMode, internalTransactionJSON.cashlessPersonName, internalTransactionJSON.cashWithdrawalTransactionStatus, internalTransactionJSON.cashlessSecurityCode, internalTransactionJSON.cashlessEmail, internalTransactionJSON.cashlessPhone, internalTransactionJSON.cashlessOTP, internalTransactionJSON.cashlessOTPValidDate);

    return internalTransactionRecord;
};

kony.rb.utilityHandler.prototype.convertJSonToAccountsObject = function(internalAccountJSON) {

    kony.print("Account Manager: Inside convertJSonToAccountsObject");
    var internalAccountRecord = new Account(internalAccountJSON.accountID, internalAccountJSON.accountName, internalAccountJSON.accountType,
        internalAccountJSON.availableBalance, internalAccountJSON.availablePoints, internalAccountJSON.bankName, internalAccountJSON.creditCardNumber,
        internalAccountJSON.currencyCode, internalAccountJSON.currentBalance, internalAccountJSON.deviceID, internalAccountJSON.dueDate, internalAccountJSON.errmsg,
        internalAccountJSON.interestRate, internalAccountJSON.isPFM, internalAccountJSON.lastStatementBalance, internalAccountJSON.maturityDate,
        internalAccountJSON.minimumDue, internalAccountJSON.nickName, internalAccountJSON.openingDate, internalAccountJSON.outstandingBalance,
        internalAccountJSON.paymentTerm, internalAccountJSON.principalValue, internalAccountJSON.success, internalAccountJSON.supportBillPay,internalAccountJSON.supportCardlessCash,
        internalAccountJSON.supportDeposit, internalAccountJSON.supportTransferFrom, internalAccountJSON.supportTransferTo,
        internalAccountJSON.transactionLimit, internalAccountJSON.transferLimit, internalAccountJSON.userName,internalAccountJSON.bsbNum);

    return internalAccountRecord;
};
kony.rb.utilityHandler.prototype.validateAmount = function(denomination, amount) {
    s = 0;
    if ((denomination.length === 0) || (amount === 0))
        return false;
    return this.validateAmountSub(denomination, amount, (denomination.length - 1));
};
kony.rb.utilityHandler.prototype.validateAmountSub = function(denomination, amount, index) {

    if (index < 0) {
        kony.print("Failed to meet demand");
        return false;
    }
    // If amount is a perfect multiple of the denomination we are good		
    if (amount % denomination[index] === 0) {
        s += " + " + (denomination[index] + "*" + Math.floor(amount / denomination[index]));
        return true;
    }
    // If amount is not a perfect multiple of the denomination
    if (amount % denomination[index] !== 0) {
        // If amount is greater than the denomination value
        if (amount > denomination[index]) {
            // There is enough quantity, so get remaining fractional amount
            s += " + " + (denomination[index] + "*" + Math.floor(amount / denomination[index]));
            //The () around amount/denomination[index] below is an absolute must 
            // given compiler optimisations 
            return this.validateAmountSub(denomination,
                amount - (denomination[index] * Math.floor((amount / denomination[index]))), (index - 1));
        }
        // Amount is less than denomination value, just move to a lesser denomination
        else {
            return this.validateAmountSub(denomination, amount, (index - 1));
        }
    }
    return false;
};

kony.rb.utilityHandler.prototype.calculateExpiryTimeInHrsandMins = function(timeStamp1) {
  var minutesArray=[];
  	var mins=0;
  	var expiryTime=parseInt(timeStamp1);
  	var hoursAndMins = expiryTime / 3600000;
  	var expiryTimeString=hoursAndMins.toString();
  	if(expiryTimeString.includes(".")){
  		minutesArray=expiryTimeString.split(".");
    }
  	if(hoursAndMins<0){
    	return -1;
  	}
  	if(minutesArray.length>0){
		mins=Math.floor(minutesArray[1].substring(0,2)*0.6);
    }
  	if(minutesArray[0]>=24 && parseInt(mins)>0){
    	return -1;
    }
  	if(mins<10){
    	mins = "0" + mins;
    }
  	if (minutesArray[0] < 10) {
        minutesArray[0] = "0" + minutesArray[0];
    }
  	
  	return (minutesArray[0]+"h:"+mins+"m");
};


kony.rb.utilityHandler.prototype.getAccountTypeforSkin=function(sknColor)
{
  switch (sknColor){
   case sknCheckingKA:return "Checking";
                  
   case sknSavingsKA:return "Savings";
                  
   case sknCreditKA:return "CreditCard";
                  
   case sknDepositKA:return "Deposit";
                  
   case sknMortgageKA:return "Mortgage";
                  
 } 
};

kony.rb.utilityHandler.prototype.convertJSonToAdsObject = function(internalAdsJSON){
     
     kony.print("Ads Manager: Inside internalAdsJSON");
     internalAdsJSON = new Ads(internalAdsJSON.id,internalAdsJSON.adType,internalAdsJSON.description,internalAdsJSON.imageURL,internalAdsJSON.action1,internalAdsJSON.action2,internalAdsJSON.title, internalAdsJSON.imageURL2, internalAdsJSON.actionType); 
     return internalAdsJSON;
};

kony.rb.utilityHandler.prototype.convertJSonToQrCodeObject = function(internalQrCodeJSON){
     kony.print("QrCode Manager: Inside internalQrCodeJSON");
     internalQrCodeJSON = new QrCode(internalQrCodeJSON.AtmId,internalQrCodeJSON.id,internalQrCodeJSON.timestamp); 
     return internalQrCodeJSON;
};

kony.rb.utilityHandler.prototype.convertJSonToProductsObject=function(internalProductsJSON)
{
  internalProductsJSON = new NewUserProducts(internalProductsJSON.features,internalProductsJSON.productId,internalProductsJSON.rates,internalProductsJSON.productDescription,internalProductsJSON.productType,internalProductsJSON.info,internalProductsJSON.termsAndConditions,internalProductsJSON.productImageURL,internalProductsJSON.productName);
  return internalProductsJSON;
};
kony.rb.utilityHandler.prototype.convertJSonToSeQObject=function(internalSeQJSON)
{
  internalSeQJSON = new NewUserSecurityQuestions(internalSeQJSON.question,internalSeQJSON.QuestionId);
  return internalSeQJSON;
};