kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.frmConfirmNPPRegistrationViewController = function(){
  
};
kony.rb.frmConfirmNPPRegistrationViewController.prototype.confirmNPPRegistrationPreshow = function(){
  var navManager = applicationManager.getNavManager();
  var RegData = navManager.getCustomInfo("frmConfirmNPPRegistration");
  var RegDetails=RegData.withdraw.p2pRecord;
  var accName=RegData.withdraw.accountName;
  var bsbNumber=RegData.withdraw.bsbNumber;
  frmConfirmNPPRegistration.lblDisplayNameValue.text=RegDetails.displayName;
  frmConfirmNPPRegistration.lblBSBNumberValue.text=bsbNumber;
   frmConfirmNPPRegistration.lblBankName.text=kony.retailBanking.globalData.globals.BankName;
 if(RegDetails.email)
    {
      frmConfirmNPPRegistration.lblPhnValue.text=RegDetails.email;
      frmConfirmNPPRegistration.phnLabel.text="Email:";
    }
  else
    {
      frmConfirmNPPRegistration.lblPhnValue.text=RegDetails.phone;
      frmConfirmNPPRegistration.phnLabel.text="Phone:";
    }
  frmConfirmNPPRegistration.lblAccountType.text=accName;
};
kony.rb.frmConfirmNPPRegistrationViewController.prototype.onClickOfConfirm = function(){
 var navManager = applicationManager.getNavManager();
  var RegData = navManager.getCustomInfo("frmConfirmNPPRegistration");
  var RegDetails=RegData.withdraw.p2pRecord;
   var nppPC=applicationManager.getNPPPresentationController();
   nppPC.createNPPRegistration(RegDetails); 
  
};

kony.rb.frmConfirmNPPRegistrationViewController.prototype.onClickOfCancel = function() {
    var navManager = applicationManager.getNavManager();

};
kony.rb.frmConfirmNPPRegistrationViewController.prototype.onClickOfEdit=function()
{
  var navManager = applicationManager.getNavManager();
  var RegData = navManager.getCustomInfo("frmConfirmNPPRegistration");
  navManager.goBack(RegData);
  
};