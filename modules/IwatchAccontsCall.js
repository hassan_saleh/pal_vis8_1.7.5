function iWatchAccountsCall(retType){
  kony.print("Iwatch :: Inside iWatchAccountsCall for :: "+ retType);
  if (kony.sdk.isNetworkAvailable()) {
    var scopeObj = this;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
    var headers = {};
    var dataObject = new kony.sdk.dto.DataObject("Accounts");
    var Language =  "eng";//kony.store.getItem("langPrefObj")==="en"?"eng":"ara";
    dataObject.addField("Language", Language);
    dataObject.addField("custId", iWatchcustid);

    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("Iwatch :: serviceOptions for iWatchAccountsCall : " + JSON.stringify(serviceOptions));
    objectService.customVerb("iWatchgetAccounts", serviceOptions,function(response) {
      iiWatchAccountsCallSuccessCallback(response,retType);
    },function(err) {
      iWatchAccountsCallErrorCallback(err,retType);
    });
  }else {
    return_watchrequest({"accounts": [],"isUserLoggedIn": true, "isNetworkAvailable": false,"isRetry": true});
  }
}

function iWatchAccountsCallErrorCallback(err,retType) {
  kony.print("Iwatch :: iWatchAccountsCallErrorCallback-->" + "An Error has occured, cant get accounts :: " + JSON.stringify(err));
  if ((!isEmpty(kony.retailBanking.globalData.accountsDashboardData))) {
    if(retType == "accounts"){
      return_watchrequest({"accounts": kony.retailBanking.globalData.accountsDashboardData.accountsData || [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
    }else if (retType == "loans"){
      return_watchrequest({"loans": kony.retailBanking.globalData.accountsDashboardData.loansData || [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
    }else if (retType == "deposits"){
      return_watchrequest({"deposits": kony.retailBanking.globalData.accountsDashboardData.dealsData || [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
    }
  }else{
    if(retType == "accounts"){
      return_watchrequest({"accounts":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
    }else if (retType == "loans"){
      return_watchrequest({"loans":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
    }else if (retType == "deposits"){
      return_watchrequest({"deposits":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
    }
  }
}

function iiWatchAccountsCallSuccessCallback(response,retType){
  try{
    kony.print("Iwatch :: iiWatchAccountsCallSuccessCallback response :: "+ JSON.stringify(response));
    var segAccountListData = response.Accounts;
    var accountsData = [],
        dealsData = [],
        loansData = [];
    var accountPartitionData = {};
    var sectionAccData = [],
        segDealsData = [],
        segLoansData = [];
    var jomoImg = "";
    if ((!isEmpty(segAccountListData)) && segAccountListData.length > 0) {
      for (var i in segAccountListData) {
        if ((segAccountListData[i]["accountType"] == "S" || segAccountListData[i]["accountType"] == "C" || segAccountListData[i]["accountType"] == "Y" || segAccountListData[i]["accountType"] == "CC" || segAccountListData[i]["accountType"] == "B") && segAccountListData[i]["productCode"] != "320") {
          segAccountListData[i]["availableBalance"] = formatamountwithCurrency(segAccountListData[i]["availableBalance"], segAccountListData[i]["currencyCode"]);
          segAccountListData[i]["currentBalance"] = formatamountwithCurrency(segAccountListData[i]["currentBalance"], segAccountListData[i]["currencyCode"]);
          segAccountListData[i]["outstandingBalance"] = formatamountwithCurrency(segAccountListData[i]["outstandingBalance"], segAccountListData[i]["currencyCode"]);

          if(isEmpty(segAccountListData[i]["AccNickName"])){
            // do nothing
          }else{
            segAccountListData[i]["accountName"] = segAccountListData[i]["AccNickName"];      
          }

          if(isEmpty(segAccountListData[i].AccHideShowFlag)){
            accountsData.push(segAccountListData[i]);
          }else{
            if(segAccountListData[i].AccHideShowFlag === "T" || segAccountListData[i].AccHideShowFlag === "Y")
              accountsData.push(segAccountListData[i]);
          }  

        }
        if (segAccountListData[i]["accountType"] == "T") {
          segAccountListData[i]["availableBalance"] = formatamountwithCurrency(segAccountListData[i]["availableBalance"], segAccountListData[i]["currencyCode"]);
          segAccountListData[i]["currentBalance"] = formatamountwithCurrency(segAccountListData[i]["currentBalance"], segAccountListData[i]["currencyCode"]);
          segAccountListData[i]["outstandingBalance"] = formatamountwithCurrency(segAccountListData[i]["outstandingBalance"], segAccountListData[i]["currencyCode"]);

          if(isEmpty(segAccountListData[i]["AccNickName"])){
            // do nothing
          }else{
            segAccountListData[i]["accountName"] = segAccountListData[i]["AccNickName"];      
          }

          dealsData.push(segAccountListData[i]);
        }
        if (segAccountListData[i]["accountType"] == "L") {
          segAccountListData[i]["availableBalance"] = formatamountwithCurrency(segAccountListData[i]["availableBalance"], segAccountListData[i]["currencyCode"]);
          segAccountListData[i]["currentBalance"] = formatamountwithCurrency(segAccountListData[i]["currentBalance"], segAccountListData[i]["currencyCode"]);
          segAccountListData[i]["outstandingBalance"] = formatamountwithCurrency(segAccountListData[i]["outstandingBalance"], segAccountListData[i]["currencyCode"]);

          if(isEmpty(segAccountListData[i]["AccNickName"])){
            // do nothing
          }else{
            segAccountListData[i]["accountName"] = segAccountListData[i]["AccNickName"];      
          }
          loansData.push(segAccountListData[i]);
        }
      }
      kony.print("Iwatch :: accountsData:::" + JSON.stringify(accountsData));
      kony.print("Iwatch :: dealsData:::" + JSON.stringify(dealsData));
      kony.print("Iwatch :: loansData:::" + JSON.stringify(loansData));
      accountPartitionData.accountsData = isEmpty(accountsData) ? {} : accountsData;
      accountPartitionData.dealsData = isEmpty(dealsData) ? {} : dealsData;
      accountPartitionData.loansData = isEmpty(loansData) ? {} : loansData;
      kony.retailBanking.globalData.accountsDashboardData = accountPartitionData;	
      kony.print("Iwatch :: iiWatchAccountsCallSuccessCallback Processsed data:: "+ JSON.stringify(kony.retailBanking.globalData.accountsDashboardData ));

      if(retType == "accounts"){
        return_watchrequest({"accounts": kony.retailBanking.globalData.accountsDashboardData.accountsData || []});
      }else if (retType == "loans"){
        return_watchrequest({"loans": kony.retailBanking.globalData.accountsDashboardData.loansData || []});
      }else if (retType == "deposits"){
        return_watchrequest({"deposits": kony.retailBanking.globalData.accountsDashboardData.dealsData || []});
      }
    }else{
      if ((!isEmpty(kony.retailBanking.globalData.accountsDashboardData))) {
        if(retType == "accounts"){
          return_watchrequest({"accounts": kony.retailBanking.globalData.accountsDashboardData.accountsData || [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
        }else if (retType == "loans"){
          return_watchrequest({"loans": kony.retailBanking.globalData.accountsDashboardData.loansData || [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
        }else if (retType == "deposits"){
          return_watchrequest({"deposits": kony.retailBanking.globalData.accountsDashboardData.dealsData || [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
        }
      }else{
        if(retType == "accounts"){
          return_watchrequest({"accounts":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
        }else if (retType == "loans"){
          return_watchrequest({"loans":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
        }else if (retType == "deposits"){
          return_watchrequest({"deposits":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
        }
      }
    }
  }catch(err){
    kony.print("Iwatch :: Inside catch of Error in iiWatchAccountsCallSuccessCallback" + err);
    if(retType == "accounts"){
      return_watchrequest({"accounts":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
    }else if (retType == "loans"){
      return_watchrequest({"loans":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
    }else if (retType == "deposits"){
      return_watchrequest({"deposits":  [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false });
    }
  }
}