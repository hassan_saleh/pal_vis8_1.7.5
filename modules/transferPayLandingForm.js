// Determines color associated with selected segment row
var selectedAccountColor;

// Assign data for  segments


// Set the accountType color of the selected row to the selectedAccountType variable
function recentTransactionsOnRowClick() {
   /*//////////////////////////////////////////////////////
  	 This variable will need to be set programatically depending
   	 on the backgroundColor of the accountType FlexContainer in each row
  */ /////////////////////////////////////////////////////////////////////////////
  selectedAccountColor = savingsColor;
}

function scheduledTransactionsOnRowClick() {
   /*//////////////////////////////////////////////////////
  	 This variable will need to be set programatically depending
   	 on the backgroundColor of the accountType FlexContainer in each row
  */ /////////////////////////////////////////////////////////////////////////////
  selectedAccountColor = checkingColor;
}


function addExternalAccountSegmentClick()
{

  var selectedIndex = frmAddExternalAccountKA.externalAccountTypeSegment.selectedIndex;
  var boolStatus = frmAddExternalAccountKA.externalAccountTypeSegment.selectedItems[0].imgicontick.isVisible;
  var name  = frmAddExternalAccountKA.externalAccountTypeSegment.selectedItems[0].lblNameKA;
  var data= {};
 
    data = {
      lblNameKA: name,
      imgicontick:{src:"check_blue.png",isVisible: true}
    };  
  loadExternalAccountTypes();
  frmAddExternalAccountKA.externalAccountTypeSegment.setDataAt(data,selectedIndex[1],selectedIndex[0]);
  //Hidden Label Data
  frmAddExternalAccountKA.lblAccountTypeKA.text = name;
}