var glbGetLabiba="F";
function GetLabibaFlag() {
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  if (kony.sdk.isNetworkAvailable()) {
    fetchApplicationPropertiesWithoutLoading();

    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online"
    };
    var headers = {};
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects", options);
    var dataObject = new kony.sdk.dto.DataObject("DeviceRegistration");

    var devID = getCurrentDeviceId();
    var langSelected = kony.store.getItem("langPrefObj");
    var Language = langSelected.toUpperCase();
    var Token = Dvfn(kony.store.getItem("soft_token"));

    dataObject.addField("Language", Language);
    dataObject.addField("deviceId", devID);
    dataObject.addField("Token", Token);

    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };

    if(!isEmpty(devID) && !isEmpty(Token)){
      kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
      objectService.customVerb("getLoginMethod", serviceOptions, function(response) {
        typeAltLoginSuccessCallback(response);
      }, typeAltMobileLoginErrorCallback);
    }else {
      kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    }
  } else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}


function GetLabibaFlagSuccessCallback(response) {
  try {
    var model = kony.os.deviceInfo().model;
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    glbGetLabiba=response.is_siri_enabled;
    kony.print("typeAltLoginSuccessCallback-->" + JSON.stringify(response));
    kony.print("status Code : " + response.statusCode);

    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();

  } catch (err) {
    kony.print("Error in set pin" + err);
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    exceptionLogCall("typeAltLoginSuccessCallback", "UI ERROR", "UI", err);

  }
}

function GetLabibaFlagErrorCallback(err) {
  kony.print("typeAltMobileLoginErrorCallback-->" + "An Error has occured, cant register" + JSON.stringify(err));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}