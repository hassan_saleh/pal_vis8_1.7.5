function confirmPaymentPalPay()
{
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var requestData = {
    "custID": custid,
    "lang": kony.store.getItem("langPrefObj"),
    "db_brch_code": palpayAccountDetails.branchNumber,
    "db_acc_num": palpayAccountDetails.accountID,
    "db_curr_code": palpayAccountDetails.accountcurrCode,
    "cr_curr_code": gblVendorDetails.vendorcurrencycode,
    "cr_vendor_curr": gblVendorDetails.vendorcurrencycode,
    "bill_trans_amt": gblVendorDetails.totalamount,
    "bill_comm_amt": 0,
    "bankComm": 0,
    "vendor_id": gblVendorDetails.vendorid,
    "vendor_b_name": gblVendorDetails.vendorname,
    "vendor_s_name": gblVendorDetails.vendorname,
    "fieldValue": gblVendorDetails.billingnumber,
    "billingCycle": gblVendorDetails.billingcycle,
    "payeeRef": gblVendorDetails.payeeref,
    "payment_type": gblVendorDetails.paymenttype,
    "payment_type_name": gblVendorDetails.paymenttypename,
    "category_id": gblVendorDetails.categoryid,
    "category_id_name": gblVendorDetails.categoryidname,
    "P_channel": "MOBILE",
    "p_user_id": "BOJMOB",
  };
  headers = {};
  kony.print("confirm request "+JSON.stringify(requestData));
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJPalpayPaymentConfirm");
  appMFConfiguration.invokeOperation("prPalpayPaymentConfirm", {},requestData,
                                     confirmPaymentPalPaySuccess,
                                     confirmPaymentPalPayFailure);
}

function confirmPaymentPalPaySuccess(res)
{
//   alert("custID: " + custid + " /// " + 
//         "lang: " + kony.store.getItem("langPrefObj") + " /// " + 
//         "db_brch_code: " + palpayAccountDetails.branchNumber + " /// " + 
//         "db_acc_num: " + palpayAccountDetails.accountID + " /// " + 
//         "db_curr_code: " + palpayAccountDetails.accountcurrCode + " /// " + 
//         "cr_curr_code: " + gblVendorDetails.vendorcurrencycode + " /// " + 
//         "cr_vendor_curr: " + gblVendorDetails.vendorcurrencycode + " /// " + 
//         "bill_trans_amt: " + gblVendorDetails.totalamount + " /// " + 
//         "bill_comm_amt: " + 0 + " /// " + 
//         "bankComm: " + 0 + " /// " + 
//         "vendor_id: " + gblVendorDetails.vendorid + " /// " + 
//         "vendor_b_name: " + gblVendorDetails.vendorname + " /// " + 
//         "vendor_s_name: " + gblVendorDetails.vendorname + " /// " + 
//         "fieldValue: " + gblVendorDetails.billingnumber + " /// " + 
//         "billingCycle: " + gblVendorDetails.billingcycle + " /// " + 
//         "payeeRef: " + gblVendorDetails.payeeref + " /// " + 
//         "payment_type: " + gblVendorDetails.paymenttype + " /// " + 
//         "payment_type_name: " + gblVendorDetails.paymenttypename + " /// " + 
//         "category_id: " + gblVendorDetails.categoryid + " /// " + 
//         "category_id_name: " + gblVendorDetails.categoryidname + " /// " + 
//         "P_channel: " + "MOBILE" + " /// " + 
//         "p_user_id: " + "BOJMOB");
  frmPalpayHome.destroy();
  gblFlow = "";
  gblBillType = "";
  gblVendorDetails = {};
  palpayAccountDetails = {};
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  kony.print("confirm payment " + JSON.stringify(res));
  if (res.returnCode === "00000")
  {
    var palpaySuccessReferenceNumber = res.refNo;
    kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), geti18Value("i18n.PalPay.SuccessMessage") + "\n" + geti18Value("i18n.PalPay.ReferenceID") + " " + palpaySuccessReferenceNumber,
                                   geti18Value("i18n.PalPay.goToPalpayMainScreen"), "palpay", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));
  }else
  {
    var Message = getErrorMessage(res.ErrorCode);
    kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message,
                                   geti18Value("i18n.PalPay.goToPalpayMainScreen"), "palpay",geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));
  }
}

function confirmPaymentPalPayFailure(err)
{
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  kony.print(err);
  frmPalpayHome.destroy();
  frmPalpayBillInformation.destroy();
  gblFlow = "";
  gblBillType = "";
  gblVendorDetails = {};
  palpayAccountDetails = {};
}

function confirmPrepaidPaymentPalPay()
{
  kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
  var requestData = {
    "p_customer_id": custid,
    "p_id_lang": kony.store.getItem("langPrefObj"),
    "P_DB_BRCH_CODE": palpayAccountDetails.branchNumber,
    "P_DB_ACC_NUM": palpayAccountDetails.accountID,
    "P_DB_CURR_CODE": palpayAccountDetails.accountcurrCode,
    "P_CR_CURR_CODE": gblVendorDetails.vendorcurrencycode,
    "P_CR_VENDOR_CURR": gblVendorDetails.vendorcurrencycode,
    "P_BILL_TRANS_AMNT": gblVendorDetails.totalamount,
    "P_BILL_COMM_AMNT": 0,
    "P_BANKCOMM": 0,
    "P_VENDOR_ID": gblVendorDetails.vendorid,
    "P_VENDOR_B_NAME": gblVendorDetails.vendorname,
    "P_VENDOR_S_NAME": gblVendorDetails.vendorname,
    "P_FIELDVALUE": gblVendorDetails.billingnumber,
    "P_BILLINGCYCLE": null,
    "P_PAYEEREF": null,
    "P_PAYMENT_TYPE": gblVendorDetails.paymenttype,
    "P_PAYMENT_TYPE_NAME": gblVendorDetails.paymenttypename,
    "P_CATEGROY_ID": gblVendorDetails.categoryid,
    "P_CATEGROY_ID_NAME": gblVendorDetails.categoryidname,
    "p_channel": "MOBILE",
    "p_user_id": "BOJMOB",
  };
  headers = {};
  kony.print("confirm request "+JSON.stringify(requestData));
  var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJPalpayPpPaymentConfirm");
  appMFConfiguration.invokeOperation("prPalpayPpPaymentConfirm", {},requestData,
                                     confirmPrepaidPaymentPalPaySuccess,
                                     confirmPrepaidPaymentPalPayFailure);
}

function confirmPrepaidPaymentPalPaySuccess(res)
{
  //   alert("custID: " + custid + " /// " + 
  //         "p_customer_id: "+ custid + " /// " + 
  //         "p_id_lang: "+ kony.store.getItem("langPrefObj") + " /// " + 
  //         "P_DB_BRCH_CODE: "+ palpayAccountDetails.branchNumber + " /// " + 
  //         "P_DB_ACC_NUM: "+ palpayAccountDetails.accountID + " /// " + 
  //         "P_DB_CURR_CODE: "+ palpayAccountDetails.accountcurrCode + " /// " + 
  //         "P_CR_CURR_CODE: "+ gblVendorDetails.vendorcurrencycode + " /// " + 
  //         "P_CR_VENDOR_CURR: "+ gblVendorDetails.vendorcurrencycode + " /// " + 
  //         "P_BILL_TRANS_AMNT: "+ gblVendorDetails.totalamount + " /// " + 
  //         "P_BILL_COMM_AMNT: "+ 0 + " /// " + 
  //         "P_BANKCOMM: "+ 0 + " /// " + 
  //         "P_VENDOR_ID: "+ gblVendorDetails.vendorid + " /// " + 
  //         "P_VENDOR_B_NAME: "+ gblVendorDetails.vendorname + " /// " + 
  //         "P_VENDOR_S_NAME: "+ gblVendorDetails.vendorname + " /// " + 
  //         "P_FIELDVALUE: "+ gblVendorDetails.billingnumber + " /// " + 
  //         "P_BILLINGCYCLE: "+ gblVendorDetails.billingcycle + " /// " + 
  //         "P_PAYEEREF: "+ gblVendorDetails.payeeref + " /// " + 
  //         "P_PAYMENT_TYPE: "+ gblVendorDetails.paymenttype + " /// " + 
  //         "P_PAYMENT_TYPE_NAME: "+ gblVendorDetails.paymenttypename + " /// " + 
  //         "P_CATEGROY_ID: "+ gblVendorDetails.categoryid + " /// " + 
  //         "P_CATEGROY_ID_NAME: "+ gblVendorDetails.categoryidname + " /// " + 
  //         "p_channel: "+ "MOBILE" + " /// " + 
  //         "p_user_id: "+ "BOJMOB");
  frmPalpayHome.destroy();
  gblFlow = "";
  gblBillType = "";
  gblVendorDetails = {};
  palpayAccountDetails = {};
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  kony.print("confirm payment " + JSON.stringify(res));
  if (res.return_code === "00000")
  {
    var palpaySuccessReferenceNumber = res.ref_no;
    kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.Bene.Success"), geti18Value("i18n.PalPay.SuccessMessage") + "\n" + geti18Value("i18n.PalPay.ReferenceID") + " " + palpaySuccessReferenceNumber,
                                   geti18Value("i18n.PalPay.goToPalpayMainScreen"), "palpay", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));
  }else
  {
    var Message = getErrorMessage(res.ErrorCode);
    kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message,
                                   geti18Value("i18n.PalPay.goToPalpayMainScreen"), "palpay",geti18Value("i18n.Bene.GotoAccDash"), geti18nkey ("i18n.common.AccountDashboard"));
  }
}

function confirmPrepaidPaymentPalPayFailure(err)
{
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  kony.print(err);
  frmPalpayHome.destroy();
  frmPalpayBillInformation.destroy();
  gblFlow = "";
  gblBillType = "";
  gblVendorDetails = {};
  palpayAccountDetails = {};
}
