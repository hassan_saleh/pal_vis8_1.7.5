function addDatatoSegQuickBalance(fl){
  var cardsData = [
      {
        "AccountType" : "Salary Account",
        "AccountNumber" : "1234567890"
      },
      {
        "AccountType" : "Credit card Account",
        "AccountNumber" : "1890234567"
      },
      {
        "AccountType" : "Savings Account",
        "AccountNumber" : "1234905678"
      },
      {
        "AccountType" : "Term deposit Account",
        "AccountNumber" : "1289034467"
      },
      {
        "AccountType" : "Current Account",
        "AccountNumber" : "3458901267"
      }
    ];
  if(fl===1)
  {
    for(var i in cardsData){
		var x = cardsData[i].AccountNumber;
      	x = "***"+((parseInt(x)%1000).toString());
      	cardsData[i].hiddenAccountNumber = x;
      	//cardsData[i].template = flxSegQuickBalance;
    }
  	//alert("CardsData "+ JSON.stringify(cardsData));
    frmAuthorizationAlternatives.segAccounts.widgetDataMap = {lblAccount : "AccountType", lblAccNumber : "hiddenAccountNumber" };
    frmAuthorizationAlternatives.segAccounts.setData(cardsData);
  	//alert("SegmentedUI data ::"+frmAuthorizationAlternatives.segAccounts.data); 
  }
  else{
    cardsData = [{}];
    frmAuthorizationAlternatives.segAccounts.setData(cardsData);
  }
}