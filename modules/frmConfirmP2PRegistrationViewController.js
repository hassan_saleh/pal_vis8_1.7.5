kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.frmConfirmP2PRegistrationViewController = function(){
  
};
kony.rb.frmConfirmP2PRegistrationViewController.prototype.confirmP2PRegistrationPreshow = function(){
  var navManager = applicationManager.getNavManager();
  var RegData = navManager.getCustomInfo("frmConfirmP2PRegistrationKA");
  var RegDetails=RegData.withdraw.p2pRecord;
  var accName=RegData.withdraw.accountName;
  var bsbNumber=RegData.withdraw.bsbNumber;
  frmConfirmP2PRegistrationKA.lblBSBNumberValue.text=bsbNumber;
   frmConfirmP2PRegistrationKA.lblBankName.text=kony.retailBanking.globalData.globals.BankName;
 if(RegDetails.email)
    {
      frmConfirmP2PRegistrationKA.lblPhnValue.text=RegDetails.email;
      frmConfirmP2PRegistrationKA.phnLabel.text="Email:";
    }
  else
    {
      frmConfirmP2PRegistrationKA.lblPhnValue.text=RegDetails.phone;
      frmConfirmP2PRegistrationKA.phnLabel.text="Phone:";
    }
  frmConfirmP2PRegistrationKA.lblAccountType.text=accName;
};
kony.rb.frmConfirmP2PRegistrationViewController.prototype.onClickOfConfirm = function(){
 var navManager = applicationManager.getNavManager();
  var RegData = navManager.getCustomInfo("frmConfirmP2PRegistrationKA");
  var RegDetails=RegData.withdraw.p2pRecord;
   var nppPC=applicationManager.getP2PPresentationController();
   nppPC.createP2PRegistration(RegDetails); 
  
};

kony.rb.frmConfirmP2PRegistrationViewController.prototype.onClickOfCancel = function() {
    var navManager = applicationManager.getNavManager();
    
};
kony.rb.frmConfirmP2PRegistrationViewController.prototype.onClickOfEdit=function()
{
  var navManager = applicationManager.getNavManager();
  var RegData = navManager.getCustomInfo("frmConfirmP2PRegistrationKA");
  navManager.goBack(RegData);
  
};