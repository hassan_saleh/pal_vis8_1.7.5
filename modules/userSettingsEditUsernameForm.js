function onClickSettingsEditUserName(){
   var INSTANCE =  kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
   var controller = INSTANCE.getFormController("frmUserSettingsEditUsernameKA");
   var formModel=controller.getFormModel();
   var newUserName=formModel.getViewAttributeByProperty("txtNewUserNameKA","text");
   var currentUserName=formModel.getViewAttributeByProperty("lblCurrentUserNameKA","text");
    if(newUserName === currentUserName || !kony.retailBanking.util.validation.isValidUsername(newUserName)){
      frmUserSettingsEditUsernameKA.CopylblQuestion09d281a56bb7e4c.skin="sknD0021BLatoSemiBold";
      frmUserSettingsEditUsernameKA.flxSep.skin="sknFlxBGe2e2e2B1pxD0021B";
      frmUserSettingsEditUsernameKA.lblunameErr.setVisibility(false);
      if(frmUserSettingsEditUsernameKA.txtNewUserNameKA.text !== null && frmUserSettingsEditUsernameKA.txtNewUserNameKA.text !== "")
      frmUserSettingsEditUsernameKA.lblunameErr.setVisibility(true);
    }
  	else
  	{
       frmUserSettingsEditUsernameKA.CopylblQuestion09d281a56bb7e4c.skin="sknsectionHeaderLabel";
       frmUserSettingsEditUsernameKA.flxSep.skin="slFbox";
       frmUserSettingsEditUsernameKA.lblunameErr.setVisibility(false);
  	}
	if(newUserName !== currentUserName && kony.retailBanking.util.validation.isValidUsername(newUserName)){
        var navObject = new kony.sdk.mvvm.NavigationObject();
      	navObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
        navObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
		controller.performAction("saveData",[navObject]);
    }
}

function onClickSettingsUsername(){
  		var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
		var listcontroller = INSTANCE.getFormController("frmUserSettingsEditUsernameKA");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
        listcontroller.performAction("loadDataAndShowForm",[navObject]);
}

function onClickSettingsEditProfile(){
    var INSTANCE =  kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  	var controller = INSTANCE.getFormController("frmUserSettingsEditPersonalDetailsKA");
  	var formModel=controller.getFormModel();
  	var userPhone=formModel.getViewAttributeByProperty("answerField1","text");
    var userSPhone=formModel.getViewAttributeByProperty("answerField2","text");
    var userEmail=formModel.getViewAttributeByProperty("answerField3","text");
    var userSEmail=formModel.getViewAttributeByProperty("answerField4","text");
  	var isValid = true;
    var validPhone=kony.retailBanking.util.validation.isValidPhoneNumber(userPhone);
  	if(userPhone === "" || userPhone === null||!validPhone){
      frmUserSettingsEditPersonalDetailsKA.lblQuestion.skin = "sknD0021BLatoSemiBold";
      frmUserSettingsEditPersonalDetailsKA.lblLine1KA.skin = "sknlblD0021BSep"
      isValid = false;
    } else {
      frmUserSettingsEditPersonalDetailsKA.lblQuestion.skin = "skn";
      frmUserSettingsEditPersonalDetailsKA.lblLine1KA.skin = "sknLineEDEDEDKA"
    }
  
    if(userEmail === "" || userEmail === null){
      	frmUserSettingsEditPersonalDetailsKA.CopylblQuestion0dcf1a03c2d5e44.skin = "sknD0021BLatoSemiBold";
        frmUserSettingsEditPersonalDetailsKA.CopylblLine05f69e54d4f914e.skin = "sknlblD0021BSep";
      	isValid = false;
    } else {
      frmUserSettingsEditPersonalDetailsKA.CopylblQuestion0dcf1a03c2d5e44.skin = "skn";
      frmUserSettingsEditPersonalDetailsKA.CopylblLine05f69e54d4f914e.skin = "sknLineEDEDEDKA";
    }
  
 	if(userEmail !== "" && !kony.retailBanking.util.validation.isValidEmail(userEmail)){
      	frmUserSettingsEditPersonalDetailsKA.lblInvalidPrimary.setVisibility(true);
      	isValid = false;
    } else {
      	frmUserSettingsEditPersonalDetailsKA.lblInvalidPrimary.setVisibility(false);
    }
  	var validSPhone=kony.retailBanking.util.validation.isValidPhoneNumber(userSPhone);
  	if(userSPhone === "" || userSPhone === null||!validSPhone) {
      frmUserSettingsEditPersonalDetailsKA.CopylblQuestion0bdaad05e0ff44d.skin = "sknD0021BLatoSemiBold"
      frmUserSettingsEditPersonalDetailsKA.lblLine2KA.skin = "sknlblD0021BSep";
      isValid = false;
    } else {
      frmUserSettingsEditPersonalDetailsKA.CopylblQuestion0bdaad05e0ff44d.skin = "skn"
      frmUserSettingsEditPersonalDetailsKA.lblLine2KA.skin = "sknLineEDEDEDKA";
    }
  	
  	if(userSEmail === "" || userSEmail === null) {
      frmUserSettingsEditPersonalDetailsKA.CopylblQuestion0b52a1bc3c19c42.skin = "sknD0021BLatoSemiBold";
      frmUserSettingsEditPersonalDetailsKA.CopylblLine00051147595274b.skin = "sknlblD0021BSep"
      isValid = false;
    } else {
     frmUserSettingsEditPersonalDetailsKA.CopylblQuestion0b52a1bc3c19c42.skin = "skn";
      frmUserSettingsEditPersonalDetailsKA.CopylblLine00051147595274b.skin = "sknLineEDEDEDKA" 
    }
  
  	if(userSEmail !== "" && !kony.retailBanking.util.validation.isValidEmail(userSEmail)){
      	frmUserSettingsEditPersonalDetailsKA.lblInvalidSecondary.setVisibility(true);
      	isValid = false;
    } else {
      	frmUserSettingsEditPersonalDetailsKA.lblInvalidSecondary.setVisibility(false);
    }
  	
  	if(isValid) {
        var navObject = new kony.sdk.mvvm.NavigationObject();
      	navObject.setDataModel(null, kony.sdk.mvvm.OperationType.FILTER_BY_PRIMARY_KEY, "form");
        navObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
		controller.performAction("saveData",[navObject]);
    }
}

function onClickSettingsProfile(){
  		var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
		var listcontroller = INSTANCE.getFormController("frmUserSettingsEditPersonalDetailsKA");
        var navObject = new kony.sdk.mvvm.NavigationObject();
        navObject.setRequestOptions("form",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
  		listcontroller.performAction("loadDataAndShowForm",[navObject]);

}

//Change Profile Security Questions functionality

function changeProfileQFetch(){
//   var options = {"access":"online"};
//   objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
//   var dataObject = new kony.sdk.dto.DataObject("UserSecurityQuestions");
//   var usrName = kony.retailBanking.globalData.globals.userObj.userName;
//   kony.retailBanking.globalData.globals.usrName = usrName;
//   var queryParams = {"userName":usrName};
//   var serviceOptions = {"dataObject":dataObject,"queryParams":queryParams};
//   objectService.fetch(serviceOptions, profileSuccess,profileError);
}

function profileSuccess(res){
  kony.print("Removed");
}
function profileError(res){
  alert(res.errmsg);
}

function changeProfile(){
//   var options = {"access":"online"};
//   objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);

  
//   var usrName = kony.retailBanking.globalData.globals.usrName;
//   var headers = {"userName":usrName};
//   var dataObject = new kony.sdk.dto.DataObject("UserSecurityQuestions");
//   dataObject.setRecord(record);
//   var serviceOptions = {"dataObject":dataObject, "headers":headers};
//   objectService.partialUpdate(serviceOptions, myProfileSuccess,customErrorCallback);
  kony.print("Removed");
 }
function myProfileSuccess(res){
  kony.print("Removed");
}
function myProfileError(res){
   kony.print("Removed");
}