/****
Purpose: Destroy the forms except login

****/
function destroyForms() {
    kony.print("destroyForms:start");
    var formData = [
        frmTermsAndConditionsKA,
        //frmJoMopay,
        frmJomoPayAccountList,
        frmJoMoPayConfirmation,
        frmJoMoPayLanding,
        frmJomopayContacts,
      	frmJoMoPayRegistration,
        frmAccountDetailKA,
        frmFilterTransaction,
        frmSettingsKA,
        frmTransactionDetails,
        frmConfirmTransferKA,
        frmCurrency,
        frmNewTransferKA,
        //frmNewUserOnboardVerificationKA,
        frmAccountsLandingKA,
        frmAccountInfoKA,
        frmAuthorizationAlternatives,
        frmEditAccountSettings,
        frmManageCardsKA,
        frmCardsLandingKA,
        frmMyAccountSettingsKA,
        //frmWebCharge,
        //frmCreditCardPayment,
        frmCreditCardNumber,
        frmPaymentDashboard,
        frmAddExternalAccountHome,
        frmShowAllBeneficiary,
        frmAddExternalAccountKA,
        //frmCongratulations,
        frmPayBillHome,
        frmManagePayeeKA,
        frmAddNewPayeeKA,
        frmPayeeDetailsKA,
        //frmNewBillKA,
        frmNewBillDetails,
        frmConfirmPayBill,
        frmCurrency,
        frmAccountDetailsScreen,
        frmCancelCardKA,
      	frmEstatementLandingKA,
      	frmEstatementConfirmKA
        //frmStopCardKA,
        //frmSettingsKA
    ]

    // Destroying form will happen on Logout, Session Timeout and  Invalid session code
    //Checking form id destroying the current form will result in showing blank screen for some time untill next form.show happen
    var currentForm = kony.application.getCurrentForm();
    for (var i in formData) {
        if (formData[i].id != undefined && formData[i].id != null && currentForm != null && currentForm.id != null && formData[i].id != currentForm.id) {
            formData[i].destroy();
        }
    }
    kony.print("destroyForms:end");
}



// frmSplash
// frmLanguageChange
// frmLoginKA
// frmTermsAndConditionsKA
// frmJoMopay
// frmJomoPayAccountList
// frmJoMoPayConfirmation
// frmJoMoPayLanding
// frmJomopayContacts
// frmAccountDetailKA
// frmFilterTransaction
// frmDeviceRegistration
// frmSettingsKA
// frmTransactionDetails
// frmConfirmTransferKA
// frmCurrency
// frmEnrolluserLandingKA
// frmNewTransferKA
// frmNewUserOnboardVerificationKA
// frmRegisterUser
// frmAccountsLandingKA
// frmAccountInfoKA
// frmAuthorizationAlternatives
// frmEditAccountSettings
// frmManageCardsKA
// frmCardsLandingKA
// frmMyAccountSettingsKA
// frmWebCharge
// frmCreditCardPayment
// frmCreditCardNumber
// frmPaymentDashboard
// frmAddExternalAccountHome
// frmShowAllBeneficiary
// frmAddExternalAccountKA
// frmCongratulations
// frmPayBillHome
// frmManagePayeeKA
// frmAddNewPayeeKA
// frmPayeeDetailsKA
// frmNewBillKA
// frmNewBillDetails
// frmConfirmPayBill
// frmCurrency
// frmAccountDetailsScreen
// frmCancelCardKA
// frmStopCardKA
// frmSettingsKA